﻿using System;
using Task05.Common;
using Task05.Storage;

namespace Task05Application
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read meters from file or create a new file for meters.
            IStorage<MetersCollection> storage;
            MetersCollection meters;
            CreateStorage(out storage, out meters);

            ProcessMetersMenu(storage, meters);
        }

        private static void CreateStorage(out IStorage<MetersCollection> storage, out MetersCollection meters)
        {
            storage = null;
            meters = null;
            while (true)
            {
                Console.WriteLine("Type an action number and press Enter.");
                Console.WriteLine(" 1. load XML file;");
                Console.WriteLine(" 2. create new XML file (if file exists it will be overidden);");
                Console.WriteLine(" 3. load JSON file;");
                Console.WriteLine(" 4. create new JSON file (if file exists it will be overidden);");
                Console.WriteLine(" 5. close application.");
                var choice = Console.ReadLine();
                var canContinue = false;
                try
                {
                    switch (choice)
                    {
                        case "1":
                            storage = new MetersStorageXml(GetFileName());
                            meters = storage.Restore();
                            canContinue = true;
                            break;
                        case "2":
                            storage = new MetersStorageXml(GetFileName());
                            meters = new MetersCollection();
                            storage.Store(meters);
                            canContinue = true;
                            break;
                        case "3":
                            storage = new MetersStorageJson(GetFileName());
                            meters = storage.Restore();
                            canContinue = true;
                            break;
                        case "4":
                            storage = new MetersStorageJson(GetFileName());
                            meters = new MetersCollection();
                            storage.Store(meters);
                            canContinue = true;
                            break;
                        case "5":
                            CloseApplication();
                            break;
                        default:
                            Console.WriteLine("Action '{0}' is not supported.", choice);
                            break;
                    }
                }
                catch (StorageException exc)
                {
                    ProcessStorageException(exc);
                    storage = null;
                    meters = null;
                    canContinue = false;
                }

                if (canContinue)
                    break;
                Console.WriteLine("Wrong data. Try once again.");
                Console.WriteLine();
            }            
        }

        private static string GetFileName()
        {
            Console.WriteLine("Type a path to file and press Enter.");
            return Console.ReadLine();
        }

        private static void ProcessMetersMenu(IStorage<MetersCollection> storage, MetersCollection meters)
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine("Type an action number and press Enter.");
                Console.WriteLine(" 1. read meters data;");
                Console.WriteLine(" 2. create new meter;");
                Console.WriteLine(" 3. delete a meter;");
                Console.WriteLine(" 4. switch meter mode;");
                Console.WriteLine(" 5. show current values;");
                Console.WriteLine(" 6. save meters to file;");
                Console.WriteLine(" 7. close application.");
                var choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        Console.WriteLine(meters.ToString());
                        break;
                    case "2":
                        Console.WriteLine(AddMeter(meters) ? "A new meter was created." : "Wrong data. Try once again.");
                        Console.WriteLine();
                        break;
                    case "3":
                        Console.WriteLine(DeleteMeter(meters) ? "A meter was deleted." : "Wrong data. Try once again.");
                        Console.WriteLine();
                        break;
                    case "4":
                        Console.WriteLine(ChangeMeterMode(meters) ? "A meters mode was changed." : "Wrong data. Try once again.");
                        Console.WriteLine();
                        break;
                    case "5":
                        ShowMeterValues(meters);
                        Console.WriteLine();
                        break;
                    case "6":
                        var saved = true;
                        try
                        {
                            storage.Store(meters);
                        }
                        catch (StorageException exc)
                        {
                            ProcessStorageException(exc);
                            saved = false;
                        }
                        Console.WriteLine(saved ? "Meters were saved." : "Meters were not saved.");
                        Console.WriteLine();
                        break;
                    case "7":
                        CloseApplication();
                        break;
                    default:
                        Console.WriteLine("Action '{0}' is not supported.", choice);
                        Console.WriteLine("Wrong data. Try once again.");
                        Console.WriteLine();
                        break;
                }
            }
        }

        private static void ProcessStorageException(StorageException exc)
        {
            Console.WriteLine(exc.Message);
            if (exc.InnerException != null)
                Console.WriteLine(exc.InnerException.Message);
        }

        private static void CloseApplication()
        {
            Environment.Exit(0);
        }

        private static bool AddMeter(MetersCollection meters)
        {
            //Read interval.
            int interval;
            Console.WriteLine("Type an interval to refresh meter data and press Enter.");
            if (!int.TryParse(Console.ReadLine(), out interval))
            {
                Console.WriteLine("Non integer data for interval.");
                return false;
            }
            if (interval <= 0)
            {
                Console.WriteLine("Interval should be greater than 0.");
                return false;
            }

            //Read meter type.
            Console.WriteLine("Type an action number to select a meter type and press Enter.");
            var values = Enum.GetValues(typeof (MeterTypes));
            for (int i = 0; i < values.Length; i++)
            {
                var value = values.GetValue(i);
                Console.WriteLine("{0}. {1}", (int)value, value);
            }
            int meterType;
            if (!int.TryParse(Console.ReadLine(), out meterType))
            {
                Console.WriteLine("Non integer data for action.");
                return false;
            }
            if (!Enum.IsDefined(typeof (MeterTypes), meterType))
            {
                Console.WriteLine("Action number is incorrect.");
                return false;
            }

            meters.AddMeter(interval, (MeterTypes)meterType);
            return true;
        }

        private static bool DeleteMeter(MetersCollection meters)
        {
            Console.WriteLine(meters.ToString());
            int id;
            if (!ReadMeterId(out id))
                return false;
            if (meters.DeleteMeter(id))
                return true;
            Console.WriteLine("Meter with id {0} does not exist.", id);
            return false;
        }

        private static bool ChangeMeterMode(MetersCollection meters)
        {
            Console.WriteLine(meters.ToString());
            int id;
            if (!ReadMeterId(out id))
                return false;
            if (meters.ChangeMeterState(id))
                return true;
            Console.WriteLine("Meter with id {0} does not exist.", id);
            return false;
        }

        private static bool ReadMeterId(out int id)
        {
            Console.WriteLine("Type an id of meter.");
            if (int.TryParse(Console.ReadLine(), out id)) 
                return true;
            Console.WriteLine("Non integer data for id.");
            return false;
        }

        private static void ShowMeterValues(MetersCollection meters)
        {
            foreach (var pair in meters.GetMetersValues())
                Console.WriteLine("ID: {0}, Value: {1}", pair.Key, pair.Value);
        }
    }
}

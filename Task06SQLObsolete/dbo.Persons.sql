﻿CREATE TABLE [dbo].[Persons]
(
	[Person_Id] INT IDENTITY(1,1),
    [Name] NVARCHAR(255) NOT NULL, 
    [AccountName] NVARCHAR(255) NOT NULL, 
    [SequredPassword] NVARCHAR(255) NOT NULL, 
    [Flags] INT NOT NULL, 
	[Role_Id] INT NULL, 
    CONSTRAINT [FK_Persons_Roles] FOREIGN KEY ([Role_Id]) REFERENCES [Roles]([Role_Id])
)
GO
CREATE UNIQUE CLUSTERED INDEX IX_Persons_Person_Id ON [dbo].[Persons] ([Person_Id])
GO
CREATE UNIQUE INDEX IX_Persons_AccountName ON [dbo].[Persons] ([AccountName])
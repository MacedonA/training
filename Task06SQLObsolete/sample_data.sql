﻿delete from Persons
delete from Roles
DBCC CHECKIDENT (Roles, RESEED, 1)
DBCC CHECKIDENT (Persons, RESEED, 2)

--Create roles.
insert into Roles (Name, [Description]) values ('Role 1', 'It''s a Role 1')
insert into Roles (Name, [Description]) values ('Role 2', 'It''s a Role 2')
insert into Roles (Name, [Description]) values ('Role 22', NULL)

--Create persons.
insert into Persons (Name, AccountName, SequredPassword, Flags, Role_Id) values ('Somebody, Ivan', 'Account', 'Password', 1, 3)
insert into Persons (Name, AccountName, SequredPassword, Flags, Role_Id) values ('OneMoreMan, Dave', 'Account2', 'LongPassword', 2, 2)
insert into Persons (Name, AccountName, SequredPassword, Flags, Role_Id) values ('OneMoreWoman, Sherri', 'Account-1', 'HardPassword', 0, 4)
insert into Persons (Name, AccountName, SequredPassword, Flags, Role_Id) values ('StrangeAccount, Bobbi', 'BAccount', 'ImpossiblePassword', 3, 4)
insert into Persons (Name, AccountName, SequredPassword, Flags, Role_Id) values ('JustUser, Vassisuary', 'AccountV', 'Pass', 1, 3)
insert into Persons (Name, AccountName, SequredPassword, Flags, Role_Id) values ('JustUserVassisuary', 'AccountV334', 'Pass', 1, 4)
insert into Persons (Name, AccountName, SequredPassword, Flags, Role_Id) values ('JustUser,', 'AccountVr456', 'Pass', 1, 2)

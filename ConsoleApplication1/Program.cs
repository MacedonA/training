﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine("Type an action number and press Enter.");
                Console.WriteLine(" 1. get all medcines;");
                Console.WriteLine(" 2. get medcine by id;");
                Console.WriteLine(" 3. get all users;");
                Console.WriteLine(" 4. get user by id;");
                Console.WriteLine(" 5. register new user;");
                Console.WriteLine(" 6. close application.");
                var choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        var medcines = GetData<IEnumerable<Medcine>>("Medcines");
                        if (medcines == null || !medcines.Any())
                            Console.WriteLine("Nothing found.");
                        else
                            MessageBox.Show(string.Join(
                                Environment.NewLine,
                                medcines.Select(med => string.Format("'{0}' by '{1}'.", med.MedcineName, med.ManufacturerName))
                            ));
                        break;
                    case "2":
                        Console.WriteLine("Type id.");
                        var medcineId = Console.ReadLine();
                        var medcine = GetData<Medcine>("Medcines/" + medcineId);
                        if (medcine == null)
                            Console.WriteLine("Nothing found.");
                        else
                            MessageBox.Show(string.Format("'{0}' by '{1}'.", medcine.MedcineName, medcine.ManufacturerName));
                        break;
                    case "3":
                        var users = GetData<IEnumerable<User>>("Users");
                        if (users == null || !users.Any())
                            Console.WriteLine("Nothing found.");
                        else
                            MessageBox.Show(string.Join(
                                Environment.NewLine,
                                users.Select(usr => string.Format("'{0}' in role '{1}'.", usr.Login, usr.Role))
                            ));
                        break;
                    case "4":
                        Console.WriteLine("Type id.");
                        var userId = Console.ReadLine();
                        var user = GetData<User>("Users/" + userId);
                        if (user == null)
                            Console.WriteLine("Nothing found.");
                        else
                            MessageBox.Show(string.Format("'{0}' in role '{1}'.", user.Login, user.Role));
                        break;
                    case "5":
                        Console.WriteLine("Type login.");
                        var login = Console.ReadLine();
                        Console.WriteLine("Type password.");
                        var password = Console.ReadLine();
                        var res = PostData("Users", new User { Login = login, Password = password });
                        if (!string.IsNullOrEmpty(res))
                            Console.WriteLine(res);
                        break;
                    case "6":
                        CloseApplication();
                        break;
                    default:
                        Console.WriteLine("Action '{0}' is not supported.", choice);
                        Console.WriteLine("Wrong data. Try once again.");
                        Console.WriteLine();
                        break;
                }
            }
        }

        private static void CloseApplication()
        {
            Environment.Exit(0);
        }

        private static T GetData<T>(string url) where T : class
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SiteAddress"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0,0,10);
                    var response = client.GetAsync("api/" + url).Result;
                    return !response.IsSuccessStatusCode ? null : JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("It does not work.");
                Console.WriteLine(exc);
                return null;
            }
        }

        private static string PostData<T>(string url, T data) where T : class
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SiteAddress"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = new TimeSpan(0, 0, 10);
                    var response = client.PostAsJsonAsync("api/" + url, data).Result;
                    var str = response.Content.ReadAsStringAsync().Result;
                    bool result;
                    if (bool.TryParse(str, out result))
                        return result ? string.Empty : "Action failed.";
                    return str;
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("It does not work.");
                Console.WriteLine(exc);
                return null;
            }
        }

        public class Medcine
        {
            [JsonProperty("<MedcineName>k__BackingField")]
            public string MedcineName { get; set; }
            [JsonProperty("<ManufacturerName>k__BackingField")]
            public string ManufacturerName { get; set; }
        }

        public class User
        {
            public string Login { get; set; }
            public string Role { get; set; }
            public string Password { get; set; }
        }
    }
}

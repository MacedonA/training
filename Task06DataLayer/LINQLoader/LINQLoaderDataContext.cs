﻿using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;
using Task06DataLayer.Data.LINQData;

namespace Task06DataLayer.LINQLoader
{
    /// <summary>
    /// Represents a connection to databse.
    /// </summary>
    internal class LINQLoaderDataContext : DataContext
    {
        /// <summary>
        /// Constructor with connection string.
        /// </summary>
        public LINQLoaderDataContext(string fileOrServerOrConnection) : base(fileOrServerOrConnection) { }
        
        [Function(Name = "dbo.SelectAllPersons")]
        public ISingleResult<Person> SelectAllPersons()
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return (ISingleResult<Person>)(result.ReturnValue); ;
        }

        [Function(Name = "dbo.SelectPerson")]
        [ResultType(typeof(Person))]
        [ResultType(typeof(Role))]
        public IMultipleResults SelectPerson([Parameter(DbType = "Int")] int person_id)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), person_id);
            return (IMultipleResults)(result.ReturnValue); ;
        }

        [Function(Name = "dbo.InsertPerson")]
        public ISingleResult<Person> InsertPerson(
            [Parameter(DbType = "NVarChar(255)", Name = "firstname")] string firstname,
            [Parameter(DbType = "NVarChar(255)", Name = "lastname")] string lastname,
            [Parameter(DbType = "NVarChar(255)", Name = "accountname")] string accountname,
            [Parameter(DbType = "NVarChar(255)", Name = "sequredpassword")] string sequredpassword,
            [Parameter(DbType = "Bit", Name = "isactive")] bool? isactive,
            [Parameter(DbType = "Bit", Name = "isfavorite")] bool? isfavorite,
            [Parameter(DbType = "Xml", Name = "role_ids")] string role_ids)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())),
                firstname, lastname, accountname, sequredpassword, isactive,
                isfavorite, role_ids);
            return (ISingleResult<Person>)(result.ReturnValue);
        }

        [Function(Name = "dbo.UpdatePerson")]
        public ISingleResult<object> UpdatePerson(
            [Parameter(DbType = "Int", Name = "person_id")] int personid,
            [Parameter(DbType = "NVarChar(255)", Name = "firstname")] string firstname,
            [Parameter(DbType = "NVarChar(255)", Name = "lastname")] string lastname,
            [Parameter(DbType = "NVarChar(255)", Name = "accountname")] string accountname,
            [Parameter(DbType = "NVarChar(255)", Name = "sequredpassword")] string sequredpassword,
            [Parameter(DbType = "Bit", Name = "isactive")] bool? isactive,
            [Parameter(DbType = "Bit", Name = "isfavorite")] bool? isfavorite,
            [Parameter(DbType = "Xml", Name = "role_ids")] string role_ids)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())),
                personid, firstname, lastname, accountname, sequredpassword, isactive,
                isfavorite, role_ids);
            return (ISingleResult<object>)(result.ReturnValue);
        }
        
        [Function(Name = "dbo.DeletePerson")]
        public ISingleResult<object> DeletePerson([Parameter(DbType = "Int")] int person_id)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), person_id);
            return (ISingleResult<object>)(result.ReturnValue); ;
        }

        [Function(Name = "dbo.SelectAllRoles")]
        public ISingleResult<Role> SelectAllRoles()
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return (ISingleResult<Role>)(result.ReturnValue); ;
        }

        [Function(Name = "dbo.SelectRole")]
        public ISingleResult<Role> SelectRole([Parameter(DbType = "Int")] int role_id)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), role_id);
            return (ISingleResult<Role>)(result.ReturnValue); ;
        }

        [Function(Name = "dbo.InsertRole")]
        public ISingleResult<Role> InsertRole(
            [Parameter(DbType = "NVarChar(255)", Name = "name")] string name,
            [Parameter(DbType = "NVarChar(255)", Name = "description")] string description)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())),
                name, description);
            return (ISingleResult<Role>)(result.ReturnValue);
        }

        [Function(Name = "dbo.UpdateRole")]
        public ISingleResult<object> UpdateRole(
            [Parameter(DbType = "Int", Name = "role_id")] int roleid,
            [Parameter(DbType = "NVarChar(255)", Name = "name")] string name,
            [Parameter(DbType = "NVarChar(255)", Name = "description")] string description)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())),
                roleid, name, description);
            return (ISingleResult<object>)(result.ReturnValue);
        }

        [Function(Name = "dbo.DeleteRole")]
        public ISingleResult<object> DeleteRole([Parameter(DbType = "Int")] int role_id)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), role_id);
            return (ISingleResult<object>)(result.ReturnValue); ;
        }
    }
}

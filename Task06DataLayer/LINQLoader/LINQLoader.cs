﻿using System.Linq;
using Task06DataLayer.Data;
using Task06DataLayer.Data.LINQData;

namespace Task06DataLayer.LINQLoader
{
    /// <summary>
    /// Loads data from database using LINQ to SQL.
    /// </summary>
    public class LINQLoader : ILoader
    {
        private readonly LINQLoaderDataContext context;

        /// <summary>
        /// Creates a loader by configuration string.
        /// </summary>
        public LINQLoader(string cfgStr)
        {
            context = new LINQLoaderDataContext(cfgStr);
        }

        /// <summary>
        /// Frees context.
        /// </summary>
        public void Dispose()
        {
            context.Dispose();
        }

        #region Persons
        /// <summary>
        /// Selects all persons from database.
        /// Does nor select a roles list.
        /// </summary>
        public IPerson[] SelectAllPersonsInfo()
        {
            var result = context.SelectAllPersons();
            return result.Cast<IPerson>().ToArray();
        }

        /// <summary>
        /// Selects a person by id.
        /// </summary>
        public IPerson SelectPerson(int person_id)
        {
            var result = context.SelectPerson(person_id);
            var toRet = result.GetResult<Person>().FirstOrDefault();
            if (toRet != null)
                toRet.Roles = result.GetResult<Role>().Cast<IRole>().ToArray();
            return toRet;
        }

        /// <summary>
        /// Inserts a person.
        /// </summary>
        public int InsertPerson(IPerson person)
        {
            var result = context.InsertPerson(person.FirstName, person.LastName, person.AccountName, person.Password,
                person.IsActive, person.IsFavorite, (Helper.RolesToXml(person.Roles)));
            return result.FirstOrDefault().PersonId;
        }

        /// <summary>
        /// Updates a person.
        /// </summary>
        public void UpdatePerson(IPerson person)
        {
            context.UpdatePerson(person.PersonId, person.FirstName, person.LastName, person.AccountName,
                person.Password, person.IsActive, person.IsFavorite, (Helper.RolesToXml(person.Roles)));
        }

        /// <summary>
        /// Deletes a person.
        /// </summary>
        public void DeletePerson(int id)
        {
            context.DeletePerson(id);
        }
        #endregion

        #region Roles
        /// <summary>
        /// Selects all roles from database.
        /// </summary>
        public IRole[] SelectAllRolesInfo()
        {
            var result = context.SelectAllRoles();
            return result.Cast<IRole>().ToArray();
        }

        /// <summary>
        /// Selects a role by id.
        /// </summary>
        public IRole SelectRole(int id)
        {
            var result = context.SelectRole(id);
            return result.FirstOrDefault();
        }

        /// <summary>
        /// Inserts a role.
        /// </summary>
        public int InsertRole(IRole role)
        {
            var result = context.InsertRole(role.Name, role.Description);
            return result.FirstOrDefault().RoleId;
        }

        /// <summary>
        /// Updates a role.
        /// </summary>
        public void UpdateRole(IRole role)
        {
            context.UpdateRole(role.RoleId, role.Name, role.Description);
        }

        /// <summary>
        /// Deletes a role.
        /// </summary>
        public void DeleteRole(int id)
        {
            context.DeleteRole(id);
        }
        #endregion
    }
}

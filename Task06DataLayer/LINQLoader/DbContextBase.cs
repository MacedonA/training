﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Task06DataLayer.Data.EFData;

namespace Task06DataLayer.LINQLoader
{
    public abstract class DbContextBase<TInstance, TDbInstance> : DbContext 
        where TInstance : class 
        where TDbInstance : class 
    {
        protected DbContextBase()
            : base("name=EFLoaderDataContext")
        {
        }

        public override DbSet<TEntity> Set<TEntity>()
        {
            var asd = ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<TDbInstance>("SelectAllRoles");
            return base.Set<TEntity>();
        }
    }

    internal class RolesDbContext : DbContextBase<Role, RoleLocal>
    {
        
    }
}
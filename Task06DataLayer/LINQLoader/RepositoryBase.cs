﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Task06DataLayer.Data.EFData;

namespace Task06DataLayer.LINQLoader
{
    /// <summary>
    /// Represents a bease class to manage a repository of entries.
    /// </summary>
    public class RepositoryBase<T> : IDisposable
       where T : DbContext, new()
    {
        private T _context;

        /// <summary>
        /// Gets a data context. If it does not exists creates a new one.
        /// </summary>
        public virtual T DataContext
        {
            get { return _context ?? (_context = new T()); }
        }

        /// <summary>
        /// Returns a single entry.
        /// </summary>
        public virtual TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return DataContext.Set<TEntity>().Where(predicate).SingleOrDefault();
        }

        /// <summary>
        /// Returns a list of entries.
        /// </summary>
        public virtual IQueryable<TEntity> GetList<TEntity>() where TEntity : class
        {
            return DataContext.Set<TEntity>();
        }

        /// <summary>
        /// Creates a new entry in database.
        /// </summary>
        public virtual bool Save<TEntity>(TEntity entity) where TEntity : class
        {
            return DataContext.SaveChanges() > 0;
        }

        /// <summary>
        /// Updates an entry in database.
        /// </summary>
        public virtual bool Update<TEntity>(TEntity entity, params string[] propsToUpdate) where TEntity : class
        {
            DataContext.Set<TEntity>().Attach(entity);
            return DataContext.SaveChanges() > 0;
        }

        /// <summary>
        /// Deletes an entry from database.
        /// </summary>
        public virtual bool Delete<TEntity>(TEntity entity) where TEntity : class
        {
            ObjectSet<TEntity> objectSet = ((IObjectContextAdapter)DataContext).ObjectContext.CreateObjectSet<TEntity>();
            objectSet.Attach(entity);
            objectSet.DeleteObject(entity);
            return DataContext.SaveChanges() > 0;
        }

        /// <summary>
        /// Frees al lresources.
        /// </summary>
        public void Dispose()
        {
            if (_context != null)
                _context.Dispose();
        }
    }
}
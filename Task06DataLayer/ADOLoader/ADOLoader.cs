﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Task06DataLayer.Data;
using Task06DataLayer.Data.ADOData;

namespace Task06DataLayer.ADOLoader
{
    /// <summary>
    /// Loads data from database using ADO.NET.
    /// </summary>
    public class ADOLoader : ILoader
    {
        private readonly string _connectionString;

        /// <summary>
        /// Creates a loader by configuration string.
        /// </summary>
        public ADOLoader(string cfgStr)
        {
            _connectionString = cfgStr;
        }

        /// <summary>
        /// Frees context.
        /// </summary>
        public void Dispose()
        {

        }

        #region Persons
        /// <summary>
        /// Selects all persons from database.
        /// Does nor select a roles list.
        /// </summary>
        public IPerson[] SelectAllPersonsInfo()
        {
            var ds = new DataSet();
            FillDataSet("SelectAllPersons", null, ref ds);

            return (from DataRow row in ds.Tables[0].Rows select RowToPerson(row, null)).ToArray();
        }

        /// <summary>
        /// Selects a person by id.
        /// </summary>
        public IPerson SelectPerson(int id)
        {
            var ds = new DataSet();
            FillDataSet("SelectPerson", PersonToParameters(null, id, true, false), ref ds);
            var roles = (from DataRow row in ds.Tables[1].Rows select RowToRole(row)).ToArray();
            return RowToPerson(ds.Tables[0].Rows[0], roles);
        }

        /// <summary>
        /// Inserts a person.
        /// </summary>
        public int InsertPerson(IPerson person)
        {
            var ds = new DataSet();
            FillDataSet("InsertPerson", PersonToParameters(person, null, false, true), ref ds);

            return (int)(ds.Tables[0].Rows[0]["Person_Id"]);
        }

        /// <summary>
        /// Updates a person.
        /// </summary>
        public void UpdatePerson(IPerson person)
        {
            var ds = new DataSet();
            FillDataSet("UpdatePerson", PersonToParameters(person, null, true, true), ref ds);
        }

        /// <summary>
        /// Deletes a person.
        /// </summary>
        public void DeletePerson(int id)
        {
            var ds = new DataSet();
            FillDataSet("DeletePerson", PersonToParameters(null, id, true, false), ref ds);
        }

        private static IPerson RowToPerson(DataRow row, IRole[] roles)
        {
            return new Person((int)row["Person_Id"], ObjToString(row["FirstName"]), ObjToString(row["LastName"]), ObjToString(row["AccountName"]), ObjToString(row["SequredPassword"]),
                ObjToBool(row["IsActive"]), ObjToBool(row["IsFavorite"]), roles ?? new IRole[0]);
        }

        private static SqlParameter[] PersonToParameters(IPerson person, int? id, bool includeId, bool inculdeData)
        {
            var result = new List<SqlParameter>();
            if (includeId && (id != null || person != null))
                result.Add(new SqlParameter("person_id", SqlDbType.Int) { Value = id ?? person.PersonId });
            if (inculdeData && person != null)
            {
                result.Add(new SqlParameter("firstname", SqlDbType.NVarChar, 255) { Value = (object)person.FirstName ?? DBNull.Value });
                result.Add(new SqlParameter("lastname", SqlDbType.NVarChar, 255) { Value = (object)person.LastName ?? DBNull.Value });
                result.Add(new SqlParameter("accountname", SqlDbType.NVarChar, 255) { Value = (object)person.AccountName ?? DBNull.Value });
                result.Add(new SqlParameter("sequredpassword", SqlDbType.NVarChar, 255) { Value = (object)person.Password ?? DBNull.Value });
                result.Add(new SqlParameter("isactive", SqlDbType.Bit) { Value = person.IsActive ? 1 : 0 });
                result.Add(new SqlParameter("isfavorite", SqlDbType.Bit) { Value = person.IsFavorite ? 1 : 0 });
                result.Add(new SqlParameter("role_ids", SqlDbType.Xml) { Value = Helper.RolesToXml(person.Roles) });
            }
            return result.ToArray();
        }
        #endregion

        #region Roles
        /// <summary>
        /// Selects all roles from database.
        /// </summary>
        public IRole[] SelectAllRolesInfo()
        {
            var ds = new DataSet();
            FillDataSet("SelectAllRoles", null, ref ds);

            return (from DataRow row in ds.Tables[0].Rows select RowToRole(row)).ToArray();
        }

        /// <summary>
        /// Selects a role by id.
        /// </summary>
        public IRole SelectRole(int id)
        {
            var ds = new DataSet();
            FillDataSet("SelectRole", RoleToParameters(null, id, true, false), ref ds);

            return RowToRole(ds.Tables[0].Rows[0]);
        }

        /// <summary>
        /// Inserts a role.
        /// </summary>
        public int InsertRole(IRole role)
        {
            var ds = new DataSet();
            FillDataSet("InsertRole", RoleToParameters(role, null, false, true), ref ds);

            return (int)(ds.Tables[0].Rows[0]["Role_Id"]);
        }

        /// <summary>
        /// Updates a role.
        /// </summary>
        public void UpdateRole(IRole role)
        {
            var ds = new DataSet();
            FillDataSet("UpdateRole", RoleToParameters(role, null, true, true), ref ds);
        }

        /// <summary>
        /// Deletes a role.
        /// </summary>
        public void DeleteRole(int id)
        {
            var ds = new DataSet();
            FillDataSet("DeleteRole", RoleToParameters(null, id, true, false), ref ds);
        }

        private static IRole RowToRole(DataRow row)
        {
            return new Role((int)row["Role_Id"], ObjToString(row["Name"]), ObjToString(row["Description"]));
        }

        private static SqlParameter[] RoleToParameters(IRole role, int? id, bool includeId, bool inculdeData)
        {
            var result = new List<SqlParameter>();
            if (includeId && (id != null || role != null))
                result.Add(new SqlParameter("role_id", SqlDbType.Int) { Value = id ?? role.RoleId });
            if (inculdeData && role != null)
            {
                result.Add(new SqlParameter("name", SqlDbType.NVarChar, 255) { Value = (object)role.Name ?? DBNull.Value });
                result.Add(new SqlParameter("description", SqlDbType.NVarChar, 4000) { Value = (object)role.Description ?? DBNull.Value });
            }
            return result.ToArray();
        }
        #endregion

        #region Helpers
        private void FillDataSet(string procedureName, SqlParameter[] sqlPars, ref DataSet ds)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            using (var cmd = new SqlCommand(procedureName, con) { CommandType = CommandType.StoredProcedure })
            {
                if (sqlPars != null)
                    cmd.Parameters.AddRange(sqlPars);
                using (var da = new SqlDataAdapter(cmd))
                    da.Fill(ds);
            }
        }

        private static bool ObjToBool(object obj)
        {
            return (int)obj == 1;
        }

        private static string ObjToString(object obj)
        {
            return obj == null || obj is DBNull ? null : obj.ToString();
        }

        /// <summary>
        /// Runs any SQL.
        /// </summary>
        public void RunSql(string sql)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            using (var cmd = new SqlCommand(sql, con) {CommandType = CommandType.Text})
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
        #endregion
    }
}

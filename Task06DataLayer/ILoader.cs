﻿using System;
using Task06DataLayer.Data;

namespace Task06DataLayer
{
    public interface ILoader : IDisposable
    {
        /// <summary>
        /// Selects all persons from database.
        /// Does not select a roles list.
        /// </summary>
        IPerson[] SelectAllPersonsInfo();

        /// <summary>
        /// Selects a person by id.
        /// </summary>
        IPerson SelectPerson(int id);

        /// <summary>
        /// Inserts a person.
        /// </summary>
        int InsertPerson(IPerson person);

        /// <summary>
        /// Updates a person.
        /// </summary>
        void UpdatePerson(IPerson person);

        /// <summary>
        /// Deletes a person.
        /// </summary>
        void DeletePerson(int id);

        /// <summary>
        /// Selects all roles from database.
        /// </summary>
        IRole[] SelectAllRolesInfo();

        /// <summary>
        /// Selects a role by id.
        /// </summary>
        IRole SelectRole(int id);

        /// <summary>
        /// Inserts a role.
        /// </summary>
        int InsertRole(IRole role);

        /// <summary>
        /// Updates a role.
        /// </summary>
        void UpdateRole(IRole role);

        /// <summary>
        /// Deletes a role.
        /// </summary>
        void DeleteRole(int id);
    }
}

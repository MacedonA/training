﻿namespace Task06DataLayer.Data
{
    public interface IRole
    {
        /// <summary>
        /// Id of the role.
        /// </summary>
        int RoleId { get; }

        /// <summary>
        /// Name of the role.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Description of the role.
        /// </summary>
        string Description { get; } 
    }
}
﻿using System.Data.Linq.Mapping;

namespace Task06DataLayer.Data.LINQData
{
    /// <summary>
    /// Represents a person object.
    /// </summary>
    [Table(Name = "dbo.Persons")]
    internal class Person : IPerson
    {
        private int _personId;
        private string _firstName;
        private string _lastName;
        private string _accountName;
        private string _password;
        private bool _isActive;
        private bool _isFavorite;
        private IRole[] _roles = new IRole[0];

        /// <summary>
        /// Id of the person.
        /// </summary>
        [Column(Name = "Person_Id", Storage = "_personId", DbType = "Int NOT NULL IDENTITY", IsDbGenerated = true, AutoSync = AutoSync.Never)]
        public int PersonId
        {
            get { return _personId; }
        }

        /// <summary>
        /// First name of the person.
        /// </summary>
        [Column(Name = "FirstName", Storage = "_firstName", DbType = "NVarChar(255) NOT NULL", CanBeNull = false, AutoSync = AutoSync.Never)]
        public string FirstName
        {
            get { return _firstName; }
        }

        /// <summary>
        /// Last name of the person.
        /// </summary>
        [Column(Name = "LastName", Storage = "_lastName", DbType = "NVarChar(255) NOT NULL", CanBeNull = false, AutoSync = AutoSync.Never)]
        public string LastName
        {
            get { return _lastName; }
        }

        /// <summary>
        /// Account name.
        /// </summary>
        [Column(Name = "AccountName", Storage = "_accountName", DbType = "NVarChar(255) NOT NULL", CanBeNull = false, AutoSync = AutoSync.Never)]
        public string AccountName
        {
            get { return _accountName; }
        }

        /// <summary>
        /// Password.
        /// </summary>
        [Column(Name = "SequredPassword", Storage = "_password", DbType = "NVarChar(255) NOT NULL", CanBeNull = false, AutoSync = AutoSync.Never)]
        public string Password
        {
            get { return _password; }
        }

        /// <summary>
        /// Is active flag.
        /// </summary>
        [Column(Name = "IsActive", Storage = "_isActive", DbType = "Int", CanBeNull = false, AutoSync = AutoSync.Never)]
        public bool IsActive
        {
            get { return _isActive; }
        }

        /// <summary>
        /// Is favorite flag.
        /// </summary>
        [Column(Name = "IsFavorite", Storage = "_isFavorite", DbType = "Int", CanBeNull = false, AutoSync = AutoSync.Never)]
        public bool IsFavorite
        {
            get { return _isFavorite; }
        }

        /// <summary>
        /// Roles of a person.
        /// </summary>
        public IRole[] Roles
        {
            get { return _roles; }
            internal set { _roles = value; }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Person() { }

        /// <summary>
        /// Constructor with full initialization.
        /// </summary>
        public Person(int personId, string firstName, string lastName, string accountName, string password,
            bool isActive, bool isFavorite, IRole[] roles)
        {
            _personId = personId;
            _firstName = firstName;
            _lastName = lastName;
            _accountName = accountName;
            _password = password;
            _isActive = isActive;
            _isFavorite = isFavorite;
            _roles = roles;
        }
    }
}

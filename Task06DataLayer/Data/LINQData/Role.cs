﻿using System.Data.Linq.Mapping;

namespace Task06DataLayer.Data.LINQData
{
    /// <summary>
    /// Represents a role object.
    /// </summary>
    [Table(Name = "dbo.Roles")]
    internal class Role : IRole
    {
        private int _roleId;
        private string _name;
        private string _description;

        /// <summary>
        /// Id of the role.
        /// </summary>
        [Column(Name = "Role_Id", Storage = "_roleId", DbType = "Int NOT NULL IDENTITY", IsDbGenerated = true, AutoSync = AutoSync.Never)]
        public int RoleId
        {
            get { return _roleId; }
        }

        /// <summary>
        /// Name of the role.
        /// </summary>
        [Column(Name = "Name", Storage = "_name", DbType = "NVarChar(255) NOT NULL", CanBeNull = false, AutoSync = AutoSync.Never)]
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Description of the role.
        /// </summary>
        [Column(Name = "Description", Storage = "_description", DbType = "NVarChar(4000)", AutoSync = AutoSync.Never)]
        public string Description
        {
            get { return _description; }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Role() { }

        /// <summary>
        /// Constructor with full initialization.
        /// </summary>
        public Role(int roleId, string name, string description)
        {
            _roleId = roleId;
            _name = name;
            _description = description;
        }
    }
}

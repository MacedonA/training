﻿using System.Xml;

namespace Task06DataLayer.Data
{
    /// <summary>
    /// Several helper methods used by different classes.
    /// </summary>
    internal static class Helper
    {
        /// <summary>
        /// Converts list of role to XML.
        /// </summary>
        public static string RolesToXml(IRole[] roles)
        {
            XmlDocument ids = new XmlDocument();
            var root = ids.CreateElement("root");
            foreach (var role in roles)
            {
                var roleid = ids.CreateElement("id");
                roleid.InnerText = role.RoleId.ToString();
                root.AppendChild(roleid);
            }
            ids.AppendChild(root);
            return ids.InnerXml;
        }
    }
}

﻿namespace Task06DataLayer.Data
{
    /// <summary>
    /// Represents a person object.
    /// </summary>
    public interface IPerson
    {
        /// <summary>
        /// Id of the person.
        /// </summary>
        int PersonId { get; }

        /// <summary>
        /// First name of the person.
        /// </summary>
        string FirstName { get; }

        /// <summary>
        /// Last name of the person.
        /// </summary>
        string LastName { get; }

        /// <summary>
        /// Account name.
        /// </summary>
        string AccountName { get; }

        /// <summary>
        /// Password.
        /// </summary>
        string Password { get; }

        /// <summary>
        /// Is active flag.
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Is favorite flag.
        /// </summary>
        bool IsFavorite { get; }

        /// <summary>
        /// Roles of a person.
        /// </summary>
        IRole[] Roles { get; }
    }
}
﻿namespace Task06DataLayer.Data.ADOData
{
    /// <summary>
    /// Represents a role object.
    /// </summary>
    public class Role : IRole
    {
        /// <summary>
        /// Id of the role.
        /// </summary>
        public int RoleId { get; private set; }

        /// <summary>
        /// Name of the role.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Description of the role.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Constructor with full initialization.
        /// </summary>
        public Role(int roleId, string name, string description)
        {
            RoleId = roleId;
            Name = name;
            Description = description;
        }
    }
}

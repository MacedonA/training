﻿namespace Task06DataLayer.Data.ADOData
{
    /// <summary>
    /// Represents a person object.
    /// </summary>
    public class Person : IPerson
    {
        /// <summary>
        /// Id of the person.
        /// </summary>
        public int PersonId { get; private set; }

        /// <summary>
        /// First name of the person.
        /// </summary>
        public string FirstName { get; private set; }

        /// <summary>
        /// Last name of the person.
        /// </summary>
        public string LastName { get; private set; }

        /// <summary>
        /// Account name.
        /// </summary>
        public string AccountName { get; private set; }

        /// <summary>
        /// Password.
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// Is active flag.
        /// </summary>
        public bool IsActive { get; private set; }

        /// <summary>
        /// Is favorite flag.
        /// </summary>
        public bool IsFavorite { get; private set; }

        /// <summary>
        /// Roles of a person.
        /// </summary>
        public IRole[] Roles { get; private set; }

        /// <summary>
        /// Constructor with full initialization.
        /// </summary>
        public Person(int personId, string firstName, string lastName, string accountName, string password,
            bool isActive, bool isFavorite, IRole[] roles)
        {
            PersonId = personId;
            FirstName = firstName;
            LastName = lastName;
            AccountName = accountName;
            Password = password;
            IsActive = isActive;
            IsFavorite = isFavorite;
            Roles = roles;
        }
    }
}

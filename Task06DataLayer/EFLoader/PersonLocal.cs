//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Task06DataLayer.EFLoader
{
    using System;
    
    internal partial class PersonLocal
    {
        public int Person_Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AccountName { get; set; }
        public string SequredPassword { get; set; }
        public Nullable<int> IsActive { get; set; }
        public int IsFavorite { get; set; }
    }
}

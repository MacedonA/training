﻿using System;

namespace Task06DataLayer.EFLoader
{
    /// <summary>
    /// Unit of works for people and roles.
    /// </summary>
    internal class PeopleRolesUnitOfWork : IDisposable
    {
        private readonly EFLoaderDataContext _context;
        private bool disposed;
        private PeopleRepository _people;
        private RoleRepository _roles;
        
        /// <summary>
        /// Default constructor. Creates a context.
        /// </summary>
        public PeopleRolesUnitOfWork()
        {
            _context = new EFLoaderDataContext();
        }

        /// <summary>
        /// Saves changes to database.
        /// </summary>
        public void Save()
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// Frees context. Unsaved changes will be discarded.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Frees context. Unsaved changes will be discarded.
        /// </summary>
        public virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                _context.Dispose();
            }
            disposed = true;
        }

        /// <summary>
        /// Access to a people repository.
        /// </summary>
        public PeopleRepository People {get { return _people ?? (_people = new PeopleRepository(_context)); }}

        /// <summary>
        /// Access to a people repository.
        /// </summary>
        public RoleRepository Roles { get { return _roles ?? (_roles = new RoleRepository(_context)); } }
    }
}
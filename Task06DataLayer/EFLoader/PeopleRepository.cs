﻿using System.Linq;
using Task06DataLayer.Data;
using Task06DataLayer.Data.ADOData;

namespace Task06DataLayer.EFLoader
{
    /// <summary>
    /// Represents a repository for people.
    /// </summary>
    internal class PeopleRepository
    {
        private readonly EFLoaderDataContext _context;

        /// <summary>
        /// Creates a repository.
        /// </summary>
        public PeopleRepository(EFLoaderDataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Selects all persons from database.
        /// Does nor select a roles list.
        /// </summary>
        public IPerson[] SelectAllPersonsInfo()
        {
            var result = _context.SelectAllPersons();
            return result.Select(person => new Person(person.Person_Id, person.FirstName, person.LastName, person.AccountName,
                person.SequredPassword, person.IsActive.Value == 1, person.IsFavorite == 1, new IRole[0])).Cast<IPerson>().ToArray();
        }

        /// <summary>
        /// Selects a person by id.
        /// </summary>
        public IPerson SelectPerson(int person_id)
        {
            var result = _context.SelectPerson(person_id);
            var person = result.FirstOrDefault();
            var roles = person == null ? new IRole[0] : result.GetNextResult<RoleLocal>().Select(role => new Role(role.Role_Id, role.Name, role.Description)).OfType<IRole>().ToArray();
            return new Person(person.Person_Id, person.FirstName, person.LastName, person.AccountName,
                person.SequredPassword, person.IsActive.Value == 1, person.IsFavorite == 1, roles);
        }

        /// <summary>
        /// Inserts a person.
        /// </summary>
        public int InsertPerson(IPerson person)
        {
            var result = _context.InsertPerson(person.FirstName, person.LastName, person.AccountName, person.Password,
                person.IsActive, person.IsFavorite, (Helper.RolesToXml(person.Roles)));
            return result.FirstOrDefault().Value;
        }

        /// <summary>
        /// Updates a person.
        /// </summary>
        public void UpdatePerson(IPerson person)
        {
            _context.UpdatePerson(person.PersonId, person.FirstName, person.LastName, person.AccountName,
                person.Password, person.IsActive, person.IsFavorite, (Helper.RolesToXml(person.Roles)));
        }

        /// <summary>
        /// Deletes a person.
        /// </summary>
        public void DeletePerson(int id)
        {
            _context.DeletePerson(id);
        }
    }
}
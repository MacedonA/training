﻿using System.Linq;
using Task06DataLayer.Data;
using Task06DataLayer.Data.ADOData;

namespace Task06DataLayer.EFLoader
{
    /// <summary>
    /// Represents a repository for roles.
    /// </summary>
    internal class RoleRepository
    {
        private readonly EFLoaderDataContext _context;

        /// <summary>
        /// Creates a repository.
        /// </summary>
        public RoleRepository(EFLoaderDataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Selects all roles from database.
        /// </summary>
        public IRole[] SelectAllRolesInfo()
        {
            var result = _context.SelectAllRoles();
            return result.Select(role => new Role(role.Role_Id, role.Name, role.Description)).Cast<IRole>().ToArray();
        }

        /// <summary>
        /// Selects a role by id.
        /// </summary>
        public IRole SelectRole(int id)
        {
            var role = _context.SelectRole(id).FirstOrDefault();
            return role == null ? null : new Role(role.Role_Id, role.Name, role.Description);
        }

        /// <summary>
        /// Inserts a role.
        /// </summary>
        public int InsertRole(IRole role)
        {
            var result = _context.InsertRole(role.Name, role.Description);
            return result.FirstOrDefault().Value;
        }

        /// <summary>
        /// Updates a role.
        /// </summary>
        public void UpdateRole(IRole role)
        {
            _context.UpdateRole(role.RoleId, role.Name, role.Description);
        }

        /// <summary>
        /// Deletes a role.
        /// </summary>
        public void DeleteRole(int id)
        {
            _context.DeleteRole(id);
        }
    }
}
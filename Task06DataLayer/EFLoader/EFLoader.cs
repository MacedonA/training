﻿using Task06DataLayer.Data;

namespace Task06DataLayer.EFLoader
{
    /// <summary>
    /// Loads data from database using EntityFramework.
    /// </summary>
    public class EFLoader : ILoader
    {
        private readonly PeopleRolesUnitOfWork unitOfWork;

        /// <summary>
        /// Creates a loader.
        /// </summary>
        public EFLoader()
        {
            unitOfWork = new PeopleRolesUnitOfWork();
        }

        /// <summary>
        /// Frees unit of work.
        /// </summary>
        public void Dispose()
        {
            unitOfWork.Dispose();
        }

        #region Persons
        /// <summary>
        /// Selects all persons from database.
        /// Does nor select a roles list.
        /// </summary>
        public IPerson[] SelectAllPersonsInfo()
        {
            var res = unitOfWork.People.SelectAllPersonsInfo();
            unitOfWork.Save();
            return res;
        }

        /// <summary>
        /// Selects a person by id.
        /// </summary>
        public IPerson SelectPerson(int person_id)
        {
            var res = unitOfWork.People.SelectPerson(person_id);
            unitOfWork.Save();
            return res;
        }

        /// <summary>
        /// Inserts a person.
        /// </summary>
        public int InsertPerson(IPerson person)
        {
            var res = unitOfWork.People.InsertPerson(person);
            unitOfWork.Save();
            return res;
        }

        /// <summary>
        /// Updates a person.
        /// </summary>
        public void UpdatePerson(IPerson person)
        {
            unitOfWork.People.UpdatePerson(person);
            unitOfWork.Save();
        }

        /// <summary>
        /// Deletes a person.
        /// </summary>
        public void DeletePerson(int id)
        {
            unitOfWork.People.DeletePerson(id);
            unitOfWork.Save();
        }
        #endregion

        #region Roles
        /// <summary>
        /// Selects all roles from database.
        /// </summary>
        public IRole[] SelectAllRolesInfo()
        {
            var res = unitOfWork.Roles.SelectAllRolesInfo();
            unitOfWork.Save();
            return res;
        }

        /// <summary>
        /// Selects a role by id.
        /// </summary>
        public IRole SelectRole(int id)
        {
            var res = unitOfWork.Roles.SelectRole(id);
            unitOfWork.Save();
            return res;
        }

        /// <summary>
        /// Inserts a role.
        /// </summary>
        public int InsertRole(IRole role)
        {
            var res = unitOfWork.Roles.InsertRole(role);
            unitOfWork.Save();
            return res;
        }

        /// <summary>
        /// Updates a role.
        /// </summary>
        public void UpdateRole(IRole role)
        {
            unitOfWork.Roles.UpdateRole(role);
            unitOfWork.Save();
        }

        /// <summary>
        /// Deletes a role.
        /// </summary>
        public void DeleteRole(int id)
        {
            unitOfWork.Roles.DeleteRole(id);
            unitOfWork.Save();
        }
        #endregion
    }
}
﻿namespace Task05.Common
{
    /// <summary>
    /// An interface to proccess <see cref="IMeter"/>'s messages.
    /// </summary>
    public interface IMeterObserver
    {
        /// <summary>
        /// Processes a changed value from <see cref="IMeter"/>.
        /// </summary>
        void Update(int id, double val);
    }
}
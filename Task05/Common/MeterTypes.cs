﻿namespace Task05.Common
{
    /// <summary>
    /// All available types of the meter.
    /// </summary>
    public enum MeterTypes
    {
        Pressure, 
        Force,
        Length,
        Temperature
    }
}
﻿using System;

namespace Task05.Common
{
    /// <summary>
    /// Access to shared properties and methods of the meter.
    /// </summary>
    public interface IMeter: IDisposable
    {
        /// <summary>
        /// Uniques identifier of the meter.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Type of the meter.
        /// </summary>
        MeterTypes MeterType { get; }

        /// <summary>
        /// Interval to change <see cref="CurrentValue"/> in milliseconds.
        /// </summary>
        int MeterInterval { get; }

        /// <summary>
        /// Current value of the meter.
        /// </summary>
        double CurrentValue { get; }

        /// <summary>
        /// Gets a current state of the meter.
        /// </summary>
        string GetState();

        /// <summary>
        /// Changes state of the meter.
        /// </summary>
        void ChangeState();

        /// <summary>
        /// Adds a new observer to the meter.
        /// </summary>
        void AddMeterObserver(IMeterObserver observer);

        /// <summary>
        /// Removes an observer from the meter.
        /// </summary>
        bool RemoveMeterObserver(IMeterObserver observer);

        /// <summary>
        /// Notifies all added observers.
        /// </summary>
        void NotifyMeterObservers(double val);
    }
}
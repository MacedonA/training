﻿using System.Runtime.Serialization;

namespace Task05.Storage
{
    /// <summary>
    /// Represents a class to store meters in the XML file.
    /// </summary>
    public class MetersStorageXml : FileStorage<MetersCollection>
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="fileName">Path to file with a storage.</param>
        public MetersStorageXml(string fileName) : base(fileName) { }

        /// <summary>
        /// Creates a serializer.
        /// </summary>
        protected override XmlObjectSerializer CreateSerializer()
        {
            return new DataContractSerializer(typeof(MetersCollection));
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace Task05.Storage
{
    /// <summary>
    /// Class to create a unique id.
    /// </summary>
    public class IdentifiersManager
    {
        private static IdentifiersManager _instance;

        /// <summary>
        /// An access to the single instance of the class.
        /// </summary>
        public static IdentifiersManager Instance
        {
            get { return _instance ?? (_instance = new IdentifiersManager()); }
        }

        /// <summary>
        /// Default constructor is hidden.
        /// </summary>
        private IdentifiersManager() { }

        /// <summary>
        /// Creates a unique id based on <see cref="usedIds"/>.
        /// </summary>
        /// <param name="usedIds">All used ids.</param>
        /// <returns>An id which does not exist in <see cref="usedIds"/>.</returns>
        public int CreateUniqueId(IEnumerable<int> usedIds)
        {
            var idsArray = usedIds.ToArray();
            return idsArray.Length == 0 ? default(int) : idsArray.Max() + 1;
        }
    }
}
﻿using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Task05.Storage
{
    /// <summary>
    /// Represents a class to store meters in the JSON file.
    /// </summary>
    public class MetersStorageJson : FileStorage<MetersCollection>
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="fileName">Path to file with a storage.</param>
        public MetersStorageJson(string fileName) : base(fileName) { }

        /// <summary>
        /// Creates a serializer.
        /// </summary>
        protected override XmlObjectSerializer CreateSerializer()
        {
            return new DataContractJsonSerializer(typeof(MetersCollection));
        }
    }
}
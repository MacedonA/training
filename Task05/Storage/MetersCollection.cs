﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Task05.Common;
using Task05.Meter;

namespace Task05.Storage
{
    /// <summary>
    /// Represents collection of meters.
    /// </summary>
    [DataContract]
    [KnownType(typeof(Meter.Meter))]
    public class MetersCollection : IMeterObserver
    {
        [DataMember]
        private readonly List<IMeter> _meters = new List<IMeter>();
        private Dictionary<int, double> _meterValues = new Dictionary<int, double>();
        private object _lockObj = new object();

        /// <summary>
        /// Set defauls after deserialized.
        /// </summary>
        [OnDeserialized]
        private void OnDeserialized(StreamingContext c)
        {
            _lockObj = new object();
            lock (_lockObj)
            {
                _meterValues = new Dictionary<int, double>();
                foreach (var meter in _meters)
                {
                    meter.AddMeterObserver(this);
                    _meterValues[meter.Id] = meter.CurrentValue;
                }
            }
        }

        /// <summary>
        /// Meters as read-only collection.
        /// It is required to do unit testing.
        /// </summary>
        public ReadOnlyCollection<IMeter> Meters { get { return _meters.AsReadOnly(); } }

        /// <summary>
        /// Creates a meter and adds it to collection.
        /// </summary>
        public void AddMeter(int meterInterval, MeterTypes meterType)
        {
            var newId = IdentifiersManager.Instance.CreateUniqueId(_meters.Select(meter => meter.Id));
            var newMeter = MeterFactory.CreateMeter(newId, meterInterval, meterType);
            _meters.Add(newMeter);
            newMeter.AddMeterObserver(this);
            lock (_lockObj)
            {
                _meterValues[newId] = newMeter.CurrentValue;
            }
        }

        /// <summary>
        /// Deletes a meter.
        /// </summary>
        public bool DeleteMeter(int id)
        {
            var toRemove = GetMeterById(id);
            if (toRemove == null)
                return false;
            toRemove.RemoveMeterObserver(this);
            return _meters.Remove(toRemove);
        }
        
        /// <summary>
        /// Changes a meter mode.
        /// </summary>
        public bool ChangeMeterState(int id)
        {
            var toSwitch = GetMeterById(id);
            if (toSwitch != null)
                toSwitch.ChangeState();
            return toSwitch != null;
        }

        /// <summary>
        /// Returns pairs of meter id and meter value.
        /// </summary>
        public Dictionary<int, double> GetMetersValues()
        {
            var result = new Dictionary<int, double>();
            lock (_lockObj)
            {
                foreach (var meterValue in _meterValues)
                {
                    result.Add(meterValue.Key, meterValue.Value);
                }
            }
            return result;
        }

        private IMeter GetMeterById(int id)
        {
            return _meters.FirstOrDefault(meter => meter.Id == id);
        }

        /// <summary>
        /// See <see cref="IMeterObserver.Update"/>.
        /// </summary>
        public void Update(int id, double val)
        {
            lock (_lockObj)
            {
                _meterValues[id] = val;
            }
        }
        
        /// <summary>
        /// Gets text info about all meters.
        /// </summary>
        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendLine(string.Format("Meters count: {0}", _meters.Count));
            _meters.ForEach(meter => result.AppendLine(meter.ToString()));
            return result.ToString();
        }
    }
}
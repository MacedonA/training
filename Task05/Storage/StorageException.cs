﻿using System;

namespace Task05.Storage
{
    /// <summary>
    /// Represents an exception which can be thrown by for storage code.
    /// </summary>
    public class StorageException : Exception
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        internal StorageException(string message, Exception innerException) : base(message, innerException) { }
    }
}
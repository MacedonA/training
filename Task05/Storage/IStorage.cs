﻿namespace Task05.Storage
{
    /// <summary>
    /// An interface to store or restore meters collection.
    /// </summary>
    public interface IStorage<T>
    {
        /// <summary>
        /// Stores meters in the storage.
        /// </summary>
        /// <param name="toStore">Meters to store.</param>
        /// <exception cref="StorageException">Throws this exception if file was not stored by any reason. Reason is placed in InnerException.</exception>
        void Store(T toStore);

        /// <summary>
        /// Restores meters from the storage.
        /// </summary>
        /// <returns>Meters from the storage.</returns>
        /// <exception cref="StorageException">Throws this exception if file was not stored by any reason. Reason is placed in InnerException.</exception>
        T Restore();
    }
}
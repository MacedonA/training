﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security;

namespace Task05.Storage
{
    /// <summary>
    /// Represents a class to store meters in the file.
    /// </summary>
    public abstract class FileStorage<T> : IStorage<T>
    {
        private XmlObjectSerializer _serializer;

        /// <summary>
        /// A serializer to store data. Should be created by descendant.
        /// </summary>
        protected XmlObjectSerializer Serializer { get { return _serializer ?? (_serializer = CreateSerializer()); } }

        /// <summary>
        /// File to store meters.
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="fileName">Path to file with a storage.</param>
        protected FileStorage(string fileName)
        {
            FileName = fileName;
        }

        /// <summary>
        /// See <see cref="IStorage{T}.Store"/>.
        /// </summary>
        public void Store(T toStore)
        {
            try
            {
                using (var file = new FileStream(FileName, FileMode.Create, FileAccess.Write))
                {
                    Serializer.WriteObject(file, toStore);
                }
            }
            catch (Exception exc)
            {
                if (exc is SerializationException ||
                    exc is ArgumentNullException ||
                    exc is ArgumentException ||
                    exc is NotSupportedException ||
                    exc is SecurityException ||
                    exc is DirectoryNotFoundException ||
                    exc is UnauthorizedAccessException ||
                    exc is PathTooLongException ||
                    exc is IOException)
                {
                    throw new StorageException(string.Format("Couldn't store data into file '{0}'.", FileName), exc);
                }
                throw;
            }
        }

        /// <summary>
        /// See <see cref="IStorage{T}.Store"/>.
        /// </summary>
        public T Restore()
        {
            try
            {
                using (var file = new FileStream(FileName, FileMode.Open, FileAccess.Read))
                {
                    return (T) Serializer.ReadObject(file);
                }
            }
            catch (Exception exc)
            {
                if (exc is SerializationException ||
                    exc is ArgumentNullException ||
                    exc is ArgumentException ||
                    exc is NotSupportedException ||
                    exc is FileNotFoundException ||
                    exc is SecurityException ||
                    exc is DirectoryNotFoundException ||
                    exc is UnauthorizedAccessException ||
                    exc is PathTooLongException ||
                    exc is IOException)
                {
                    throw new StorageException(string.Format("Couldn't restore data from file '{0}'.", FileName), exc);
                }
                throw;
            }
        }

        /// <summary>
        /// Creates a serializer.
        /// </summary>
        protected abstract XmlObjectSerializer CreateSerializer();
    }
}
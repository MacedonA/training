﻿using System;
using Task05.Common;

namespace Task05.Meter
{
    /// <summary>
    /// A factory to create meters.
    /// </summary>
    public static class MeterFactory
    {
        /// <summary>
        /// Create meter and sets several parameters for it.
        /// </summary>
        public static IMeter CreateMeter(int id, int meterInterval, MeterTypes meterType)
        {
            if (meterInterval <= 0)
                throw new ArgumentException("Meter interval should be greater than 0.");
            return new Meter(id) {MeterInterval = meterInterval, MeterType = meterType};
        }
    }
}
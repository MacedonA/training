﻿namespace Task05.Meter.State
{
    /// <summary>
    /// Represents interface to work with states in a meter.
    /// </summary>
    internal interface IMeterStateContext
    {
        /// <summary>
        /// Returns Wait state of the meter.
        /// </summary>
        IMeterState GetWaitState();

        /// <summary>
        /// Returns Calibration state of the meter.
        /// </summary>
        IMeterState GetCalibrationState();

        /// <summary>
        /// Returns GetData state of the meter.
        /// </summary>
        IMeterState GetGetDataState();

        /// <summary>
        /// Sets a new state for meter.
        /// </summary>
        void SetMeterState(IMeterState state);

        /// <summary>
        /// Meter interval property from meter.
        /// Can be used to setup timer.
        /// </summary>
        int MeterInterval { get; }
    }
}
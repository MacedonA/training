﻿using System.Threading;

namespace Task05.Meter.State
{
    /// <summary>
    /// Represents a Calibration state of the meter.
    /// </summary>
    internal class CalibrationMeterState : MeterStateBase
    {
        private int value;

        /// <summary>
        /// Constructor with context initialization.
        /// </summary>
        public CalibrationMeterState(IMeterStateContext context) : base(context) { }

        /// <summary>
        /// See <see cref="IMeterState.SwitchToNextState"/>.
        /// </summary>
        public override void SwitchToNextState()
        {
            Context.SetMeterState(Context.GetGetDataState());
        }

        /// <summary>
        /// See <see cref="IMeterState.SetupTimer"/>.
        /// </summary>
        public override void SetupTimer(Timer timer)
        {
            value = 0;
            timer.Change(0, 1000);
        }

        /// <summary>
        /// See <see cref="IMeterState.GetCurrentValue"/>.
        /// </summary>
        public override double GetCurrentValue()
        {
            var res = value;
            value++;
            return res;
        }

        /// <summary>
        /// Returns text identifier of the state.
        /// </summary>
        public override string ToString()
        {
            return "Calibration";
        }
    }
}
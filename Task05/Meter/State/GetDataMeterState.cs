﻿using System;
using System.Threading;

namespace Task05.Meter.State
{
    /// <summary>
    /// Represents a GetData state of the meter.
    /// </summary>
    internal class GetDataMeterState : MeterStateBase
    {
        private readonly Random _rnd = new Random();

        /// <summary>
        /// Constructor with context initialization.
        /// </summary>
        public GetDataMeterState(IMeterStateContext context) : base(context) { }

        /// <summary>
        /// See <see cref="IMeterState.SwitchToNextState"/>.
        /// </summary>
        public override void SwitchToNextState()
        {
            Context.SetMeterState(Context.GetWaitState());
        }

        /// <summary>
        /// See <see cref="IMeterState.SetupTimer"/>.
        /// </summary>
        public override void SetupTimer(Timer timer)
        {
            timer.Change(0, Context.MeterInterval);
        }

        /// <summary>
        /// See <see cref="IMeterState.GetCurrentValue"/>.
        /// </summary>
        public override double GetCurrentValue()
        {
            return _rnd.NextDouble() * 1000;
        }

        /// <summary>
        /// Returns text identifier of the state.
        /// </summary>
        public override string ToString()
        {
            return "GetData";
        }
    }
}
﻿using System.Threading;

namespace Task05.Meter.State
{
    /// <summary>
    /// Represents a state of the meter.
    /// </summary>
    internal interface IMeterState
    {
        /// <summary>
        /// Meter context attached to the state.
        /// </summary>
        IMeterStateContext Context { get; }

        /// <summary>
        /// Switches <see cref="Context"/> to the next state.
        /// </summary>
        void SwitchToNextState();

        /// <summary>
        /// Setups timer according to state.
        /// </summary>
        void SetupTimer(Timer timer);

        /// <summary>
        /// Gets current value.
        /// </summary>
        double GetCurrentValue();
    }
}
﻿using System.Threading;

namespace Task05.Meter.State
{
    /// <summary>
    /// Represents a Wait state of the meter.
    /// </summary>
    internal class WaitMeterState : MeterStateBase
    {
        /// <summary>
        /// Constructor with context initialization.
        /// </summary>
        public WaitMeterState(IMeterStateContext context) : base(context) { }

        /// <summary>
        /// See <see cref="IMeterState.SwitchToNextState"/>.
        /// </summary>
        public override void SwitchToNextState()
        {
            Context.SetMeterState(Context.GetCalibrationState());
        }

        /// <summary>
        /// See <see cref="IMeterState.SetupTimer"/>.
        /// </summary>
        public override void SetupTimer(Timer timer)
        {
            timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        /// <summary>
        /// See <see cref="IMeterState.GetCurrentValue"/>.
        /// </summary>
        public override double GetCurrentValue()
        {
            return double.NaN;
        }

        /// <summary>
        /// Returns text identifier of the state.
        /// </summary>
        public override string ToString()
        {
            return "Wait";
        }
    }
}
﻿using System;
using System.Threading;

namespace Task05.Meter.State
{
    /// <summary>
    /// Base functionality for all states.
    /// </summary>
    internal abstract class MeterStateBase : IMeterState
    {
        /// <summary>
        /// See <see cref="IMeterState.Context"/>.
        /// </summary>
        public IMeterStateContext Context { get; private set; }

        /// <summary>
        /// Constructor with context initialization.
        /// </summary>
        protected MeterStateBase(IMeterStateContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            Context = context;
        }

        /// <summary>
        /// See <see cref="IMeterState.SwitchToNextState"/>.
        /// </summary>
        public abstract void SwitchToNextState();

        /// <summary>
        /// See <see cref="IMeterState.SetupTimer"/>.
        /// </summary>
        public abstract void SetupTimer(Timer timer);

        /// <summary>
        /// See <see cref="IMeterState.GetCurrentValue"/>.
        /// </summary>
        public abstract double GetCurrentValue();
    }
}
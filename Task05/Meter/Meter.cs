﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading;
using Task05.Common;
using Task05.Meter.State;

namespace Task05.Meter
{
    /// <summary>
    /// Base impementation of the meter.
    /// </summary>
    [DataContract]
    internal class Meter : IMeter, IMeterStateContext
    {
        private IMeterState _activeState;
        private object _lockTimer;
        private double _currentValue;
        private IList<IMeterObserver> _meterObservers;

        /// <summary>
        /// See <see cref="IMeter.Id"/>.
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// See <see cref="IMeter.MeterType"/>.
        /// </summary>
        [DataMember]
        public MeterTypes MeterType { get; set; }

        /// <summary>
        /// See <see cref="IMeter.MeterInterval"/>.
        /// </summary>
        [DataMember]
        public int MeterInterval { get; set; }

        /// <summary>
        /// See <see cref="IMeter.CurrentValue"/>.
        /// </summary>
        public double CurrentValue
        {
            get
            {
                lock (_lockTimer)
                {
                    return _currentValue;                    
                }
            }
            private set
            {
                _currentValue = value;
                NotifyMeterObservers(_currentValue);
            }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        private Meter()
        {
            SetNonserializableDefaults();
        }
        
        /// <summary>
        /// Constructor with id.
        /// </summary>
        public Meter(int id) : this()
        {
            Id = id;
        }

        /// <summary>
        /// Set defauls after deserialized.
        /// </summary>
        [OnDeserialized]
        private void OnDeserialized(StreamingContext c)
        {
            SetNonserializableDefaults();
        }

        /// <summary>
        /// Set defaults after an object created via costructor or after it deserialized.
        /// </summary>
        private void SetNonserializableDefaults()
        {
            _lockTimer = new object();
            _currentValue = double.NaN;
            _waitState = new WaitMeterState(this);
            _calibrationState = new CalibrationMeterState(this);
            _getDataState = new GetDataMeterState(this);
            _timer = new Timer(TimerTick, null, Timeout.Infinite, Timeout.Infinite);
            _meterObservers = new List<IMeterObserver>();
            SetMeterState(_waitState);
        }

        /// <summary>
        /// Timer tick. Uses active state to populate a value.
        /// </summary>
        private void TimerTick(object state)
        {
            lock (_lockTimer)
            {
                CurrentValue = _activeState.GetCurrentValue();
            }
        }

        /// <summary>
        /// See <see cref="IDisposable.Dispose"/>.
        /// </summary>
        public void Dispose()
        {
            lock (_lockTimer)
            {
                if (_timer != null)
                {
                    _timer.Dispose();
                    _timer = null;
                }
                _meterObservers.Clear();
            }
        }

        /// <summary>
        /// See <see cref="IMeter.GetState"/>.
        /// </summary>
        public string GetState()
        {
            lock (_lockTimer)
            {
                return _activeState.ToString();
            }
        }

        /// <summary>
        /// See <see cref="IMeter.ChangeState"/>.
        /// </summary>
        public void ChangeState()
        {
            lock (_lockTimer)
            {
                _activeState.SwitchToNextState();
            }
        }

        /// <summary>
        /// See <see cref="IMeter.AddMeterObserver"/>.
        /// </summary>
        public void AddMeterObserver(IMeterObserver observer)
        {
            lock (_lockTimer)
            {
                _meterObservers.Add(observer);
            }
        }

        /// <summary>
        /// See <see cref="IMeter.RemoveMeterObserver"/>.
        /// </summary>
        public bool RemoveMeterObserver(IMeterObserver observer)
        {
            lock (_lockTimer)
            {
                return _meterObservers.Remove(observer);
            }
        }

        /// <summary>
        /// See <see cref="IMeter.NotifyMeterObservers"/>.
        /// </summary>
        public void NotifyMeterObservers(double val)
        {
            lock (_lockTimer)
            {
                foreach (var meterObserver in _meterObservers)
                    meterObserver.Update(Id, CurrentValue);
            }
        }

        /// <summary>
        /// Gets text info about a meter.
        /// </summary>
        public override string ToString()
        {
            lock (_lockTimer)
            {
                return string.Format("ID: {0}; Type: {1}; Interval: {2}; Current State: {3}",
                    Id, MeterType, MeterInterval, _activeState);
            }
        }

        #region IMeterStateContext Implementation
        private IMeterState _waitState;
        private IMeterState _calibrationState;
        private IMeterState _getDataState;
        private Timer _timer;

        /// <summary>
        /// See <see cref="IMeterStateContext.GetWaitState()"/>.
        /// </summary>
        public IMeterState GetWaitState()
        {
            return _waitState;
        }

        /// <summary>
        /// See <see cref="IMeterStateContext.GetCalibrationState()"/>.
        /// </summary>
        public IMeterState GetCalibrationState()
        {
            return _calibrationState;
        }

        /// <summary>
        /// See <see cref="IMeterStateContext.GetGetDataState()"/>.
        /// </summary>
        public IMeterState GetGetDataState()
        {
            return _getDataState;
        }

        /// <summary>
        /// See <see cref="IMeterStateContext.SetMeterState(IMeterState)"/>.
        /// </summary>
        public void SetMeterState(IMeterState state)
        {
            lock (_lockTimer)
            {
                //Stop timer before change it's state.
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
                _activeState = state;
                _activeState.SetupTimer(_timer);
            }
        }
        #endregion IMeterStateContext Implementation
    }
}
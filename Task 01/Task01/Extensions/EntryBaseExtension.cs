﻿using System;
using Task01.Classes;

namespace Task01.Extensions
{
    /// <summary>
    /// Contains extension methods for <see cref="EntryBase"/>.
    /// </summary>
    public static class EntryBaseExtension
    {
        #region Methods
        /// <summary>
        /// Creates an ID for the entry.
        /// </summary>
        /// <param name="entry">Entry to create id.</param>
        public static void CreateID(this EntryBase entry)
        {
            entry.ID = Guid.NewGuid();
        }
        #endregion
    }
}
﻿using System;
using Task01.Enums;
using Task01.Interfaces;

namespace Task01.Classes
{
    public class ReferenceMaterial : EntryBase, IEducationalMaterial
    {
        #region Properties
        /// <summary>
        /// Type of reference.
        /// </summary>
        public ReferenceTypes ReferenceType { get; private set; }

        /// <summary>
        /// URI of reference.
        /// </summary>
        public Uri Reference { get; private set; }
        #endregion

        #region Life Cycle
        /// <summary>
        /// Constuctor with full initialization.
        /// </summary>
        public ReferenceMaterial(string description, ReferenceTypes referenceType, Uri reference)
            : base(description)
        {
            if (reference == null)
                throw new ArgumentNullException("reference");
            ReferenceType = referenceType;
            Reference = reference;
        }
        #endregion

        #region Implementation of IEducationalMaterial
        /// <summary>
        /// See <see cref="IEducationalMaterial.CreateCopy"/>.
        /// </summary>
        /// <returns></returns>
        public IEducationalMaterial CreateCopy()
        {
            return new ReferenceMaterial(Description, ReferenceType, Reference);
        }
        #endregion
    }
}
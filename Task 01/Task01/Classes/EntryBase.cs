﻿using System;
using Task01.Extensions;

namespace Task01.Classes
{
    /// <summary>
    /// Represents a base entry data.
    /// </summary>
    public class EntryBase
    {
        #region Constants
        private const int MaxDescriptionLength = 256;
        #endregion

        #region Fields
        private readonly string _description;
        #endregion

        #region Properties
        /// <summary>
        /// Stores a unique identidfier for the entry.
        /// </summary>
        public Guid ID { get; internal set; }

        /// <summary>
        /// Stores a description for the entry.
        /// </summary>
        public string Description
        {
            get { return _description; }
        }
        #endregion        

        #region Life Cycle
        /// <summary>
        /// Constuctor with full initialization.
        /// </summary>
        public EntryBase(string description)
        {
            if (description == null)
                throw new ArgumentNullException("description");
            if (description.Length > MaxDescriptionLength)
                throw new ArgumentException(string.Format("Too long description. Maximal length is {0} characters.", MaxDescriptionLength));
            this.CreateID();
            _description = description;
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Gets a description of the entry.
        /// </summary>
        /// <returns>Description of the entry.</returns>
        public override string ToString()
        {
            return Description;
        }

        /// <summary>
        /// Compares entries by ID.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is EntryBase))
                return false;
            //May be we should take care that this.GetType()==obj.GetType() here. It is not clear by task text.
            return ID.Equals(((EntryBase)obj).ID);
        }

        /// <summary>
        /// Generates hash code of entry using ID.
        /// It was not requred to override this method but resharper suggests to do it if <see cref="Equals"/> method is overriden.
        /// </summary>
        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }
        #endregion
    }
}
﻿using System;
using Task01.Interfaces;

namespace Task01.Classes
{
    /// <summary>
    /// Represents a text material.
    /// </summary>
    public class TextMaterial : EntryBase, IEducationalMaterial
    {
        #region Constants
        private const int MaxTextLength = 10000;
        #endregion

        #region Fields
        private readonly string _text;
        #endregion

        #region Properties
        /// <summary>
        /// Text of the material.
        /// </summary>
        public string Text
        {
            get { return _text; }
        }
        #endregion

        #region Life Cycle
        /// <summary>
        /// Constuctor with full initialization.
        /// </summary>
        public TextMaterial(string description, string text)
            : base(description)
        {
            if (text == null)
                throw new ArgumentNullException("text");
            if (text.Length > MaxTextLength)
                throw new ArgumentException(string.Format("Too long text. Max length is {0} characters.", MaxTextLength));
            _text = text;
        }
        #endregion

        #region Implementation of IEducationalMaterial
        /// <summary>
        /// See <see cref="IEducationalMaterial.CreateCopy"/>.
        /// </summary>
        /// <returns></returns>
        public IEducationalMaterial CreateCopy()
        {
            return new TextMaterial(Description, Text);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using Task01.Enums;
using Task01.Interfaces;

namespace Task01.Classes
{
    /// <summary>
    /// Represents a lesson.
    /// </summary>
    public class Lesson : EntryVersionable, ICloneable
    {
        #region Properties
        /// <summary>
        /// Materials of the lesson.
        /// </summary>
        public IEducationalMaterial[] Materials { get; private set; } 
        #endregion

        #region Life Cycle
        /// <summary>
        /// Constuctor with full initialization.
        /// </summary>
        public Lesson(string description, byte[] version, IEducationalMaterial[] materials)
            : base(description, version)
        {
            if (materials == null)
                throw new ArgumentNullException("materials");
            Materials = materials;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets kind of lesson based on materials.
        /// </summary>
        /// <returns>Kind of the lesson.</returns>
        public KindsOfLesson KindOfLesson()
        {
            return Materials.Any(educationalMaterial => educationalMaterial is VideoMaterial)
                ? KindsOfLesson.VideoLesson
                : KindsOfLesson.TextLesson;
        }
        #endregion

        #region Implementation of ICloneable
        /// <summary>
        /// Creates a copy of the lesson.
        /// </summary>
        /// <returns>A copy of the lesson with copies of materials.</returns>
        public object Clone()
        {
            return new Lesson(Description, Version, Materials.Select(material => material.CreateCopy()).ToArray());
        }
        #endregion
    }
}

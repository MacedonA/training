﻿using System;
using Task01.Enums;
using Task01.Interfaces;

namespace Task01.Classes
{
    public class VideoMaterial : EntryVersionable, IEducationalMaterial
    {
        #region Properties
        /// <summary>
        /// Format of video.
        /// </summary>
        public VideoFormats VideoFormat { get; private set; }

        /// <summary>
        /// Reference for video.
        /// </summary>
        public Uri VideoReference { get; private set; }

        /// <summary>
        /// Reference for miniature.
        /// </summary>
        public Uri MiniatureReference { get; private set; }
        #endregion

        #region Life Cycle
        /// <summary>
        /// Constuctor with full initialization.
        /// </summary>
        public VideoMaterial(string description, byte[] version, VideoFormats videoFormat, Uri videoReference, Uri miniatureReference)
            : base(description, version)
        {
            if (videoReference == null)
                throw new ArgumentNullException("videoReference");
            if (miniatureReference == null)
                throw new ArgumentNullException("miniatureReference");
            VideoFormat = videoFormat;
            VideoReference = videoReference;
            MiniatureReference = miniatureReference;
        }
        #endregion

        #region Implementation of IEducationalMaterial
        /// <summary>
        /// See <see cref="IEducationalMaterial.CreateCopy"/>.
        /// </summary>
        /// <returns></returns>
        public IEducationalMaterial CreateCopy()
        {
            return new VideoMaterial(Description, Version, VideoFormat, VideoReference, MiniatureReference);
        }
        #endregion
    }
}
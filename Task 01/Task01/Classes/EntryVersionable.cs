﻿using System;
using Task01.Interfaces;

namespace Task01.Classes
{
    /// <summary>
    /// Represents an entry which stores a version.
    /// </summary>
    public class EntryVersionable : EntryBase, IVersionable
    {
        #region Fields
        private readonly byte[] _version = new byte[8];
        #endregion

        #region Properties
        /// <summary>
        /// See <see cref="IVersionable.Version"/>.
        /// </summary>
        public byte[] Version
        {
            get { return _version; }
        }
        #endregion

        #region Life Cycle
        /// <summary>
        /// Constuctor with full initialization.
        /// </summary>
        public EntryVersionable(string description, byte[] version)
            : base(description)
        {
            if (version == null)
                throw new ArgumentNullException("version");
            if (_version.Length != version.Length)
                throw new ArgumentException(string.Format("Version must contain {0} bytes.", version.Length));
            for (int i = 0; i < _version.Length; i++)
                _version[i] = version[i];             
        }
        #endregion
    }
}
﻿namespace Task01.Interfaces
{
    /// <summary>
    /// Represents any versionable entry.
    /// </summary>
    public interface IVersionable
    {
        #region Fields
        /// <summary>
        /// Version of the entry.
        /// </summary>
        byte[] Version { get; }
        #endregion
    }
}
﻿namespace Task01.Interfaces
{
    /// <summary>
    /// Represents any education material.
    /// </summary>
    public interface IEducationalMaterial
    {
        #region Metods
        /// <summary>
        /// Creates a copy of an education material.
        /// </summary>
        /// <returns></returns>
        IEducationalMaterial CreateCopy();
        #endregion
    }
}

﻿using Task01.Classes;

namespace Task01.Enums
{
    /// <summary>
    /// Represents available reference types for <see cref="ReferenceMaterial"/>.
    /// </summary>
    public enum ReferenceTypes
    {
        Local,
        Global
    }
}
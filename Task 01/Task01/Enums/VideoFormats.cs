﻿using Task01.Classes;

namespace Task01.Enums
{
    /// <summary>
    /// Represents available video types for <see cref="VideoMaterial"/>.
    /// </summary>
    public enum VideoFormats
    {
        Avi,
        Mkv,
        Mp4
    }
}
﻿using Task01.Classes;

namespace Task01.Enums
{
    /// <summary>
    /// Represents available reference types for <see cref="Lesson"/>.
    /// </summary>
    public enum KindsOfLesson
    {
        VideoLesson,
        TextLesson
    }
}
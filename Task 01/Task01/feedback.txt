﻿- Avoid defining public mutable properties and fields in models (see TODOs)
  This violates encapsulation / information hiding
  Instead use constructor arguments for data dependencies and methods for exposing logic / behavior

- Don't use constructors that initialize invalid objects (see TODOs)
  An object instance returned from a constructor should be valid and ready for use without additional initialization of null fields.
  Don't provide () if it's not possible to initialize the object correctly without arguments.

- Don't use null as a value for empty arrays/lists (see TODOs)
  Initialize such collections as new T[0] or new List<T>(). It will avoid potential null reference bugs without having to check for null every time.

- Don't modify the passed value in property setters (see TODOs)
  The user of a class expects that the value he specified will be saved without modification.
  Otherwise it creates conditions for a bug.
  If it's not possible to set the value because of a constraint then throw an exception indicating the violation.

- Avoid grouping files in the project based on a language features (Enums, Classes, etc). 
  Instead, use structure based on the meaning of types in the domain or based on layers of functionality.
  This will help to better understand the structure of a large project.
  For example, in this project you could have one namespace for types related to Lesson and Materials, another one for base types, and put everything else in the root namespace

- Do not use regions (especially in such small files) they make it very hard to see the code.
  Regions are meant to be used to hide sections of auto-generated code or to work with very long files


﻿using System;

namespace Task03
{
    /// <summary>
    /// Is used to log data to console.
    /// </summary>
    internal static class Logger
    {
        private static readonly object ConsoleLock = new object();

        /// <summary>
        /// Logs a message to console. Adds time data to message's text.
        /// </summary>
        internal static void LogMessage(string message)
        {
            lock (ConsoleLock)
            {
                Console.Write("{0}: ", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                Console.WriteLine(message);
            }
        }
    }
}

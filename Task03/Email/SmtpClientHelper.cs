﻿using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Task03.Email
{
    /// <summary>
    /// Stores email settings.
    /// </summary>
    public class SmtpClientHelper
    {
        private static readonly object ClientLock = new object();
        private readonly SmtpClient _client = new SmtpClient();
        private string _fromAddress;
        private string _toAddress;
        private bool _initialized;
        private static SmtpClientHelper _instance;

        /// <summary>
        /// Hide constructor to implement singleton pattern.
        /// </summary>
        private SmtpClientHelper() { }

        /// <summary>
        /// An instance of smtp client.
        /// </summary>
        public static SmtpClientHelper Instance { get { return _instance ?? (_instance = new SmtpClientHelper()); } }

        /// <summary>
        /// Initializes client from app.config.
        /// </summary>
        public void Initialize()
        {
            if (_initialized)
                return;
            var appSet = ConfigurationManager.AppSettings;
            _client.Host = appSet["SmtpDefaultServerName"];
            _client.Credentials = new NetworkCredential(appSet["SmtpDefaultAccountName"], SecurePassword.DecryptString(appSet["SmtpDefaultAccountPassword"]));
            _client.Port = int.Parse(appSet["SmtpDefaultPort"]);
            _client.EnableSsl = bool.Parse(appSet["SmtpEnableSSL"]);
            _client.Timeout = int.Parse(appSet["SmtpDefaultTimeout"]) * 1000;
            _fromAddress = appSet["DefaultEmailFromAddress"];
            _toAddress = appSet["DefaultEmailToAddress"];
            _initialized = true;
        }

        /// <summary>
        /// Tries to send a email.
        /// </summary>
        public Task<bool> SendAsync(string subject, string message)
        {
            return Task.Run(() =>
            {
                if (!_initialized)
                    return false;
                try
                {
                    lock (ClientLock)
                    {
                        _client.Send(_fromAddress, _toAddress, subject, message);
                    }
                    return true;
                }
                catch (SmtpException e)
                {
                    Logger.LogMessage(e.ToString());
                    return false;   
                }                
            });
        }
    }
}
﻿using System;
using System.Security;

namespace Task03.Email
{
    /// <summary>
    /// Is used to encode/decode a password.
    /// Follow this link to get more info: http://weblogs.asp.net/jongalloway/encrypting-passwords-in-a-net-app-config-file.
    /// </summary>
    public static class SecurePassword
    {
        private static readonly byte[] entropy = System.Text.Encoding.Unicode.GetBytes("{2E7FC415-E25F-4C67-9E02-E28C27008F68}");

        /// <summary>
        /// Converts a secure string to a string which can be saved to app.config.
        /// Uses <see cref="entropy"/> to protect data.
        /// </summary>
        public static string EncryptString(SecureString input)
        {
            byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(
                System.Text.Encoding.Unicode.GetBytes(ToInsecureString(input)),
                entropy,
                System.Security.Cryptography.DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedData);
        }

        /// <summary>
        /// Converts a string from app.config to secure string to set as password.
        /// Uses <see cref="entropy"/> to unprotect data.
        /// </summary>
        public static SecureString DecryptString(string encryptedData)
        {
            try
            {
                byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(
                    Convert.FromBase64String(encryptedData),
                    entropy,
                    System.Security.Cryptography.DataProtectionScope.CurrentUser);
                return ToSecureString(System.Text.Encoding.Unicode.GetString(decryptedData));
            }
            catch
            {
                return new SecureString();
            }
        }

        /// <summary>
        /// Converts string whch came from user to use it as secure string.
        /// </summary>
        public static SecureString ToSecureString(string input)
        {
            SecureString secure = new SecureString();
            foreach (char c in input)
            {
                secure.AppendChar(c);
            }
            secure.MakeReadOnly();
            return secure;
        }

        private static string ToInsecureString(SecureString input)
        {
            string returnValue;
            IntPtr ptr = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(input);
            try
            {
                returnValue = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(ptr);
            }
            return returnValue;
        }         
    }
}
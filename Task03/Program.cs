﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Task03.Email;
using Task03.Monitor;

namespace Task03
{
    class Program
    {
        /// <summary>
        /// Application name to avoid more than one application instanse.
        /// </summary>
        private const string AppName = "SiteMonitorApplication";

        /// <summary>
        /// Reads config and monitors data.
        /// </summary>
        static void Main(string[] args)
        {
            //Only one instance of application is available.
            if (!IsSingleInstance())
            {
                Console.WriteLine("The instance of this application is already running.");
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                return;
            }
            
            //Initialize SmtpClient using app.config.
            try { SmtpClientHelper.Instance.Initialize(); }
            catch (TypeInitializationException e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                return;
            }

            //Initialize tasks using app.config.
            string error;
            var siteMonitors = GetSiteMonitors(out error);
            if (siteMonitors.Length == 0)
            {
                Console.WriteLine(string.IsNullOrEmpty(error) ? "Sites were not found." : error);
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                return;
            }

            while (true)
            {
                Thread.Sleep(1000);
                var monitorTasks = siteMonitors.Select(siteMonitor => siteMonitor.MonitorAsync()).ToArray();
                Task.WaitAll(monitorTasks);
            }
        }

        private static bool IsSingleInstance()
        {
            bool tryCreateNewApp;
            var mutexInstance = new Mutex(true, AppName, out tryCreateNewApp);
            return tryCreateNewApp;
        }

        private static SiteMonitor[] GetSiteMonitors(out string error)
        {
            error = null;
            try
            {
                var config = ConfigurationManager.GetSection("sites") as SitesSection;
                return config == null ? new SiteMonitor[0] : config.Instances.OfType<SiteElement>().Select(siteSetting => (new SiteMonitor(siteSetting))).ToArray();
            }
            catch (ConfigurationErrorsException e)
            {
                error = e.ToString();
            }
            catch (ArgumentException e)
            {
                error = e.ToString();
            }
            return new SiteMonitor[0];
        }
    }
}

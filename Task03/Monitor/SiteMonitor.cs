﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Task03.Email;

namespace Task03.Monitor
{
    /// <summary>
    /// Monitors a single site to make sure that it is responsible.
    /// </summary>
    public class SiteMonitor
    {
        private DateTime _lastMonitorTime = DateTime.MinValue;
        private bool _reuquestInProgress;

        /// <summary>
        /// A reference to the site.
        /// </summary>
        public string SiteReference { get; private set; }

        /// <summary>
        /// Timeout to wait response from the site.
        /// </summary>
        public int WaitTimeout { get; private set; }

        /// <summary>
        /// Interval between check attempts.
        /// </summary>
        public int RunInterval { get; private set; }

        /// <summary>
        /// Constructor from settings.
        /// </summary>
        public SiteMonitor(SiteElement siteElement)
        {
            if (string.IsNullOrEmpty(siteElement.SiteReference))
                throw new ArgumentException("Reference is invalid.");
            if (siteElement.RunInterval <= 0)
                throw new ArgumentException(string.Format("Run interval value is {0}. It must be greater than zero.", siteElement.RunInterval));
            if (siteElement.WaitTimeout <= 0)
                throw new ArgumentException(string.Format("Wait timeout value is {0}. It must be greater than zero.", siteElement.WaitTimeout));
            SiteReference = siteElement.SiteReference;
            WaitTimeout = siteElement.WaitTimeout;
            RunInterval = siteElement.RunInterval;
        }

        /// <summary>
        /// Creates a task to monitor current site and starts it.
        /// </summary>
        public Task MonitorAsync()
        {
            return Task.Run(() => Monitor());
        }

        /// <summary>
        /// A method to monitor site responsibility.
        /// </summary>
        private void Monitor()
        {
            if (_reuquestInProgress)
                return;
            var timeDiff = DateTime.Now - _lastMonitorTime;
            if (timeDiff.TotalSeconds < RunInterval)
                return;
            ReportResponsibitlity(CheckSitResponsibitlity());
        }

        /// <summary>
        /// A method to monitor site responsibility.
        /// </summary>
        /// <returns>True if request was sent.</returns>
        private string CheckSitResponsibitlity()
        {
            _reuquestInProgress = true;
            string error = null;
            try
            {
                Logger.LogMessage(string.Format("Processing request to '{0}'.", SiteReference));
                WebRequest request = WebRequest.Create(SiteReference);
                request.Timeout = WaitTimeout*1000;
                try
                {
                    using (request.GetResponse())
                    {
                        //May be we should process responce to make sure that it is valid but I'm not sure how.
                    }
                }
                catch (WebException webEx)
                {
                    error = webEx.ToString();
                }
                _lastMonitorTime = DateTime.Now;
            }
            finally
            {
                _reuquestInProgress = false;
            }

            return error;
        }

        /// <summary>
        /// Reports about responsibility. Sends a email.
        /// </summary>
        private async void ReportResponsibitlity(string error)
        {
            if (!string.IsNullOrEmpty(error))
            {
                var subject = string.Format("Site '{0}' does not responce", SiteReference);
                var message = string.Format("The following error has been occurred:\n{0}", error);
                var sendTask = SmtpClientHelper.Instance.SendAsync(subject, message);
                Logger.LogMessage(string.Format("{0}. {1}", subject, message));
                if (!await sendTask)
                    Logger.LogMessage("Could not send email.");
            }
            else
                Logger.LogMessage(string.Format("Response from '{0}' was received.", SiteReference));
        }
    }
}
﻿using System.Configuration;

namespace Task03.Monitor
{
    /// <summary>
    /// Section of the app.config filr to store different sites with settings.
    /// </summary>
    public class SitesSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
        public SitesCollection Instances
        {
            get { return (SitesCollection)this[""]; }
            set { this[""] = value; }
        }
    }

    /// <summary>
    /// Collection of the sites from configuration file.
    /// </summary>
    public class SitesCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new SiteElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SiteElement)element).SiteReference;
        }
    }

    /// <summary>
    /// Single site from configuration.
    /// </summary>
    public class SiteElement : ConfigurationElement
    {
        [ConfigurationProperty("siteReference", IsKey = true, IsRequired = true)]
        public string SiteReference
        {
            get { return (string)base["siteReference"]; }
            set { base["siteReference"] = value; }
        }

        [ConfigurationProperty("waitTimeout", IsRequired = true)]
        public int WaitTimeout
        {
            get { return (int)this["waitTimeout"]; }
            set { this["waitTimeout"] = value; }
        }

        [ConfigurationProperty("runInterval", IsRequired = true)]
        public int RunInterval
        {
            get { return (int)this["runInterval"]; }
            set { this["runInterval"] = value; }
        }
    }
}
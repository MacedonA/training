﻿DELETE FROM [dbo].[MedcineForms]
SET IDENTITY_INSERT [MedcineForms] ON
INSERT INTO [dbo].[MedcineForms] ([MedcineFormId], [MedcineFormName]) VALUES (1, 'Таблетки')
INSERT INTO [dbo].[MedcineForms] ([MedcineFormId], [MedcineFormName]) VALUES (2, 'Жидкость наружного применения')
INSERT INTO [dbo].[MedcineForms] ([MedcineFormId], [MedcineFormName]) VALUES (3, 'Сироп')
INSERT INTO [dbo].[MedcineForms] ([MedcineFormId], [MedcineFormName]) VALUES (4, 'Мазь')
SET IDENTITY_INSERT [MedcineForms] OFF
﻿DELETE FROM [dbo].[Users]
DELETE FROM [dbo].[Roles]

SET IDENTITY_INSERT [Roles] ON
INSERT INTO [dbo].[Roles] 
([RoleId], [Name], [InternalName]) 
VALUES (1, 'Администратор', 'admininstrator')
INSERT INTO [dbo].[Roles] 
([RoleId], [Name], [InternalName]) 
VALUES (2, 'Пользователь', 'user')
SET IDENTITY_INSERT [Roles] OFF

SET IDENTITY_INSERT [Users] ON
INSERT INTO [dbo].[Users] 
([UserId], [Login], [Password], [RoleId]) 
VALUES (1, 'admin', '123456', 1)
SET IDENTITY_INSERT [Users] OFF
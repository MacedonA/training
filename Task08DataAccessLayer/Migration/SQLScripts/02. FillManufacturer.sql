﻿DELETE FROM [dbo].[Manufacturers]
SET IDENTITY_INSERT [Manufacturers] ON
INSERT INTO [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName]) VALUES (1, 'Bauer')
INSERT INTO [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName]) VALUES (2, 'БелФарм')
INSERT INTO [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName]) VALUES (3, 'РосФарм')
INSERT INTO [dbo].[Manufacturers] ([ManufacturerId], [ManufacturerName]) VALUES (4, 'МонголФарм')
SET IDENTITY_INSERT [Manufacturers] OFF
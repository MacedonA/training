﻿DELETE FROM [dbo].[Medcines]
SET IDENTITY_INSERT [Medcines] ON

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (1, 2, NULL, 4, 1, NULL, 'Лекарство №1')

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (3, 3, 'Полезно', 2, 2, NULL, 'Что-то полезное')

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (2, 1, NULL, 1, 3, NULL, 'Таблеточки')

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (5, 2, NULL, 3, 4, NULL, 'Оттакмвон')

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (4, 4, 'Очень полезно', 3, 5, NULL, 'Недопересыпазм')

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (3, 4, NULL, 1, 6, NULL, 'Антананариву')

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (1, 3, 'Сверх полезно', 1, 7, NULL, 'Лекарство №3.14')

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (1, 1, NULL, 4, 8, NULL, 'Для палаты №6')

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (2, 1, 'Бесполезно', 4, 9, NULL, 'Сложно что-то придумать')

INSERT INTO [dbo].[Medcines] 
([ActiveSubstanceId], [ManufacturerId], [MedcineAnnotation], [MedcineFormId], [MedcineId], [MedcineImageId], [MedcineName]) 
VALUES (4, 2, 'Бесполезно и немного вредит', 1, 10, NULL, 'Лекарство №10.5')

SET IDENTITY_INSERT [Medcines] OFF
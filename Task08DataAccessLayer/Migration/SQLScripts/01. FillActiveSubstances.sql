﻿DELETE FROM [dbo].[ActiveSubstances]
SET IDENTITY_INSERT [ActiveSubstances] ON
INSERT INTO [dbo].[ActiveSubstances] ([ActiveSubstanceId], [ActiveSubstanceName]) VALUES (1, 'Активированный уголь')
INSERT INTO [dbo].[ActiveSubstances] ([ActiveSubstanceId], [ActiveSubstanceName]) VALUES (2, 'Аспирин')
INSERT INTO [dbo].[ActiveSubstances] ([ActiveSubstanceId], [ActiveSubstanceName]) VALUES (3, 'Парацетомол')
INSERT INTO [dbo].[ActiveSubstances] ([ActiveSubstanceId], [ActiveSubstanceName]) VALUES (4, 'Нитроглицирин')
INSERT INTO [dbo].[ActiveSubstances] ([ActiveSubstanceId], [ActiveSubstanceName]) VALUES (5, 'Озверин')
SET IDENTITY_INSERT [ActiveSubstances] OFF
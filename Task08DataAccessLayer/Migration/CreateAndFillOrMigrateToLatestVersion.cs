﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using Task08DataAccessLayer.Model;

namespace Task08DataAccessLayer.Migration
{
    /// <summary>
    /// Migration support for database.
    /// </summary>
    internal class CreateAndFillOrMigrateToLatestVersion : MigrateDatabaseToLatestVersion<MedcineDbContext, MigrationConfiguration>
    {
        /// <summary>
        /// Creates a database and fills it by SQL scripts if required.
        /// Also supports a migration.
        /// </summary>
        /// <exception cref="ApplicationException">Can be thrown if data ib sql scripts is invalid.</exception>
        public override void InitializeDatabase(MedcineDbContext context)
        {
            CreateAndFillDatabase(context);
            base.InitializeDatabase(context);
        }

        /// <summary>
        /// Creates and tries to fill a database.
        /// </summary>
        /// <exception cref="ApplicationException">Can be thrown if data ib sql scripts is invalid.</exception>
        public void CreateAndFillDatabase(MedcineDbContext context)
        {
            if (context.Database.Exists())
                return;

            //1. Create database.
            context.Database.Create();
            bool result = true;

            //2. Set cyrylic collation for database.
            try
            {
                using (SqlConnection conn = new SqlConnection(context.Database.Connection.ConnectionString))
                using (SqlCommand command = new SqlCommand(string.Format("ALTER DATABASE [{0}] COLLATE Cyrillic_General_CI_AS", context.Database.Connection.Database), conn))
                {
                    conn.Open();
                    command.ExecuteNonQuery();
                }
                SqlConnection.ClearAllPools();
            }
            catch (Exception)
            {
                result = false;
            }

            //3. Run all sql files from dll.
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            if (result)
            {
                var files = thisAssembly.GetManifestResourceNames().Where(name => name.EndsWith(".sql")).OrderBy(name => name).ToList();
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var file in files)
                        {
                            using (Stream s = thisAssembly.GetManifestResourceStream(file))
                            using (StreamReader sr = new StreamReader(s))
                            {
                                context.Database.ExecuteSqlCommand(sr.ReadToEnd());
                            }
                        }
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        result = false;
                    }
                }
            }

            //4. Insert all images from dll (if possible).
            if (result)
            {
                var sql =
                    "IF EXISTS(SELECT 1 FROM Medcines WHERE MedcineId=@id) BEGIN " +
                    "INSERT INTO MedcineImages (Image) VALUES (@Image) " +
                    "DECLARE @newId INT SELECT @newId=SCOPE_IDENTITY() " +
                    "UPDATE Medcines SET MedcineImageId=@newId WHERE MedcineId=@id END";
                var files = thisAssembly.GetManifestResourceNames().Where(name => name.EndsWith(".jpg")).OrderBy(name => name).ToList();
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var file in files)
                        {
                            var idStr = file.Split('.');
                            int id;
                            if (idStr.Length < 2 || !int.TryParse(idStr[idStr.Length - 2], out id))
                                continue;
                            byte[] image;

                            using (Stream s = thisAssembly.GetManifestResourceStream(file))
                            using (BinaryReader sr = new BinaryReader(s))
                            {
                                image = sr.ReadBytes((int)s.Length);
                            }
                            context.Database.ExecuteSqlCommand(sql, 
                                new SqlParameter("@id", SqlDbType.Int) { Value = id },
                                new SqlParameter("@Image", SqlDbType.Image, image.Length) { Value = image });
                        }
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        result = false;
                    }
                }                
            }
            if (result) 
                return;

            //If something goes wrong delete created database.
            context.Database.Connection.Close();
            Database.Delete(context.Database.Connection.ConnectionString);
            throw new ApplicationException("SQL files are not is not valid.");
        } 
    }
}
﻿using System.Data.Entity.Migrations;
using Task08DataAccessLayer.Model;

namespace Task08DataAccessLayer.Migration
{
    /// <summary>
    /// A config to migrate database if model changed.
    /// </summary>
    internal class MigrationConfiguration : DbMigrationsConfiguration<MedcineDbContext>
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
        }     
    }
}
﻿using System;
using System.Data.Entity;
using Task08DataAccessLayer.BaseTypes;
using Task08DataAccessLayer.Entities;
using Task08DataAccessLayer.Migration;

namespace Task08DataAccessLayer.Model
{
    /// <summary>
    /// Implements <see cref="IMedcineDbContext"/>.
    /// </summary>
    public class MedcineDbContext : DbContext, IMedcineDbContext
    {
        /// <summary>
        /// Connection string to database.
        /// </summary>
        internal const string ConnectionStringName = "Task08TestDb";

        /// <summary>
        /// Default constructor.
        /// Creates a database if it does not exists.
        /// </summary>
        /// <exception cref="ApplicationException">Can be thrown if data ib sql scripts is invalid.</exception>
        public MedcineDbContext()
            : base(ConnectionStringName)
        {
            var initializer = new CreateAndFillOrMigrateToLatestVersion();
            Database.SetInitializer(initializer);
            initializer.CreateAndFillDatabase(this);
        }

        /// <summary>
        /// Medcines DbSet.
        /// </summary>
        public DbSet<Medcine> Medcines { get; set; }

        /// <summary>
        /// MedcineForms DbSet.
        /// </summary>
        public DbSet<MedcineForm> MedcineForms { get; set; }

        /// <summary>
        /// MedcineImages DbSet.
        /// </summary>
        public DbSet<MedcineImage> MedcineImages { get; set; }

        /// <summary>
        /// Manufacturers DbSet.
        /// </summary>
        public DbSet<Manufacturer> Manufacturers { get; set; }

        /// <summary>
        /// ActiveSubstances DbSet.
        /// </summary>
        public DbSet<ActiveSubstance> ActiveSubstances { get; set; }

        /// <summary>
        /// Orders DbSet.
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Roles DbSet.
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// Users DbSet.
        /// </summary>
        public DbSet<User> Users { get; set; }
    }
}
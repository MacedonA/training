﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Task08DataAccessLayer.Entities
{
    /// <summary>
    /// An instance of medcine image.
    /// </summary>
    public class MedcineImage
    {
        /// <summary>
        /// Id of medcine image.
        /// </summary>
        [Key]
        public int MedcineImageId { get; set; }

        /// <summary>
        /// Image of medcine.
        /// </summary>
        [Column(TypeName = "image")]
        public byte[] Image { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Task08DataAccessLayer.Entities
{
    /// <summary>
    /// Represents a user of application.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Id of user.
        /// </summary>
        [Key]
        public int UserId { get; set; }

        /// <summary>
        /// Id of role for user.
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// Role of user.
        /// </summary>
        public virtual Role Role { get; set; }

        /// <summary>
        /// Login of user.
        /// </summary>
        [StringLength(150)]
        public string Login { get; set; }

        /// <summary>
        /// Password of user.
        /// </summary>
        [StringLength(150)]
        public string Password { get; set; }
    }
}
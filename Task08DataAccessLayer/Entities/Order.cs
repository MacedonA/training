﻿using System.ComponentModel.DataAnnotations;

namespace Task08DataAccessLayer.Entities
{
    public class Order
    {
        /// <summary>
        /// Id of order.
        /// </summary>
        [Key]
        public int OrderId { get; set; }

        /// <summary>
        /// A medcine to order.
        /// </summary>
        public int MedcineId { get; set; }

        /// <summary>
        /// A reference to medcine.
        /// </summary>
        public virtual Medcine Medcine { get; set; }

        /// <summary>
        /// A customer first name.
        /// </summary>
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        /// <summary>
        /// A customer last name.
        /// </summary>
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        /// <summary>
        /// A customer phone number.
        /// </summary>
        [Required]
        [StringLength(30)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// A customer adderss.
        /// </summary>
        [Required]
        [StringLength(1000)]
        public string Address { get; set; }

        /// <summary>
        /// A customer email.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string EMailAddress { get; set; }

        /// <summary>
        /// Additional comments from customer.
        /// </summary>
        [StringLength(4000)]
        public string Comments { get; set; }
    }
}

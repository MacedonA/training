﻿using System.ComponentModel.DataAnnotations;

namespace Task08DataAccessLayer.Entities
{
    /// <summary>
    /// An instance of medcine.
    /// </summary>
    public class Medcine
    {
        /// <summary>
        /// Id of medcine.
        /// </summary>
        [Key]
        public int MedcineId { get; set; }

        /// <summary>
        /// Name of medcine.
        /// </summary>
        [Required]
        [StringLength(150)]
        public string MedcineName { get; set; }

        /// <summary>
        /// Annotation of medcine.
        /// </summary>
        [StringLength(4000)]
        public string MedcineAnnotation { get; set; }

        /// <summary>
        /// Id of medcine form.
        /// </summary>
        public int MedcineFormId { get; set; }

        /// <summary>
        /// A reference to medcine form.
        /// </summary>
        public virtual MedcineForm MedcineForm { get; set; }

        /// <summary>
        /// Id of medcine image.
        /// </summary>
        public int? MedcineImageId { get; set; }

        /// <summary>
        /// A reference to medcine image.
        /// </summary>
        public virtual MedcineImage MedcineImage { get; set; }

        /// <summary>
        /// Id of substance.
        /// </summary>
        public int ActiveSubstanceId { get; set; }

        /// <summary>
        /// A reference to substance.
        /// </summary>
        public virtual ActiveSubstance ActiveSubstance { get; set; }

        /// <summary>
        /// Id of manufacturer.
        /// </summary>
        public int ManufacturerId { get; set; }

        /// <summary>
        /// A reference to manufacturer.
        /// </summary>
        public virtual Manufacturer Manufacturer { get; set; }
    }
}
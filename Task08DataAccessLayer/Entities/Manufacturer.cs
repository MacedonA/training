﻿using System.ComponentModel.DataAnnotations;

namespace Task08DataAccessLayer.Entities
{
    /// <summary>
    /// An instance of manufacturer type.
    /// </summary>
    public class Manufacturer
    {
        /// <summary>
        /// Id of manufacturer.
        /// </summary>
        [Key]
        public int ManufacturerId { get; set; }

        /// <summary>
        /// Name of manufacturer.
        /// </summary>
        [Required]
        [StringLength(150)]
        public string ManufacturerName { get; set; }         
    }
}
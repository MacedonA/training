﻿using System.ComponentModel.DataAnnotations;

namespace Task08DataAccessLayer.Entities
{
    /// <summary>
    /// An instance of medcine form.
    /// </summary>
    public class MedcineForm
    {
        /// <summary>
        /// Id of medcine form.
        /// </summary>
        [Key]
        public int MedcineFormId { get; set; }

        /// <summary>
        /// Name of medcine form.
        /// </summary>
        [Required]
        [StringLength(150)]
        public string MedcineFormName { get; set; }
    }
}

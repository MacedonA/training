﻿using System.ComponentModel.DataAnnotations;

namespace Task08DataAccessLayer.Entities
{
    /// <summary>
    /// An instance of substance type.
    /// </summary>
    public class ActiveSubstance
    {
        /// <summary>
        /// Id of substance.
        /// </summary>
        [Key]
        public int ActiveSubstanceId { get; set; }

        /// <summary>
        /// Name of substance.
        /// </summary>
        [Required]
        [StringLength(150)]
        public string ActiveSubstanceName { get; set; }
    }
}

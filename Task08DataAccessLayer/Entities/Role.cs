﻿using System.ComponentModel.DataAnnotations;

namespace Task08DataAccessLayer.Entities
{
    /// <summary>
    /// Represents a role of user of application.
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Id of role.
        /// </summary>
        [Key]
        public int RoleId { get; set; }

        /// <summary>
        /// Name of the role.
        /// </summary>
        [StringLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// Internal name of the role.
        /// </summary>
        [StringLength(150)]
        public string InternalName { get; set; }
    }
}

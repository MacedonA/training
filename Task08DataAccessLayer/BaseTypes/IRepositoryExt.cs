﻿namespace Task08DataAccessLayer.BaseTypes
{
    /// <summary>
    /// A generic repository pattern that wraps the IDbSet interface.
    /// Allows to change data.
    /// </summary>
    public interface IRepositoryExt<T> : IRepository<T> where T : class
    {
        /// <summary>
        /// Inserts a single entity into the DbSet
        /// </summary>
        void Insert(T entity);

        /// <summary>
        /// Updates a single entity in the DbSet
        /// </summary>
        void Update(T entity);

        /// <summary>
        /// Deletes a single entity from the DbSet
        /// </summary>
        void Delete(T entity);
    }
}
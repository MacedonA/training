﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task08DataAccessLayer.Entities;

namespace Task08DataAccessLayer.BaseTypes
{
    /// <summary>
    /// Implements <see cref="IUnitOfWork"/>.
    /// </summary>
    internal class UnitOfWork<TContext> : IUnitOfWork where TContext : IMedcineDbContext, new()
    {
        private readonly IMedcineDbContext _dbContext;
        private bool _disposed;
        private readonly Dictionary<Type, object> _repositories;
        private IRepositoryExt<Medcine> _medcinesRepository;
        private IRepository<MedcineForm> _medcineFormsRepository;
        private IRepositoryExt<MedcineImage> _medcineImagesRepository;
        private IRepository<Manufacturer> _manufacturersRepository;
        private IRepository<ActiveSubstance> _activeSubstancesRepository;
        private IRepositoryExt<Order> _ordersRepository;
        private IRepositoryExt<Role> _rolesRepository;
        private IRepositoryExt<User> _usersRepository;

        /// <summary>
        /// See <see cref="IUnitOfWork.MedcinesRepository"/>.
        /// </summary>
        public IRepositoryExt<Medcine> MedcinesRepository { get { return _medcinesRepository ?? (_medcinesRepository = new Repository<Medcine>(_dbContext)); } }

        /// <summary>
        /// See <see cref="IUnitOfWork.MedcineFormsRepository"/>.
        /// </summary>
        public IRepository<MedcineForm> MedcineFormsRepository { get { return _medcineFormsRepository ?? (_medcineFormsRepository = new Repository<MedcineForm>(_dbContext)); } }

        /// <summary>
        /// See <see cref="IUnitOfWork.MedcineImagesRepository"/>.
        /// </summary>
        public IRepositoryExt<MedcineImage> MedcineImagesRepository { get { return _medcineImagesRepository ?? (_medcineImagesRepository = new Repository<MedcineImage>(_dbContext)); } }

        /// <summary>
        /// See <see cref="IUnitOfWork.ManufacturersRepository"/>.
        /// </summary>
        public IRepository<Manufacturer> ManufacturersRepository { get { return _manufacturersRepository ?? (_manufacturersRepository = new Repository<Manufacturer>(_dbContext)); } }

        /// <summary>
        /// See <see cref="IUnitOfWork.ActiveSubstancesRepository"/>.
        /// </summary>
        public IRepository<ActiveSubstance> ActiveSubstancesRepository { get { return _activeSubstancesRepository ?? (_activeSubstancesRepository = new Repository<ActiveSubstance>(_dbContext)); } }

        /// <summary>
        /// See <see cref="IUnitOfWork.OrdersRepository"/>.
        /// </summary>
        public IRepositoryExt<Order> OrdersRepository { get { return _ordersRepository ?? (_ordersRepository = new Repository<Order>(_dbContext)); } }

        /// <summary>
        /// See <see cref="IUnitOfWork.RolesRepository"/>.
        /// </summary>
        public IRepositoryExt<Role> RolesRepository { get { return _rolesRepository ?? (_rolesRepository = new Repository<Role>(_dbContext)); } }

        /// <summary>
        /// See <see cref="IUnitOfWork.UsersRepository"/>.
        /// </summary>
        public IRepositoryExt<User> UsersRepository { get { return _usersRepository ?? (_usersRepository = new Repository<User>(_dbContext)); } }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public UnitOfWork()
        {
            _dbContext = new TContext();
            _repositories = new Dictionary<Type, object>();
            _disposed = false;
        }

        /// <summary>
        /// Returns a repository by type. If it does not exist creates it.
        /// </summary>
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (_repositories.Keys.Contains(typeof(TEntity)))
                return _repositories[typeof(TEntity)] as IRepository<TEntity>;
            var repository = new Repository<TEntity>(_dbContext);
            _repositories.Add(typeof(TEntity), repository);
            return repository;
        }

        /// <summary>
        /// See <see cref="IUnitOfWork.Commit"/>.
        /// </summary>
        public virtual void Commit()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// See <see cref="IDisposable.Dispose"/>.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes context.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            _disposed = true;
        }
    }
}

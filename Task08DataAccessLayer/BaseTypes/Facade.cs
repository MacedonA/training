﻿using System;
using System.Configuration;
using Task08DataAccessLayer.Model;

namespace Task08DataAccessLayer.BaseTypes
{
    /// <summary>
    /// Implements <see cref="IDALFacade"/>.
    /// </summary>
    public class Facade : IDALFacade
    {
        /// <summary>
        /// Instance of unit of work managed by the facade. Only one unit of work is used at any time.
        /// </summary>
        private IUnitOfWork _unitOfWork;

        /// <summary>
        /// See <see cref="IDALFacade.GetUnitOfWork"/>.
        /// </summary>
        public virtual IUnitOfWork GetUnitOfWork()
        {
            if (_unitOfWork != null)
                throw new Exception("a unit of work is already in use.");
            _unitOfWork = new UnitOfWork<MedcineDbContext>();
            return _unitOfWork;
        }

        /// <summary>
        /// See <see cref="IDALFacade.ReturnUnitOfWork"/>.
        /// </summary>
        public virtual void ReturnUnitOfWork()
        {
            if (_unitOfWork == null) 
                return;
            _unitOfWork.Dispose();
            _unitOfWork = null;
        }

        /// <summary>
        /// Returns a connection string to database.
        /// </summary>
        public static string GetConectionString()
        {
            new MedcineDbContext();
            return ConfigurationManager.ConnectionStrings[MedcineDbContext.ConnectionStringName].ConnectionString;
        }
    }
}

﻿using System;
using Task08DataAccessLayer.Entities;

namespace Task08DataAccessLayer.BaseTypes
{
    /// <summary>
    /// Interface to define contract for the unit of work class.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Access to medcines.
        /// </summary>
        IRepositoryExt<Medcine> MedcinesRepository { get; }

        /// <summary>
        /// Access to medcine forms.
        /// </summary>
        IRepository<MedcineForm> MedcineFormsRepository { get; }

        /// <summary>
        /// Access to medcine images.
        /// </summary>
        IRepositoryExt<MedcineImage> MedcineImagesRepository { get; }

        /// <summary>
        /// Access to manufacturers.
        /// </summary>
        IRepository<Manufacturer> ManufacturersRepository { get; }

        /// <summary>
        /// Access to substances.
        /// </summary>
        IRepository<ActiveSubstance> ActiveSubstancesRepository { get; }

        /// <summary>
        /// Access to orders.
        /// </summary>
        IRepositoryExt<Order> OrdersRepository { get; }

        /// <summary>
        /// Access to roles.
        /// </summary>
        IRepositoryExt<Role> RolesRepository { get; }

        /// <summary>
        /// Access to users.
        /// </summary>
        IRepositoryExt<User> UsersRepository { get; }

        /// <summary>
        /// Used to save the changes to the underlying data store.
        /// </summary>
        void Commit();
    }
}

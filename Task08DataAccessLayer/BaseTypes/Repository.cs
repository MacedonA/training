﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Task08DataAccessLayer.BaseTypes
{
    /// <summary>
    /// Implements <see cref="IRepository{T}"/>.
    /// </summary>
    internal class Repository<T> : IRepositoryExt<T> where T : class
    {
        private readonly IDbSet<T> _dbSet;
        private readonly IMedcineDbContext _dataContext;

        /// <summary>
        /// Injects the DbContext holding DbSet.
        /// </summary>
        /// <param name="dataContext"></param>
        public Repository(IMedcineDbContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = _dataContext.Set<T>();
        }

        /// <summary>
        /// See <see cref="IRepository{T}.GetById"/>.
        /// </summary>
        public T GetById(int id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>
        /// See <see cref="IRepository{T}.GetAsQuery"/>.
        /// </summary>
        public IQueryable<T> GetAsQuery(Expression<Func<T, bool>> query = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<T> queryResult = _dbSet;

            //If there is a query, execute it against the dbset
            if (query != null)
            {
                queryResult = queryResult.Where(query);
            }

            //get the include requests for the navigation properties and add them to the query result
            queryResult = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(queryResult, (current, property) => current.Include(property));

            //if a sort request is made, order the query accordingly.
            if (orderBy != null)
            {
                return orderBy(queryResult);
            }
            //if not, return the results as is
            return queryResult;
        }

        /// <summary>
        /// See <see cref="IRepository{T}.GetByQuery"/>.
        /// </summary>
        public IEnumerable<T> GetByQuery(Expression<Func<T, bool>> query = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            return GetAsQuery(query, orderBy, includeProperties).ToList();
        }

        /// <summary>
        /// See <see cref="IRepositoryExt{T}.Insert"/>.
        /// </summary>
        public void Insert(T entity)
        {
            _dbSet.Add(entity);
        }

        /// <summary>
        /// See <see cref="IRepositoryExt{T}.Update"/>.
        /// </summary>
        public void Update(T entity)
        {
            _dbSet.Attach(entity);
            _dataContext.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// See <see cref="IRepositoryExt{T}.Delete"/>.
        /// </summary>
        public void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }
    }
}

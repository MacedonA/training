﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Task08DataAccessLayer.BaseTypes
{
    /// <summary>
    /// An access to database context.
    /// </summary>
    public interface IMedcineDbContext
    {
        /// <summary>
        /// See <see cref="DbContext.Set{T}"/>.
        /// </summary>
        DbSet<T> Set<T>() where T : class;

        /// <summary>
        /// See <see cref="DbContext.Entry{T}"/>.
        /// </summary>
        DbEntityEntry<T> Entry<T>(T entity) where T : class;

        /// <summary>
        /// See <see cref="DbContext.SaveChanges"/>.
        /// </summary>
        int SaveChanges();

        /// <summary>
        /// See <see cref="DbContext.Dispose"/>.
        /// </summary>
        void Dispose();
    }
}
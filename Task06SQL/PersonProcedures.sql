﻿CREATE PROCEDURE [dbo].[SelectAllPersons]
AS
	SELECT 
		Person_Id, 
		FirstName, 
		LastName, 
		AccountName, 
		SequredPassword, 
		(Flags & 2)/2 as IsActive, 
		Flags & 1 as IsFavorite 
	FROM Persons 
RETURN 0
GO

CREATE PROCEDURE [dbo].[SelectPerson]
	@person_id int
AS
	SELECT 
		Person_Id, 
		FirstName, 
		LastName, 
		AccountName, 
		SequredPassword, 
		(Flags & 2)/2 as IsActive, 
		Flags & 1 as IsFavorite 
	FROM Persons 
	WHERE
		Person_Id = @person_id

	SELECT
		rl.Role_Id,
		rl.Name,
		rl.[Description]
	FROM Roles rl
	JOIN Roles_Persons rp ON rp.Role_Id = rl.Role_Id
	WHERE rp.Person_Id = @person_id
RETURN 0
GO

CREATE PROCEDURE [dbo].[DeletePerson]
	@person_id int
AS
	DELETE FROM Roles_Persons
	WHERE 
		Person_Id = @person_id

	DELETE FROM Persons 
	WHERE
		Person_Id = @person_id
RETURN 0
GO

CREATE PROCEDURE [dbo].[InsertPerson]
	@firstname nvarchar(255),
	@lastname nvarchar(255),
	@accountname nvarchar(255),
	@sequredpassword nvarchar(255),
	@isactive bit,
	@isfavorite bit,
	@role_ids XML
AS
	INSERT INTO Persons (FirstName, LastName, AccountName, SequredPassword, Flags)
	VALUES (@firstname, @lastname, @accountname, @sequredpassword, @isactive*2+@isfavorite)

	DECLARE @person_id INT
	SET @person_id = CONVERT(INT, SCOPE_IDENTITY())

	IF (NOT @role_ids IS NULL)
	BEGIN
		INSERT INTO Roles_Persons (Person_Id, Role_Id)
		SELECT @person_id, 'Role_Id' = x.v.value('text()[1]','int')
		FROM @role_ids.nodes('/root/id') as x(v)    
	END

	SELECT @person_id AS [person_id]
RETURN 0
GO

CREATE PROCEDURE [dbo].[UpdatePerson]
	@person_id int,
	@firstname nvarchar(255),
	@lastname nvarchar(255),
	@accountname nvarchar(255),
	@sequredpassword nvarchar(255),
	@isactive bit,
	@isfavorite bit,
	@role_ids XML
AS
	UPDATE Persons
	SET 
		FirstName = @firstname,
		LastName = @lastname,
		AccountName = @accountname,
		SequredPassword = @sequredpassword,
		Flags = @isactive*2+@isfavorite
	WHERE
		Person_Id = @person_id

	DELETE FROM Roles_Persons
	WHERE 
		Person_Id = @person_id
	IF (NOT @role_ids IS NULL)
	BEGIN
		INSERT INTO Roles_Persons (Person_Id, Role_Id)
		SELECT @person_id, 'Role_Id' = x.v.value('text()[1]','int')
		FROM @role_ids.nodes('/root/id') as x(v)    
	END
RETURN 0

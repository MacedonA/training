﻿CREATE TABLE [dbo].[Roles]
(
	[Role_Id] INT IDENTITY(1,1),
	[Name] NVARCHAR(255) NOT NULL,
	[Description] NVARCHAR(4000)
)
GO
CREATE UNIQUE CLUSTERED INDEX IX_Roles_Role_Id ON [dbo].[Roles] ([Role_Id])

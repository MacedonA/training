﻿CREATE TABLE [dbo].[Roles_Persons]
(
	[Person_Id] INT NOT NULL,
	[Role_Id] INT NOT NULL, 
    CONSTRAINT [FK_Roles_Persons_To_Persons] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Persons]([Person_Id]),
	CONSTRAINT [FK_Roles_Persons_To_Roles] FOREIGN KEY ([Role_Id]) REFERENCES [dbo].[Roles]([Role_Id])
)

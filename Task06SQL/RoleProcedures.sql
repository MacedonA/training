﻿CREATE PROCEDURE [dbo].[SelectAllRoles]
AS
	SELECT 
		Role_Id,
		Name,
		[Description]
	FROM Roles 
RETURN 0
GO

CREATE PROCEDURE [dbo].[SelectRole]
	@role_id int
AS
	SELECT 
		Role_Id,
		Name,
		[Description]
	FROM Roles 
	WHERE
		Role_Id = @role_id
RETURN 0
GO

CREATE PROCEDURE [dbo].[DeleteRole]
	@role_id int
AS
	DELETE FROM Roles 
	WHERE
		Role_Id = @role_id
RETURN 0
GO

CREATE PROCEDURE [dbo].[InsertRole]
	@name nvarchar(255),
	@description nvarchar(255)
AS
	INSERT INTO Roles(Name, [Description])
	VALUES (@name, @description)
	
	SELECT CONVERT(INT, SCOPE_IDENTITY()) AS [role_id]
RETURN 0
GO

CREATE PROCEDURE [dbo].[UpdateRole]
	@role_id int,
	@name nvarchar(255),
	@description nvarchar(255)
AS
	UPDATE Roles
	SET 
		Name = @name,
		[Description] = @description
	WHERE
		Role_Id = @role_id
RETURN 0

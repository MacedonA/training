﻿CREATE TABLE [dbo].[Persons]
(
	[Person_Id] INT IDENTITY(1,1),
    [FirstName] NVARCHAR(255) NOT NULL, 
    [LastName] NVARCHAR(255) NOT NULL, 
    [AccountName] NVARCHAR(255) NOT NULL, 
    [SequredPassword] NVARCHAR(255) NOT NULL, 
    [Flags] INT NOT NULL
)
GO
CREATE UNIQUE CLUSTERED INDEX IX_Persons_Person_Id ON [dbo].[Persons] ([Person_Id])
GO
CREATE UNIQUE INDEX IX_Persons_AccountName ON [dbo].[Persons] ([AccountName])
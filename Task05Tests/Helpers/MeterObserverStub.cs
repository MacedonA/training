﻿using Task05.Common;

namespace Task05Tests.Helpers
{
    internal class MeterObserverStub : IMeterObserver
    {
        public bool Updated { get; private set; }
        public void Update(int id, double val)
        {
            Updated = true;
        }
    }
}
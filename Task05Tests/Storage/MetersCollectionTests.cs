﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task05.Common;
using Task05.Storage;

namespace Task05Tests.Storage
{
    /// <summary>
    /// Tests <see cref="MetersCollection"/> class.
    /// </summary>
    [TestClass]
    public class MetersCollectionTests
    {
        /// <summary>
        /// Tests <see cref="MetersCollection.AddMeter"/> method adds a new meter with described data.
        /// </summary>
        [TestMethod]
        public void AddMeter_MeterAdded()
        {
            const int interval = 1234;
            const MeterTypes type = MeterTypes.Length;
            var collection = new MetersCollection();
            collection.AddMeter(interval, type);
            Assert.AreEqual(1, collection.Meters.Count);
            var meter = collection.Meters[0];
            Assert.AreEqual(0, meter.Id);
            Assert.AreEqual(interval, meter.MeterInterval);
            Assert.AreEqual(type, meter.MeterType);
            Assert.AreEqual(double.NaN, meter.CurrentValue);
            Assert.AreEqual("Wait", meter.GetState());
        }

        /// <summary>
        /// Tests <see cref="MetersCollection.DeleteMeter"/> method deletes a meter correctly.
        /// </summary>
        [TestMethod]
        public void DeleteMeter_MeterDeleted()
        {
            var collection = new MetersCollection();
            collection.AddMeter(3324, MeterTypes.Pressure);
            Assert.IsFalse(collection.DeleteMeter(1));
            Assert.IsTrue(collection.DeleteMeter(0));
            Assert.AreEqual(0, collection.Meters.Count);
        }
        
        /// <summary>
        /// Tests <see cref="MetersCollection.ChangeMeterState"/> method changes meter states as expected.
        /// </summary>
        [TestMethod]
        public void ChangeMeterState_StateChange()
        {
            var collection = new MetersCollection();
            collection.AddMeter(5348, MeterTypes.Temperature);
            var meter = collection.Meters[0];
            Assert.AreEqual("Wait", meter.GetState());
            collection.ChangeMeterState(0);
            Assert.AreEqual("Calibration", meter.GetState());
            collection.ChangeMeterState(0);
            Assert.AreEqual("GetData", meter.GetState());
            collection.ChangeMeterState(0);
            Assert.AreEqual("Wait", meter.GetState());
        }

        /// <summary>
        /// Tests <see cref="MetersCollection.GetMetersValues"/> method returns correct current values of the meter.
        /// </summary>
        [TestMethod]
        public void GetMetersValues_SameIdsAndCurrentValues()
        {
            var collection = new MetersCollection();
            collection.AddMeter(1000, MeterTypes.Force);
            collection.ChangeMeterState(0);
            collection.AddMeter(750, MeterTypes.Pressure);
            collection.ChangeMeterState(1);
            collection.ChangeMeterState(1);
            var ids = new List<int>{0, 1};
            Thread.Sleep(2200);
            collection.ChangeMeterState(0);
            collection.ChangeMeterState(0);
            collection.ChangeMeterState(1);
            var values = collection.GetMetersValues();
            Assert.AreEqual(ids.Count, values.Count);
            for (int i = 0; i < ids.Count; i++)
            {
                var id = ids[i];
                Assert.IsTrue(values.ContainsKey(id), string.Format("Iteration {0}", i));
                Assert.AreEqual(collection.Meters[i].CurrentValue, values[id], string.Format("Iteration {0}", i));
            }
        }
        
        /// <summary>
        /// Tests <see cref="MetersCollection.Update"/> method updates meter's value correctly.
        /// </summary>
        [TestMethod]
        public void Update_ValueUpdated()
        {
            var collection = new MetersCollection();
            collection.AddMeter(3234, MeterTypes.Force);
            collection.AddMeter(998, MeterTypes.Pressure);
            var rnd = new Random();
            foreach (var id in new List<int> { 0, 1 })
            {
                var newValue = rnd.NextDouble()*1000;
                collection.Update(id, newValue);
                Assert.AreEqual(newValue, collection.GetMetersValues()[id], string.Format("Iteration {0}", id));
            }
        }
    }
}
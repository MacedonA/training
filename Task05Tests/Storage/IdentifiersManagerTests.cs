﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task05.Storage;

namespace Task05Tests.Storage
{
    /// <summary>
    /// Tests <see cref="IdentifiersManager"/> class.
    /// </summary>
    [TestClass]
    public class IdentifiersManagerTests
    {
        /// <summary>
        /// Tests <see cref="IdentifiersManager.CreateUniqueId"/>
        /// </summary>
        [TestMethod]
        public void CreateUniqueId_UniqueValue()
        {
            var usedIds = new List<int> {1, 23, 4, 33, 56};
            var newIds = IdentifiersManager.Instance.CreateUniqueId(usedIds);
            if (usedIds.Contains(newIds))
                Assert.Fail("Not unique ID created.");
        }
    }
}
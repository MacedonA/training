﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task04.Helpers;
using Task05.Common;
using Task05.Storage;

namespace Task05Tests.Storage
{
    /// <summary>
    /// Tests <see cref="MetersStorageXml"/> and <see cref="MetersStorageJson"/> classes.
    /// </summary>
    [TestClass]
    public class FileStorageTests
    {
        /// <summary>
        /// Tests <see cref="MetersStorageXml"/> and <see cref="MetersStorageJson"/> restore with wrong data (file does not exist or file is corrupted).
        /// </summary>
        [TestMethod]
        public void Restore_WrongData_Exception()
        {
            var isExpected = HelperMethods.ThrowsExceptionOfType(() => (new MetersStorageXml("Files\\blah.xml")).Restore(), typeof(StorageException));
            Assert.IsTrue(isExpected, "'(new MetersStorageXml(\"blah.xml\")).Restore()' doesn't throw an exception of StorageException type");
            isExpected = HelperMethods.ThrowsExceptionOfType(() => (new MetersStorageXml("Files\\corrupted.xml")).Restore(), typeof(StorageException));
            Assert.IsTrue(isExpected, "'(new MetersStorageXml(\"corrupted.xml\")).Restore()' doesn't throw an exception of StorageException type");

            isExpected = HelperMethods.ThrowsExceptionOfType(() => (new MetersStorageJson("Files\\blah.json")).Restore(), typeof(StorageException));
            Assert.IsTrue(isExpected, "'(new MetersStorageXml(\"blah.json\")).Restore()' doesn't throw an exception of StorageException type");
            isExpected = HelperMethods.ThrowsExceptionOfType(() => (new MetersStorageJson("Files\\corrupted.json")).Restore(), typeof(StorageException));
            Assert.IsTrue(isExpected, "'(new MetersStorageXml(\"corrupted.json\")).Restore()' doesn't throw an exception of StorageException type");
        }

        /// <summary>
        /// Tests <see cref="MetersStorageXml"/> and <see cref="MetersStorageJson"/> restore with correct data.
        /// </summary>
        [TestMethod]
        public void Restore_CorrectData_Restored()
        {
            var storages = new IStorage<MetersCollection>[]
            {
                new MetersStorageXml("Files\\correct.xml"),
                new MetersStorageJson("Files\\correct.json")
            };
            foreach (var storage in storages)
            {
                try
                {
                    storage.Restore();
                }
                catch (StorageException exc)
                {
                    Assert.Fail(exc.ToString());
                }                
            }
        }

        /// <summary>
        /// Tests <see cref="MetersStorageXml"/> and <see cref="MetersStorageJson"/> store method to make sure data can be restored and restored data is correct.
        /// </summary>
        [TestMethod]
        public void Store_RestoresCorrectData()
        {
            var storages = new IStorage<MetersCollection>[]
            {
                new MetersStorageXml("Files\\stored.xml"),
                new MetersStorageJson("Files\\stored.json")
            };
            var stored = new MetersCollection();
            stored.AddMeter(750, MeterTypes.Force);
            stored.AddMeter(1000, MeterTypes.Length);
            stored.AddMeter(1500, MeterTypes.Pressure);
            var metersCount = stored.Meters.Count;
            stored.ChangeMeterState(metersCount - 2);
            stored.ChangeMeterState(metersCount - 1);
            stored.ChangeMeterState(metersCount - 1);
            foreach (var storage in storages)
            {
                MetersCollection restored = null;
                try
                {
                    storage.Store(stored);
                    restored = storage.Restore();
                }
                catch (StorageException exc)
                {
                    Assert.Fail(exc.ToString());
                }

                Assert.AreEqual(metersCount, restored.Meters.Count);
                for (var i = 0; i < stored.Meters.Count; i++)
                {
                    var storedMeter = stored.Meters[i];
                    var restoredMeter = restored.Meters[i];
                    Assert.AreEqual(storedMeter.Id, restoredMeter.Id, string.Format("Iteration {0}.", i));
                    Assert.AreEqual(storedMeter.MeterInterval, restoredMeter.MeterInterval, string.Format("Iteration {0}.", i));
                    Assert.AreEqual(storedMeter.MeterType, restoredMeter.MeterType, string.Format("Iteration {0}.", i));
                    Assert.AreEqual(double.NaN, restoredMeter.CurrentValue, string.Format("Iteration {0}.", i));
                    Assert.AreEqual("Wait", restoredMeter.GetState(), string.Format("Iteration {0}.", i));
                }
            }
        }
    }
}
﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task04.Helpers;
using Task05.Common;
using Task05.Meter;
using Task05Tests.Helpers;

namespace Task05Tests.Meter
{
    /// <summary>
    /// Tests <see cref="IMeter"/> interface implementeation.
    /// </summary>
    [TestClass]
    public class MeterTests
    {
        /// <summary>
        /// Tests <see cref="Task05Tests.Meter"/> constructor with wrong data.
        /// </summary>
        [TestMethod]
        public void Create_WrongData_Exception()
        {
            var isExpected = HelperMethods.ThrowsExceptionOfType(() => MeterFactory.CreateMeter(1, -111, MeterTypes.Force), typeof(ArgumentException));
            Assert.IsTrue(isExpected, "'MeterFactory.CreateMeter(1, -111, MeterTypes.Force)' doesn't throw an exception of ArgumentException type");
        }

        /// <summary>
        /// Tests <see cref="Task05Tests.Meter"/> constructor with correct data.
        /// </summary>
        [TestMethod]
        public void Create_CorrectData_Stored()
        {
            const int id = 123;
            const int interval = 4525;
            const MeterTypes type = MeterTypes.Pressure;
            var entry = MeterFactory.CreateMeter(id, interval, type);
            Assert.AreEqual(id, entry.Id);
            Assert.AreEqual(interval, entry.MeterInterval);
            Assert.AreEqual(type, entry.MeterType);
            Assert.AreEqual(double.NaN, entry.CurrentValue);
        }

        /// <summary>
        /// Tests change state functionality of <see cref="Task05Tests.Meter"/>.
        /// </summary>
        [TestMethod]
        public void ChangeState_CorrectState()
        {
            const string state1 = "Wait";
            const string state2 = "Calibration";
            const string state3 = "GetData";
            var entry = MeterFactory.CreateMeter(12, 4563, MeterTypes.Temperature);
            Assert.AreEqual(state1, entry.GetState());
            entry.ChangeState();
            Assert.AreEqual(state2, entry.GetState());
            entry.ChangeState();
            Assert.AreEqual(state3, entry.GetState());
            entry.ChangeState();
            Assert.AreEqual(state1, entry.GetState());
        }

        /// <summary>
        /// Tests Calibration state functionality of <see cref="Task05Tests.Meter"/>.
        /// </summary>
        [TestMethod]
        public void ChangeState_Calibration_Increment()
        {
            var entry = MeterFactory.CreateMeter(12, 4563, MeterTypes.Temperature);
            entry.ChangeState();
            Thread.Sleep(200);
            for (double i = 0; i < 5; i++)
            {
                Assert.AreEqual(i, entry.CurrentValue, string.Format("Iteration {0}.", i));
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Tests Calibration state functionality of <see cref="Task05Tests.Meter"/>.
        /// </summary>
        [TestMethod]
        public void ChangeState_GetData_Random()
        {
            var prev = double.NaN;
            var entry = MeterFactory.CreateMeter(33, 450, MeterTypes.Temperature);
            entry.ChangeState();
            entry.ChangeState();
            Thread.Sleep(100);
            for (double i = 0; i < 10; i++)
            {
                var current = entry.CurrentValue;
                Assert.AreNotEqual(prev, entry.CurrentValue, string.Format("Iteration {0}.", i));
                prev = current;
                Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Tests add/remove observer functionality of <see cref="Task05Tests.Meter"/>.
        /// </summary>
        [TestMethod]
        public void MeterObserver_AddedRemoved()
        {
            var entry = MeterFactory.CreateMeter(28, 961, MeterTypes.Length);
            var observer = new MeterObserverStub();
            entry.AddMeterObserver(observer);
            Assert.IsTrue(entry.RemoveMeterObserver(observer));
            Assert.IsFalse(entry.RemoveMeterObserver(observer));
        }

        /// <summary>
        /// Tests notify observer functionality of <see cref="Task05Tests.Meter"/>.
        /// </summary>
        [TestMethod]
        public void ValueUpdate_ObserverNotified()
        {
            var entry = MeterFactory.CreateMeter(93, 555, MeterTypes.Temperature);

            //Wait state. Not updated in 3 seconds.
            var observer1 = new MeterObserverStub();
            entry.AddMeterObserver(observer1);
            Thread.Sleep(3000);
            Assert.IsFalse(observer1.Updated);
            entry.RemoveMeterObserver(observer1);

            //Calibration state. Updated in 2 seconds.
            var observer2 = new MeterObserverStub();
            entry.ChangeState();
            entry.AddMeterObserver(observer2);
            Thread.Sleep(2000);
            Assert.IsTrue(observer2.Updated);
            entry.RemoveMeterObserver(observer2);

            //GetData state. Updated in 1 second.
            var observer3 = new MeterObserverStub();
            entry.ChangeState();
            entry.AddMeterObserver(observer3);
            Thread.Sleep(1000);
            Assert.IsTrue(observer3.Updated);
            entry.RemoveMeterObserver(observer3);
        }
    }
}
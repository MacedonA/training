﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;

namespace Task06ObsoleteExporter
{
    class Program
    {
        static void Main(string[] args)
        {
            //1. Make sure destination database is empty.
            bool isValid;
            using (var newDb = new SqlConnection(ConfigurationManager.ConnectionStrings["NewData"].ConnectionString))
            {
                try
                {
                    newDb.Open();
                    var query = new StringBuilder();
                    query.AppendLine(@"SELECT COUNT(*) FROM (SELECT person_id FROM Persons UNION SELECT role_id FROM Roles) AS data");
                    using (var updateCmd = new SqlCommand(query.ToString(), newDb))
                        isValid = int.Parse(updateCmd.ExecuteScalar().ToString()) <= 0;
                    if (!isValid)
                        Console.WriteLine("Database is not empty.");
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error was occurred '{0}'. Data will be rolled back.", e.Message);
                    isValid = false;
                }
            }
            if (!isValid)
                CloseApplication();

            // 2. Import all data.
            using (var obsoleteDb = new SqlConnection(ConfigurationManager.ConnectionStrings["ObsoleteData"].ConnectionString))
            using (var newDb = new SqlConnection(ConfigurationManager.ConnectionStrings["NewData"].ConnectionString))
            {
                newDb.Open();
                using (var transaction = newDb.BeginTransaction())
                {
                    try
                    {
                        obsoleteDb.Open();

                        //Roles.
                        CopyData("SELECT role_id, name, description FROM Roles",
                            obsoleteDb, transaction, "Roles");
                        UpdateIdentity(transaction, "Roles", "role_id");
                        Console.WriteLine("Roles were imported.");

                        //Persons.
                        var query = new StringBuilder();
                        query.AppendLine(@"SELECT");
                        query.AppendLine(@"Person_Id,");
                        query.AppendLine(@"CASE WHEN sepIndex = 0 THEN '' ELSE LTRIM(SUBSTRING(Name, sepIndex +1, LEN(Name))) END AS FirstName,");
                        query.AppendLine(@"CASE WHEN sepIndex = 0 THEN Name ELSE LTRIM(SUBSTRING(Name, 0, sepIndex)) END AS LastName,");
                        query.AppendLine(@"AccountName, SequredPassword, Flags");
                        query.AppendLine(@"FROM (SELECT *, CHARINDEX(',', Name) as sepIndex FROM Persons) as calc");
                        CopyData(query.ToString(), obsoleteDb, transaction, "Persons");
                        UpdateIdentity(transaction, "Persons", "Person_Id");
                        Console.WriteLine("Persons were imported.");

                        //Roles for persons.
                        CopyData("SELECT Person_Id, Role_Id FROM Persons",
                            obsoleteDb, transaction, "Roles_Persons");
                        Console.WriteLine("Roles for persons were imported.");

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An error was occurred '{0}'. Data will be rolled back.", e.Message);
                        transaction.Rollback();
                    }
                }
            }
            CloseApplication();
        }

        private static void CloseApplication()
        {
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            Environment.Exit(0);
        }

        private static void CopyData(string cmdText, SqlConnection obsoleteDb, SqlTransaction transaction, string tableName)
        {
            using (var getCmd = new SqlCommand(cmdText, obsoleteDb))
            using (var reader = getCmd.ExecuteReader())
            using (var bulk = new SqlBulkCopy(transaction.Connection, SqlBulkCopyOptions.KeepIdentity, transaction))
            {
                bulk.DestinationTableName = tableName;
                bulk.WriteToServer(reader);
            }
        }

        private static void UpdateIdentity(SqlTransaction transaction, string tableName, string idName)
        {
            var query = new StringBuilder();
            query.AppendFormat(@"DECLARE @new INT ");
            query.AppendFormat(@"SELECT @new = MAX({0}) FROM {1} ", idName, tableName);
            query.AppendFormat(@"DBCC CHECKIDENT ({0}, RESEED, @new)", tableName);
            using (var updateCmd = new SqlCommand(query.ToString(), transaction.Connection, transaction))
                updateCmd.ExecuteNonQuery();
        }
    }
}

﻿using Task02.Data;

namespace Task02.Statistics
{
    /// <summary>
    /// Stores an instance with test, user name and calculated correct answers count.
    /// </summary>
    internal class TestUserNameCorrectAnswersCount
    {
        internal Test Test { get; private set; }
        internal string UserName { get; private set; }
        internal int CorrectAnswersCount { get; private set; }

        /// <summary>
        /// Constructor with full initialization.
        /// </summary>
        public TestUserNameCorrectAnswersCount(Test test, string userName, int correctAnswersCount)
        {
            Test = test;
            UserName = userName;
            CorrectAnswersCount = correctAnswersCount;
        }
    }
}
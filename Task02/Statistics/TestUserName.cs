﻿using Task02.Data;

namespace Task02.Statistics
{
    /// <summary>
    /// Stores a pair test/user name.
    /// </summary>
    internal class TestUserName
    {
        internal Test Test { get; private set; }
        internal string UserName { get; private set; }

        /// <summary>
        /// Constructor with full initialization.
        /// </summary>
        public TestUserName(Test test, string userName)
        {
            Test = test;
            UserName = userName;
        }
    }
}
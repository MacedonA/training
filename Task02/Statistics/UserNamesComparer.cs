﻿using System;
using System.Collections.Generic;

namespace Task02.Statistics
{
    /// <summary>
    /// A string comparer where "Piotr Ivanov" and "Ivanov, Piotr" are the same.
    /// </summary>
    public class UserNamesComparer : IEqualityComparer<string>
    {
        /// <summary>
        /// Comapres names by defined way.
        /// </summary>
        public bool Equals(string x, string y)
        {
            return string.Equals(GetNormalForm(x), GetNormalForm(y));
        }

        /// <summary>
        /// Gets hash code for the name.
        /// </summary>
        public int GetHashCode(string obj)
        {
            return GetNormalForm(obj).GetHashCode();
        }

        /// <summary>
        /// Returns a name in a normal form (ex. "Ivanov, Piotr") if it is possible.
        /// </summary>
        private static string GetNormalForm(string item)
        {
            //We can use RegExp here but it seems to hard to create a regular expression for all possible languages.
            //So I'll use usual string methods.
            var names = item.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if (names.Length != 2 || names[0].EndsWith(",")) //it means that a string contains name and surname but it is not in normal form yet
                return item;
            return string.Format("{0}, {1}", names[1], names[0]);
        }
    }
}
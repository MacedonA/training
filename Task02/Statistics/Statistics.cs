﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task02.Data;

namespace Task02.Statistics
{
    /// <summary>
    /// Represents the tests statistics.
    /// </summary>
    public class Statistics
    {
        private readonly Dictionary<string, IList<Test>> _statistics = new Dictionary<string, IList<Test>>(new UserNamesComparer());

        /// <summary>
        /// Adds a test to statistics.
        /// </summary>
        public void AddTest(string userName, Test test)
        {
            if (userName == null)
                throw new ArgumentNullException("userName");
            if (test == null)
                throw new ArgumentNullException("test");
            if (!_statistics.ContainsKey(userName))
                _statistics.Add(userName, new List<Test> { test });
            else
                _statistics[userName].Add(test);
        }

        /// <summary>
        /// Gets test count by user name.
        /// </summary>
        public IEnumerable<Tuple<string, int>> GetTestsCountByName()
        {
            //This statement calculates count of unique tests passes. If user can pass one test more then 1 time and it should be included in statistics
            //use the commented statemen below.
            return _statistics.Select(pair => new Tuple<string, int>(pair.Key, pair.Value.Distinct().Count()));
            //return _statistics.Select(pair => new Tuple<string, int>(pair.Key, pair.Value.Count));
        }

        /// <summary>
        /// Gets test which was passed by the most count of unique users.
        /// If several has the same usage count the first test in alfabetical order will be returned.
        /// </summary>
        public Test GetMostPopularTest()
        {
            return _statistics
                .SelectMany(item => item.Value.Select(test => new TestUserName(test, item.Key)))
                .Distinct(new TestUserNameEqualityComparer())
                .OrderBy(tun => tun.Test.Name)
                .GroupBy(tun => tun.Test.Name)
                .OrderByDescending(tunGroup => tunGroup.Count())
                .Select(tunGroup => tunGroup.Select(tun => tun.Test).FirstOrDefault()).FirstOrDefault();
        }

        /// <summary>
        /// Gets results by test name. 
        /// One user can pass test for several times.
        /// </summary>
        public Dictionary<string, List<Tuple<string, int>>> GetResultsByTestName()
        {
            return _statistics
                .SelectMany(item => item.Value.Select(test => new TestUserNameCorrectAnswersCount(test, item.Key, test.Answers.Count(answer => answer.IsCorrect))))
                .OrderBy(test => test.Test.Name) 
                .GroupBy(test => test.Test.Name)
                .ToDictionary(item => item.Key, item => item.Select(data => new Tuple<string, int>(data.UserName, data.CorrectAnswersCount)).ToList());
        }

        /// <summary>
        /// Gets the question with min count of correct answers.
        /// </summary>
        public Question GetHardestQuestion()
        {
            return _statistics
                .SelectMany(item => item.Value.SelectMany(test => test.Answers))
                .GroupBy(answer => answer.Question.Id)
                .OrderBy(answerGroup => answerGroup.Count(answer => answer.IsCorrect))
                .Select(answerGroup => answerGroup.Select(answer => answer.Question).FirstOrDefault()).FirstOrDefault();
        }
    }
}
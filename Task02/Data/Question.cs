﻿using System;

namespace Task02.Data
{
    /// <summary>
    /// Represents a test question.
    /// </summary>
    public class Question
    {
        private readonly uint _complexity;
        private readonly Guid _id;
        private readonly string _text;

        /// <summary>
        /// Complexity of the question.
        /// </summary>
        public uint Complexity
        {
            get { return _complexity; }
        }

        /// <summary>
        /// Unique id of the question.
        /// </summary>
        public Guid Id
        {
            get { return _id; }
        }

        /// <summary>
        /// Content of the question.
        /// </summary>
        public string Text
        {
            get { return _text; }
        }

        /// <summary>
        /// Constructor with auto generated id.
        /// Can be used to create a new instance.
        /// </summary>
        public Question(uint complexity, string text) : this(complexity, text, Guid.NewGuid()) { }

        /// <summary>
        /// Constructor with full initialization.
        /// Can be used to load an instance from the external source.
        /// </summary>
        public Question(uint complexity, string text, Guid id)
        {
            if (text == null)
                throw new ArgumentNullException("text");
            _text = text;
            _complexity = complexity;
            _id = id;
        }

        /// <summary>
        /// <see cref="Id"/> is unique so compare questions by this property.
        /// </summary>
        public override bool Equals(object obj)
        {
            return obj is Question && ((Question)obj).Id == Id;
        }

        /// <summary>
        /// <see cref="Id"/> is unique so generate hash code by this property.
        /// </summary>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
﻿using System;

namespace Task02.Data
{
    /// <summary>
    /// Represents an answer for the question.
    /// </summary>
    public class Answer
    {
        private readonly bool _isCorrect;
        private readonly Question _question;

        /// <summary>
        /// Flag to let know that an answer is correct.
        /// </summary>
        public bool IsCorrect
        {
            get { return _isCorrect; }
        }

        /// <summary>
        /// Reference to the question.
        /// </summary>
        public Question Question
        {
            get { return _question; }
        }

        /// <summary>
        /// Constructor with full initialization.
        /// </summary>
        public Answer(bool isCorrect, Question question)
        {
            if (question == null)
                throw new ArgumentNullException("question");
            _isCorrect = isCorrect;
            _question = question;
        }
    }
}
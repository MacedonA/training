﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Task02.Data
{
    /// <summary>
    /// Represents a test.
    /// </summary>
    public class Test : IEnumerable
    {
        private readonly string _name;
        private readonly DateTime _passDate;
        private readonly ICollection<Answer> _answers;

        /// <summary>
        /// Pass date of the test.
        /// </summary>
        public DateTime PassDate
        {
            get { return _passDate; }
        }

        /// <summary>
        /// Unique name of the test.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Answers from the test.
        /// </summary>
        public ICollection<Answer> Answers
        {
            get { return _answers; }
        }

        /// <summary>
        /// Constructor with full initialization.
        /// Can be used to load an instance from the external source.
        /// </summary>
        public Test(DateTime passDate, ICollection<Answer> answers, string name)
        {
            if (answers == null)
                throw new ArgumentNullException("answers");
            if (name == null)
                throw new ArgumentNullException("name");
            _passDate = passDate;
            _answers = answers;
            _name = name;
        }

        /// <summary>
        /// Returns all answers sorted by complexity of the question descending.
        /// </summary>
        public IEnumerator GetEnumerator()
        {
            return Answers.OrderByDescending(answer => answer.Question.Complexity).GetEnumerator();
        }

        /// <summary>
        /// <see cref="Name"/> is unique so compare tests by this property.
        /// </summary>
        public override bool Equals(object obj)
        {
            return obj is Test && string.Equals(((Test)obj).Name, Name);
        }

        /// <summary>
        /// <see cref="Name"/> is unique so generate hash code by this property.
        /// </summary>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
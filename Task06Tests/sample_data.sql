﻿--Clear all data and reset identities.
delete from Roles_Persons
delete from Persons
delete from Roles
DBCC CHECKIDENT (Persons, RESEED, 0)
DBCC CHECKIDENT (Roles, RESEED, 0)

--Create persons.
insert into Persons (FirstName, LastName, AccountName, SequredPassword, Flags) values ('Somebody', 'Ivan', 'Account', 'Password', 1)
insert into Persons (FirstName, LastName, AccountName, SequredPassword, Flags) values ('OneMoreMan', 'Dave', 'Account2', 'LongPassword', 2)
insert into Persons (FirstName, LastName, AccountName, SequredPassword, Flags) values ('OneMoreWoman', 'Sherri', 'Account-1', 'HardPassword', 0)
insert into Persons (FirstName, LastName, AccountName, SequredPassword, Flags) values ('StrangeAccount', 'Bobbi', 'BAccount', 'ImpossiblePassword', 3)
insert into Persons (FirstName, LastName, AccountName, SequredPassword, Flags) values ('JustUser', 'Vassisuary', 'AccountV', 'Pass', 1)

--Create roles.
insert into Roles (Name, [Description]) values ('Role 1', 'It''s a Role 1')
insert into Roles (Name, [Description]) values ('Role 2', 'It''s a Role 2')
insert into Roles (Name, [Description]) values ('Role 22', NULL)

--Create roles for persons.
insert into Roles_Persons (Person_Id, Role_Id) values (1, 2)
insert into Roles_Persons (Person_Id, Role_Id) values (1, 3)
insert into Roles_Persons (Person_Id, Role_Id) values (2, 1)
insert into Roles_Persons (Person_Id, Role_Id) values (3, 3)
insert into Roles_Persons (Person_Id, Role_Id) values (4, 2)
insert into Roles_Persons (Person_Id, Role_Id) values (4, 1)
insert into Roles_Persons (Person_Id, Role_Id) values (5, 1)
insert into Roles_Persons (Person_Id, Role_Id) values (5, 2)


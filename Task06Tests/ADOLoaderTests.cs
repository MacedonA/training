﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task06DataLayer;
using Task06DataLayer.ADOLoader;

namespace Task06Tests
{
    /// <summary>
    /// Tests <see cref="ADOLoader"/>.
    /// </summary>
    [TestClass]
    public class ADOLoaderTests : LoaderTestsBase
    {
        /// <summary>
        /// Creates an <see cref="ADOLoader"/>.
        /// </summary>
        protected override ILoader CreateLoader()
        {
            return new ADOLoader(ConfigurationManager.ConnectionStrings["ADOLoaderDataContext"].ConnectionString);
        }

        /// <summary>
        /// Tests <see cref="ILoader.SelectAllRolesInfo"/> method.
        /// </summary>
        [TestMethod]
        public new void SelectAllRolesInfo_CorrectDataLoaded()
        {
            base.SelectAllRolesInfo_CorrectDataLoaded();
        }

        /// <summary>
        /// Tests <see cref="ILoader.SelectRole"/> method.
        /// </summary>
        [TestMethod]
        public new void SelectRole_CorrectDataLoaded()
        {
            base.SelectRole_CorrectDataLoaded();
        }

        /// <summary>
        /// Tests <see cref="ILoader.InsertRole"/>, <see cref="ILoader.UpdateRole"/> and <see cref="ILoader.DeleteRole"/> methods.
        /// </summary>
        [TestMethod]
        public new void InsertUpdateDeleteRole_CorrectDataProcessed()
        {
            base.InsertUpdateDeleteRole_CorrectDataProcessed();
        }

        /// <summary>
        /// Tests <see cref="ILoader.SelectAllPersonsInfo"/> method.
        /// </summary>
        [TestMethod]
        public new void SelectAllPersonsInfo_CorrectDataLoaded()
        {
            base.SelectAllPersonsInfo_CorrectDataLoaded();
        }

        /// <summary>
        /// Tests <see cref="ILoader.SelectPerson"/> method.
        /// </summary>
        [TestMethod]
        public new void SelectPerson_CorrectDataLoaded()
        {
            base.SelectPerson_CorrectDataLoaded();
        }

        /// <summary>
        /// Tests <see cref="ILoader.InsertPerson"/>, <see cref="ILoader.UpdatePerson"/> and <see cref="ILoader.DeletePerson"/> methods.
        /// </summary>
        [TestMethod]
        public new void InsertUpdateDeletePersons_CorrectDataProcessed()
        {
            base.InsertUpdateDeletePersons_CorrectDataProcessed();
        }
    }
}

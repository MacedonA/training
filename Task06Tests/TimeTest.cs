﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task06DataLayer;
using Task06DataLayer.ADOLoader;
using Task06DataLayer.Data;
using Task06DataLayer.Data.ADOData;
using Task06DataLayer.EFLoader;
using Task06DataLayer.LINQLoader;

namespace Task06Tests
{
    [TestClass]
    public class TimeTest : LoaderTestsBase
    {
        /// <summary>
        /// Shows time for all operations.
        /// </summary>
        [TestMethod]
        public void TestTime()
        {
            Dictionary<string, TimeSpan[]> timesDict = new Dictionary<string, TimeSpan[]>
            {
                {"Select All Roles", new TimeSpan[3]}, {"Select One Role", new TimeSpan[3]}, {"Insert New Role", new TimeSpan[3]}, {"Update Role", new TimeSpan[3]}, {"Delete Role", new TimeSpan[3]}, 
                {"Select All People", new TimeSpan[3]}, {"Select One Person", new TimeSpan[3]}, {"Insert New Person", new TimeSpan[3]}, {"Update Person", new TimeSpan[3]}, {"Delete Person", new TimeSpan[3]}, 
            };
            ILoader[] loaders = new ILoader[3];
            Action[] actions = new Action[3];
            actions[0] = () => loaders[0] = new ADOLoader(ConfigurationManager.ConnectionStrings["ADOLoaderDataContext"].ConnectionString);
            actions[1] = () => loaders[1] = new LINQLoader(ConfigurationManager.ConnectionStrings["LINQLoaderDataContext"].ConnectionString);
            actions[2] = () => loaders[2] = new EFLoader();
            OutputTime(actions, "Create Loader");

            for (var i = 0; i < 3; i++)
            {
                var loader = loaders[i];
                timesDict["Select All Roles"][i] = GetTime(() => loader.SelectAllRolesInfo());
                timesDict["Select One Role"][i] = GetTime(() => loader.SelectRole(2));
                int newRoleId = 0;
                timesDict["Insert New Role"][i] = GetTime(() => newRoleId = loader.InsertRole(_roleToInsert));
                IRole toUpdateRole = new Role(newRoleId, _roleToUpdate.Name, _roleToUpdate.Description);
                timesDict["Update Role"][i] = GetTime(() => loader.UpdateRole(toUpdateRole));
                timesDict["Delete Role"][i] = GetTime(() => loader.DeleteRole(newRoleId));

                timesDict["Select All People"][i] = GetTime(() => loader.SelectAllPersonsInfo());
                timesDict["Select One Person"][i] = GetTime(() => loader.SelectPerson(2));
                int newPersonId = 0;
                timesDict["Insert New Person"][i] = GetTime(() => newPersonId = loader.InsertPerson(_personToInsert));
                IPerson toUpdatePerson = new Person(newPersonId, _personToUpdate.FirstName, _personToUpdate.LastName, _personToUpdate.AccountName,
                    _personToUpdate.Password, _personToUpdate.IsActive, _personToUpdate.IsFavorite, _personToUpdate.Roles);
                timesDict["Update Person"][i] = GetTime(() => loader.UpdatePerson(toUpdatePerson));
                timesDict["Delete Person"][i] = GetTime(() => loader.DeletePerson(newPersonId));                
            }
            foreach (var pair in timesDict)
                OutputTime(pair.Value, pair.Key);
        }

        private static void OutputTime(Action[] actions, string name)
        {
            System.Diagnostics.Trace.WriteLine(string.Format("Action '{0}':", name));
            foreach (var action in actions)
            {
                var start = DateTime.Now.Ticks;
                action();
                var time = DateTime.Now.Ticks - start;
                System.Diagnostics.Trace.WriteLine(string.Format("Time {0}.", TimeSpan.FromTicks(time)));
            }
            System.Diagnostics.Trace.WriteLine("");
        }

        private static void OutputTime(TimeSpan[] times, string name)
        {
            System.Diagnostics.Trace.WriteLine(string.Format("Action '{0}':", name));
            foreach (var time in times)
                System.Diagnostics.Trace.WriteLine(string.Format("Time {0}.", time));
            System.Diagnostics.Trace.WriteLine("");
        }

        private static TimeSpan GetTime(Action action)
        {
            var start = DateTime.Now.Ticks;
            action();
            var time = DateTime.Now.Ticks - start;
            return TimeSpan.FromTicks(time);
        }

        protected override ILoader CreateLoader()
        {
            return null;
        }
    }
}
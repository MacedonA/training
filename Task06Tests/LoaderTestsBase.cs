﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task06DataLayer;
using Task06DataLayer.Data;
using Task06DataLayer.Data.ADOData;

namespace Task06Tests
{
    [TestClass]
    public abstract class LoaderTestsBase
    {
        protected readonly IRole[] _expectedRoles =
        {
            new Role(1, "Role 1", "It's a Role 1"),
            new Role(2, "Role 2", "It's a Role 2"),
            new Role(3, "Role 22", null)
        };
        protected readonly IRole _roleToInsert = new Role(-22, "Role 24", null);
        protected readonly IRole _roleToUpdate = new Role(-22, "Role 345", "It's a Role 2534");

        protected readonly IPerson[] _expectedPersons =
        {
            new Person(1, "Somebody", "Ivan", "Account", "Password", false, true, new [] {new Role(2, "Role 2", "It's a Role 2"), new Role(3, "Role 22", null)}), 
            new Person(2, "OneMoreMan", "Dave", "Account2", "LongPassword", true ,false, new [] {new Role(1, "Role 1", "It's a Role 1")}), 
            new Person(3, "OneMoreWoman", "Sherri", "Account-1", "HardPassword", false ,false, new [] {new Role(3, "Role 22", null)}), 
            new Person(4, "StrangeAccount", "Bobbi", "BAccount", "ImpossiblePassword", true ,true, new [] {new Role(2, "Role 2", "It's a Role 2"), new Role(1, "Role 1", "It's a Role 1")}), 
            new Person(5, "JustUser", "Vassisuary", "AccountV", "Pass", false ,true, new [] {new Role(1, "Role 1", "It's a Role 1"), new Role(2, "Role 2", "It's a Role 2")}) 
        };

        protected readonly IPerson _personToInsert = new Person(-234, "VeryStrangeAccount", "Bobbi-Vassisuary", "BAccountV", "ImpossiblePass", true, false,
            new[] { new Role(3, "Role 22", null) });
        protected readonly IPerson _personToUpdate = new Person(-234, "VeryStrangeAccount1", "Bobbi-Vassisuary2", "BAccountV23", "ImpossibleP23ass", false, true,
            new[] { new Role(2, "Role 2", "It's a Role 2") });

        /// <summary>
        /// Creates a loader of required type.
        /// </summary>
        protected abstract ILoader CreateLoader();

        /// <summary>
        /// Fills database by initial data.
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            using (var con = new SqlConnection(@"Data Source=(localdb)\ProjectsV12;Initial Catalog=Task06SQL;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False"))
            using (var cmd = new SqlCommand(File.ReadAllText("sample_data.sql"), con) { CommandType = CommandType.Text })
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Tests <see cref="ILoader.SelectAllRolesInfo"/> method.
        /// </summary>
        public void SelectAllRolesInfo_CorrectDataLoaded()
        {
            ILoader loader = CreateLoader();
            var roles = loader.SelectAllRolesInfo();
            //Check roles count.
            Assert.AreEqual(_expectedRoles.Length, roles.Length);
            //Check all fields for all roles.
            for (int i = 0; i < _expectedRoles.Length; i++)
            {
                Assert.AreEqual(_expectedRoles[i].RoleId, roles[i].RoleId, string.Format("Iteration {0}", i));
                Assert.AreEqual(_expectedRoles[i].Name, roles[i].Name, string.Format("Iteration {0}", i));
                Assert.AreEqual(_expectedRoles[i].Description, roles[i].Description, string.Format("Iteration {0}", i));
            }
        }

        /// <summary>
        /// Tests <see cref="ILoader.SelectRole"/> method.
        /// </summary>
        public void SelectRole_CorrectDataLoaded()
        {
            ILoader loader = CreateLoader();
            var role = loader.SelectRole(2);
            //Check all fields for selected role.
            Assert.AreEqual(_expectedRoles[1].RoleId, role.RoleId);
            Assert.AreEqual(_expectedRoles[1].Name, role.Name);
            Assert.AreEqual(_expectedRoles[1].Description, role.Description);
        }

        /// <summary>
        /// Tests <see cref="ILoader.InsertRole"/>, <see cref="ILoader.UpdateRole"/> and <see cref="ILoader.DeleteRole"/> methods.
        /// </summary>
        public void InsertUpdateDeleteRole_CorrectDataProcessed()
        {
            ILoader loader = CreateLoader();
            var newRoleId = loader.InsertRole(_roleToInsert);
            //Check roles count.
            var roles = loader.SelectAllRolesInfo();
            Assert.AreEqual(_expectedRoles.Length + 1, roles.Length);
            var role = loader.SelectRole(newRoleId);
            //Check all fields for added role.
            Assert.AreEqual(newRoleId, role.RoleId);
            Assert.AreEqual(_roleToInsert.Name, role.Name);
            Assert.AreEqual(_roleToInsert.Description, role.Description);

            var toUpdate = new Role(newRoleId, _roleToUpdate.Name, _roleToUpdate.Description);
            loader.UpdateRole(toUpdate);
            role = loader.SelectRole(newRoleId);
            //Check all fields for updated role.
            Assert.AreEqual(newRoleId, role.RoleId);
            Assert.AreEqual(_roleToUpdate.Name, role.Name);
            Assert.AreEqual(_roleToUpdate.Description, role.Description);

            loader.DeleteRole(newRoleId);
            //Check roles count.
            roles = loader.SelectAllRolesInfo();
            Assert.AreEqual(_expectedRoles.Length, roles.Length);
            //Check id fields for all roles.
            for (int i = 0; i < _expectedRoles.Length; i++)
                Assert.AreEqual(_expectedRoles[i].RoleId, roles[i].RoleId, string.Format("Iteration {0}", i));
        }

        /// <summary>
        /// Tests <see cref="ILoader.SelectAllPersonsInfo"/> method.
        /// </summary>
        public void SelectAllPersonsInfo_CorrectDataLoaded()
        {
            ILoader loader = CreateLoader();
            var persons = loader.SelectAllPersonsInfo();
            //Check persons count.
            Assert.AreEqual(_expectedPersons.Length, persons.Length);
            //Check all fields for all persond.
            for (int i = 0; i < _expectedPersons.Length; i++)
            {
                Assert.AreEqual(_expectedPersons[i].PersonId, persons[i].PersonId, string.Format("Iteration {0}", i));
                Assert.AreEqual(_expectedPersons[i].FirstName, persons[i].FirstName, string.Format("Iteration {0}", i));
                Assert.AreEqual(_expectedPersons[i].LastName, persons[i].LastName, string.Format("Iteration {0}", i));
                Assert.AreEqual(_expectedPersons[i].AccountName, persons[i].AccountName, string.Format("Iteration {0}", i));
                Assert.AreEqual(_expectedPersons[i].Password, persons[i].Password, string.Format("Iteration {0}", i));
                Assert.AreEqual(_expectedPersons[i].IsFavorite, persons[i].IsFavorite, string.Format("Iteration {0}", i));
                Assert.AreEqual(_expectedPersons[i].IsActive, persons[i].IsActive, string.Format("Iteration {0}", i));
                Assert.AreEqual(0, persons[i].Roles.Length, string.Format("Iteration {0}", i));
            }
        }

        /// <summary>
        /// Tests <see cref="ILoader.SelectPerson"/> method.
        /// </summary>
        public void SelectPerson_CorrectDataLoaded()
        {
            ILoader loader = CreateLoader();
            var person = loader.SelectPerson(3);
            //Check all fields for selected person.
            Assert.AreEqual(_expectedPersons[2].PersonId, person.PersonId);
            Assert.AreEqual(_expectedPersons[2].FirstName, person.FirstName);
            Assert.AreEqual(_expectedPersons[2].LastName, person.LastName);
            Assert.AreEqual(_expectedPersons[2].AccountName, person.AccountName);
            Assert.AreEqual(_expectedPersons[2].Password, person.Password);
            Assert.AreEqual(_expectedPersons[2].IsFavorite, person.IsFavorite);
            Assert.AreEqual(_expectedPersons[2].IsActive, person.IsActive);
            Assert.AreEqual(_expectedPersons[2].Roles.Length, person.Roles.Length);
            for (int j = 0; j < _expectedPersons[2].Roles.Length; j++)
                Assert.AreEqual(_expectedPersons[2].Roles[j].RoleId, person.Roles[j].RoleId, string.Format("Iteration {0}", j));
        }

        /// <summary>
        /// Tests <see cref="ILoader.InsertPerson"/>, <see cref="ILoader.UpdatePerson"/> and <see cref="ILoader.DeletePerson"/> methods.
        /// </summary>
        public void InsertUpdateDeletePersons_CorrectDataProcessed()
        {
            ILoader loader = CreateLoader();
            ILoader anotherLoader = CreateLoader();
            var newPersonId = loader.InsertPerson(_personToInsert);
            //Check persons count.
            var persons = anotherLoader.SelectAllPersonsInfo();
            Assert.AreEqual(_expectedPersons.Length + 1, persons.Length);
            var person = anotherLoader.SelectPerson(newPersonId);
            //Check all fields for added person.
            Assert.AreEqual(newPersonId, person.PersonId);
            Assert.AreEqual(_personToInsert.FirstName, person.FirstName);
            Assert.AreEqual(_personToInsert.LastName, person.LastName);
            Assert.AreEqual(_personToInsert.AccountName, person.AccountName);
            Assert.AreEqual(_personToInsert.Password, person.Password);
            Assert.AreEqual(_personToInsert.IsFavorite, person.IsFavorite);
            Assert.AreEqual(_personToInsert.IsActive, person.IsActive);
            Assert.AreEqual(_personToInsert.Roles.Length, person.Roles.Length);
            for (int j = 0; j < _personToInsert.Roles.Length; j++)
                Assert.AreEqual(_personToInsert.Roles[j].RoleId, person.Roles[j].RoleId, string.Format("Iteration {0}", j));

            var toUpdate = new Person(newPersonId, _personToUpdate.FirstName, _personToUpdate.LastName, _personToUpdate.AccountName,
                _personToUpdate.Password, _personToUpdate.IsActive, _personToUpdate.IsFavorite, _personToUpdate.Roles);
            loader.UpdatePerson(toUpdate);
            person = anotherLoader.SelectPerson(newPersonId);
            //Check all fields for updated person.
            Assert.AreEqual(newPersonId, person.PersonId);
            Assert.AreEqual(_personToUpdate.FirstName, person.FirstName);
            Assert.AreEqual(_personToUpdate.LastName, person.LastName);
            Assert.AreEqual(_personToUpdate.AccountName, person.AccountName);
            Assert.AreEqual(_personToUpdate.Password, person.Password);
            Assert.AreEqual(_personToUpdate.IsFavorite, person.IsFavorite);
            Assert.AreEqual(_personToUpdate.IsActive, person.IsActive);
            Assert.AreEqual(_personToUpdate.Roles.Length, person.Roles.Length);
            for (int j = 0; j < _personToUpdate.Roles.Length; j++)
                Assert.AreEqual(_personToUpdate.Roles[j].RoleId, person.Roles[j].RoleId, string.Format("Iteration {0}", j));

            loader.DeletePerson(newPersonId);
            //Check persons count.
            persons = anotherLoader.SelectAllPersonsInfo();
            Assert.AreEqual(_expectedPersons.Length, persons.Length);
            //Check id fields for all persons.
            for (int i = 0; i < _expectedPersons.Length; i++)
                Assert.AreEqual(_expectedPersons[i].PersonId, persons[i].PersonId, string.Format("Iteration {0}", i));
        }
    }
}

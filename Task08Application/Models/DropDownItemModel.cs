﻿namespace Task08Application.Models
{
    /// <summary>
    /// A model for drop down item.
    /// </summary>
    public class DropDownItemModel
    {
        /// <summary>
        /// A text to show.
        /// </summary>
        public string Text { get; set; }
        
        /// <summary>
        /// A value to select.
        /// </summary>
        public int Value { get; set; } 
    }
}
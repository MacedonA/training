﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Task08Application.Models.IndexViewModel;

namespace Task08Application.Models.OrderViewModel
{
    /// <summary>
    /// An instance to collect order info on order view.
    /// </summary>
    [DataContract]
    [Serializable]
    public class OrderViewModel
    {
        /// <summary>
        /// A medcine to order.
        /// </summary>
        [XmlAttribute("Medcine")]
        public MedcineModel Medcine { get; set; }

        /// <summary>
        /// A customer first name.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(50, ErrorMessage = "Максимальная допустимая длина значения для этого поля 50 символов.")]
        [DataMember]
        [XmlAttribute("FitstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// A customer last name.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(50, ErrorMessage = "Максимальная допустимая длина значения для этого поля 50 символов.")]
        [DataMember]
        [XmlAttribute("LastName")]
        public string LastName { get; set; }

        /// <summary>
        /// A customer phone number.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(30, ErrorMessage = "Максимальная допустимая длина значения для этого поля 30 символов.")]
        [DataMember]
        [XmlAttribute("PhoneNumber")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// A customer adderss.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(1000, ErrorMessage = "Максимальная допустимая длина значения для этого поля 1000 символов.")]
        [DataMember]
        [XmlAttribute("Address")]
        public string Address { get; set; }

        /// <summary>
        /// A customer email.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Значение для этого поля должно соответствовать формату адреса элетронной почты.")]
        [MaxLength(100, ErrorMessage = "Максимальная допустимая длина этого значения для этого 100 символов.")]
        [DataMember]
        [XmlAttribute("EMailAddress")]
        public string EMailAddress { get; set; }

        /// <summary>
        /// Additional comments from customer.
        /// </summary>
        [MaxLength(4000, ErrorMessage = "Максимальная допустимая длина значения для этого поля 4000 символов.")]
        [DataMember]
        [XmlAttribute("Comments")]
        public string Comments { get; set; }
    }
}
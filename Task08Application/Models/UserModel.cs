﻿namespace Task08Application.Models
{
    /// <summary>
    /// Data about user.
    /// </summary>
    public class UserModel
    {
        /// <summary>
        /// User's login.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// User's role.
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// User's password.
        /// </summary>
        public string Password { get; set; }
    }
}
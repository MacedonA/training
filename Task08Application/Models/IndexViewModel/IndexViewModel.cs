﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PagedList;

namespace Task08Application.Models.IndexViewModel
{
    /// <summary>
    /// An instance to show all info on index view.
    /// </summary>
    public class IndexViewModel
    {
        /// <summary>
        /// A list of medcines to show.
        /// </summary>
        public PagedList<MedcineModel> MedcineModels { get; set; }

        /// <summary>
        /// A list of substances to filter by.
        /// </summary>
        public SelectList Substances { get; set; }

        /// <summary>
        /// A list of manufacturers to filter by.
        /// </summary>
        public SelectList Manufacturers { get; set; }

        /// <summary>
        /// A list of medcine forms to filter by.
        /// </summary>
        public SelectList MedcineForms { get; set; }

        /// <summary>
        /// A selected substance.
        /// </summary>
        public int SelectedSubstanceValue { get; set; }

        /// <summary>
        /// A selected manufacturer.
        /// </summary>
        public int SelectedManufacturerValue { get; set; }

        /// <summary>
        /// A selected medcine form.
        /// </summary>
        public int SelectedMedcineFormValue { get; set; }

        /// <summary>
        /// Number of the selected page.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Size of the page.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public IndexViewModel()
        {
            PageNumber = 1;
            PageSize = 2;
            MedcineModels = new PagedList<MedcineModel>(new List<MedcineModel>(), PageNumber, PageSize);
        }

        /// <summary>
        /// Is used to get all pages for pager control.
        /// </summary>
        public int[] GetPages()
        {
            var result = new List<int> { 1, MedcineModels.PageCount };
            if (PageNumber > 1)
                result.Add(PageNumber - 1);
            result.Add(PageNumber);
            if (PageNumber < MedcineModels.PageCount)
                result.Add(PageNumber + 1);

            return result.Distinct().OrderBy(i => i).ToArray();
        }
    }
}
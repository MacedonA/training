﻿using System;
using System.Xml.Serialization;

namespace Task08Application.Models.IndexViewModel
{
    /// <summary>
    /// An instance to show medcine info on index view.
    /// </summary>
    [Serializable]
    public class MedcineModel
    {
        /// <summary>
        /// Id of medcine.
        /// </summary>
        [XmlAttribute("MedcineId")]
        public int MedcineId { get; set; }

        /// <summary>
        /// Name of medcine.
        /// </summary>
        [XmlAttribute("MedcineName")]
        public string MedcineName { get; set; }

        /// <summary>
        /// Annotation of medcine.
        /// </summary>
        public string MedcineAnnotation { get; set; }

        /// <summary>
        /// Name of medcine form.
        /// </summary>
        public string MedcineFormName { get; set; }

        /// <summary>
        /// Name of substance.
        /// </summary>
        public string ActiveSubstanceName { get; set; }

        /// <summary>
        /// Name of manufacturer.
        /// </summary>
        public string ManufacturerName { get; set; }
    }
}
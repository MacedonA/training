﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Task08Application.Models.LoginViewModel
{
    /// <summary>
    /// Represents login info.
    /// </summary>
    [DataContract]
    [Serializable]
    public class LoginViewModel
    {
        /// <summary>
        /// Login name.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(50, ErrorMessage = "Максимальная допустимая длина значения для этого поля 50 символов.")]
        [DataMember]
        [XmlAttribute("Login")]
        public string Login { get; set; }

        /// <summary>
        /// Login password.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(50, ErrorMessage = "Максимальная допустимая длина значения для этого поля 50 символов.")]
        [DataMember]
        [XmlAttribute("Password")]
        public string Password { get; set; }

        /// <summary>
        /// Url to redirect after login.
        /// </summary>
        [DataMember]
        [XmlAttribute("RedirectUrl")]
        public string RedirectUrl { get; set; }
    }
}
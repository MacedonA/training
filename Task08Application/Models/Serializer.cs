﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Task08Application.Models
{
    /// <summary>
    /// Stores a logic to serialize data for cookies.
    /// </summary>
    public static class CookieSerializer
    {
        /// <summary>
        /// Use it to serialize object for cookies.
        /// </summary>
        public static string Serialize<T>(T data)
        {
            try
            {
                var serializer = (new DataContractJsonSerializer(data.GetType()));
                using (MemoryStream ms = new MemoryStream())
                {
                    serializer.WriteObject(ms, data);
                    return Encoding.Default.GetString(ms.ToArray());
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Use it to deserialize object from cookies.
        /// </summary>
        public static T Deserialize<T>(string data) where T : class, new()
        {
            try
            {
                var instance = new T();
                var serializer = (new DataContractJsonSerializer(instance.GetType()));
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(data)))
                {
                    return (T)serializer.ReadObject(ms);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
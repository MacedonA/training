﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Caching;
using System.Web.Mvc;
using PagedList;
using Task08Application.Models.DetailsViewModel;
using Task08Application.Models.EditUserViewModel;
using Task08Application.Models.IndexViewModel;
using Task08DataAccessLayer.BaseTypes;
using Task08DataAccessLayer.Entities;

namespace Task08Application.Models
{
    /// <summary>
    /// Reads data from database.
    /// </summary>
    public class DataReader
    {
        /// <summary>
        /// Apply database caching.
        /// </summary>
        public static void CreateCachingSqlDependecies()
        {
            var connStr = Facade.GetConectionString();
            SqlCacheDependencyAdmin.EnableNotifications(connStr);
            SqlCacheDependencyAdmin.EnableTableForNotifications(connStr, "MedcineImages");
        }

        /// <summary>
        /// Creates a view model for index page and fills it from database.
        /// </summary>
        public static IndexViewModel.IndexViewModel GetIndexData(IndexViewModel.IndexViewModel prevData)
        {
            var model = new IndexViewModel.IndexViewModel
            {
                SelectedSubstanceValue = prevData == null ? 0 : prevData.SelectedSubstanceValue,
                SelectedManufacturerValue = prevData == null ? 0 : prevData.SelectedManufacturerValue,
                SelectedMedcineFormValue = prevData == null ? 0 : prevData.SelectedMedcineFormValue,
                PageNumber = prevData == null ? 1 : prevData.PageNumber
            };
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                IQueryable<MedcineModel> medModels = uow.MedcinesRepository.GetAsQuery(
                    medcine =>
                        (model.SelectedSubstanceValue == 0 || model.SelectedSubstanceValue == medcine.ActiveSubstanceId) &&
                        (model.SelectedMedcineFormValue == 0 || model.SelectedMedcineFormValue == medcine.MedcineFormId) &&
                        (model.SelectedManufacturerValue == 0 ||
                         model.SelectedManufacturerValue == medcine.ManufacturerId),
                    includeProperties: "MedcineForm,ActiveSubstance,Manufacturer",
                    orderBy: medcines => medcines.OrderBy(med => med.MedcineName)).
                    Select(item => new MedcineModel
                    {
                        MedcineId = item.MedcineId,
                        ActiveSubstanceName = item.ActiveSubstance.ActiveSubstanceName,
                        MedcineName = item.MedcineName,
                        MedcineFormName = item.MedcineForm.MedcineFormName,
                        ManufacturerName = item.Manufacturer.ManufacturerName,
                        MedcineAnnotation = item.MedcineAnnotation
                    });
                var models = new PagedList<MedcineModel>(medModels, model.PageNumber, model.PageSize);
                if (models.PageNumber > models.PageCount)
                {
                    model.PageNumber = Math.Max(models.PageCount, 1);
                    models = new PagedList<MedcineModel>(medModels, model.PageNumber, model.PageSize);
                }
                model.MedcineModels = models;

                model.Substances = new SelectList(uow.ActiveSubstancesRepository.GetByQuery(item => true).
                    Select(item => new DropDownItemModel
                    {
                        Text = item.ActiveSubstanceName,
                        Value = item.ActiveSubstanceId
                    }).Union(new[] { new DropDownItemModel { Text = string.Empty, Value = 0 } }).OrderBy(item => item.Text),
                    "Value", "Text", model.SelectedSubstanceValue);

                model.Manufacturers = new SelectList(uow.ManufacturersRepository.GetByQuery(item => true).
                    Select(item => new DropDownItemModel
                    {
                        Text = item.ManufacturerName,
                        Value = item.ManufacturerId
                    }).Union(new[] { new DropDownItemModel { Text = string.Empty, Value = 0 } }).OrderBy(item => item.Text),
                    "Value", "Text", model.SelectedManufacturerValue);

                model.MedcineForms = new SelectList(uow.MedcineFormsRepository.GetByQuery(item => true).
                    Select(item => new DropDownItemModel
                    {
                        Text = item.MedcineFormName,
                        Value = item.MedcineFormId
                    }).Union(new[] { new DropDownItemModel { Text = string.Empty, Value = 0 } }).OrderBy(item => item.Text),
                    "Value", "Text", model.SelectedMedcineFormValue);
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
            return model;
        }

        /// <summary>
        /// Creates a view model for details page and fills it from database.
        /// </summary>
        public static DetailsViewModel.DetailsViewModel GetDetailsData(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var item = uow.MedcinesRepository.GetById(id);
                return item == null ? null : new DetailsViewModel.DetailsViewModel
                {
                    Medcine = MedcineFromEntity(item)
                };
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Creates a view model for details page and fills it from database.
        /// </summary>
        public static byte[] GetMedcineImage(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var item = uow.MedcinesRepository.GetById(id);
                var imageItem = item == null ? null : item.MedcineImage;
                return imageItem == null ? null : imageItem.Image;
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Creates a medcine model to be used on order page and fills it from database.
        /// </summary>
        public static MedcineModel GetOrderMedcine(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var item = uow.MedcinesRepository.GetById(id);
                return item == null ? null : MedcineFromEntity(item);
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Creates a view model for order page and fills it from database.
        /// </summary>
        public static OrderViewModel.OrderViewModel GetOrderData(OrderViewModel.OrderViewModel model)
        {
            if (model == null || model.Medcine == null)
                return null;
            model.Medcine = GetOrderMedcine(model.Medcine.MedcineId);
            return model;
        }

        /// <summary>
        /// Is used to get medcine data from database.
        /// </summary>
        public static MedcineModel GetMedcineByIdEntity(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var item = uow.MedcinesRepository.GetById(id);
                return item == null ? null : MedcineFromEntity(item);
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Is used to save order into database.
        /// </summary>
        public static void SaveOrder(OrderViewModel.OrderViewModel model)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                uow.OrdersRepository.Insert(OrderToEntity(model));
                uow.Commit();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Creates a view model for details page and fills it from database.
        /// </summary>
        public static EditDetailsViewModel GetEditDetailsData(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var item = uow.MedcinesRepository.GetById(id);
                var model = item == null ? null : EditMedcineModelFromEntity(item);
                if (model != null)
                    FillLists(model, uow);
                return model;
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Validates identifiers.
        /// </summary>
        public static List<string> ValidateEditSelectedIds(EditDetailsViewModel data)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var result = new List<string>();
                if (!uow.ActiveSubstancesRepository.GetAsQuery(x => x.ActiveSubstanceId == data.ActiveSubstanceId).Any())
                    result.Add("ActiveSubstanceId");
                if (!uow.ManufacturersRepository.GetAsQuery(x => x.ManufacturerId == data.ManufacturerId).Any())
                    result.Add("ManufacturerId");
                if (!uow.MedcinesRepository.GetAsQuery(x => x.MedcineFormId == data.MedcineFormId).Any())
                    result.Add("MedcineFormId");
                return result;
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Fills lists for edit view model.
        /// </summary>
        public static void FillLists(EditDetailsViewModel model)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                if (model == null) return;
                FillLists(model, uow);
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Fills lists for edit view model.
        /// </summary>
        public static void FillImage(EditDetailsViewModel model)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                if (model == null || !model.ImageId.HasValue) return;
                var item = uow.MedcineImagesRepository.GetById(model.ImageId.Value);
                model.CurrentImage = item == null ? null : item.Image;
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Fills lists for edit view model using existed unit of works.
        /// </summary>
        private static void FillLists(EditDetailsViewModel model, IUnitOfWork uow)
        {
            model.Substances = new SelectList(uow.ActiveSubstancesRepository.GetByQuery(x => true).
                Select(x => new DropDownItemModel
                {
                    Text = x.ActiveSubstanceName,
                    Value = x.ActiveSubstanceId
                }).OrderBy(x => x.Text),
                "Value", "Text", model.ActiveSubstanceId);

            model.Manufacturers = new SelectList(uow.ManufacturersRepository.GetByQuery(x => true).
                Select(x => new DropDownItemModel
                {
                    Text = x.ManufacturerName,
                    Value = x.ManufacturerId
                }).OrderBy(x => x.Text),
                "Value", "Text", model.ManufacturerId);

            model.MedcineForms = new SelectList(uow.MedcineFormsRepository.GetByQuery(x => true).
                Select(x => new DropDownItemModel
                {
                    Text = x.MedcineFormName,
                    Value = x.MedcineFormId
                }).OrderBy(x => x.Text),
                "Value", "Text", model.MedcineFormId);
        }

        /// <summary>
        /// Is used to save medcine into database.
        /// </summary>
        public static void SaveMedcine(EditDetailsViewModel model)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var result = EditMedcineModelToEntity(model);
                if (model.SelectedFile != null)
                {
                    var isNew = false;
                    var image = model.ImageId.HasValue ? uow.MedcineImagesRepository.GetById(model.ImageId.Value) : null;
                    if (image == null)
                    {
                        image = new MedcineImage();
                        isNew = true;
                    }
                    MemoryStream target = new MemoryStream();
                    model.SelectedFile.InputStream.CopyTo(target);
                    image.Image = target.ToArray();
                    if (isNew)
                        uow.MedcineImagesRepository.Insert(image);
                    else
                        uow.MedcineImagesRepository.Update(image);
                    result.MedcineImage = image;
                }
                if (model.MedcineId > 0)
                    uow.MedcinesRepository.Update(result);
                else
                    uow.MedcinesRepository.Insert(result);
                uow.Commit();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Is used to delete medcine from database.
        /// </summary>
        public static void DeleteMedcine(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var toDelete = uow.MedcinesRepository.GetById(id);
                if (toDelete == null) 
                    throw new NullReferenceException("Medcine to delete was not found.");
                if (toDelete.MedcineImageId.HasValue &&
                    !uow.MedcinesRepository.GetAsQuery(x => x.MedcineId != id && x.MedcineImageId == toDelete.MedcineImageId).Any())
                {
                    uow.MedcineImagesRepository.Delete(toDelete.MedcineImage);
                }
                uow.MedcinesRepository.Delete(toDelete);
                uow.Commit();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Validates a user to log in.
        /// </summary>
        public static string ValidateUserAndGetRole(string login, string password)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var user = uow.UsersRepository.GetAsQuery(x => x.Login == login && x.Password == password).FirstOrDefault();
                return user == null ? null : user.Role.InternalName;
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        public static string TryCreateNewUser(string login, string password)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var uRep = uow.UsersRepository;
                var user = uRep.GetAsQuery(x => x.Login == login).FirstOrDefault();
                if (user != null)
                    return null;
                var role = uow.RolesRepository.GetAsQuery(x => x.InternalName == "user").FirstOrDefault();
                if (role == null)
                    return null;
                user = new User { Login = login, Password = password, RoleId = role.RoleId };
                uRep.Insert(user);
                uow.Commit();
                return role.InternalName;
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Gets data for user selection form.
        /// </summary>
        public static EditUserSelectionViewModel GetUserViewModel(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var uRep = uow.UsersRepository;
                var allUsers = uRep.GetByQuery(orderBy: x => x.OrderBy(xx => xx.Login));

                var result = new EditUserSelectionViewModel
                {
                    SelectedUser = id == 0 ? allUsers.FirstOrDefault().UserId : id
                };
                var list = new SelectList(allUsers.
                    Select(item => new DropDownItemModel
                    {
                        Text = item.Login,
                        Value = item.UserId
                    }).OrderBy(item => item.Text),
                    "Value", "Text", result.SelectedUser);
                result.Users = list;
                return result;
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Gets user login by id.
        /// </summary>
        public static string GetUserLogin(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                return uow.UsersRepository.GetById(id).Login;
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Gets additional data for user selection form.
        /// </summary>
        public static EditUserDetailsViewModel GetUserDetalisViewModel(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var uRep = uow.UsersRepository;
                var user = uRep.GetById(id);
                if (user == null)
                    return null;

                var result = new EditUserDetailsViewModel
                {
                    CurerntRole = user.RoleId
                };
                var list = new SelectList(uow.RolesRepository.GetByQuery(orderBy: x => x.OrderBy(xx => xx.Name)).
                    Select(item => new DropDownItemModel
                    {
                        Text = item.Name,
                        Value = item.RoleId
                    }),
                    "Value", "Text", result.CurerntRole);
                result.Roles = list;
                return result;
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Saves changes for user.
        /// </summary>
        public static void SaveUser(int userId, int roleId)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var uRep = uow.UsersRepository;
                var user = uRep.GetById(userId);
                user.RoleId = roleId;
                uRep.Update(user);
                uow.Commit();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Returns all medcines.
        /// </summary>
        public static List<MedcineModel> GetAllMedcines()
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                return uow.MedcinesRepository.GetAsQuery().
                    Select(item => new MedcineModel
                    {
                        MedcineId = item.MedcineId,
                        ActiveSubstanceName = item.ActiveSubstance.ActiveSubstanceName,
                        MedcineName = item.MedcineName,
                        MedcineFormName = item.MedcineForm.MedcineFormName,
                        ManufacturerName = item.Manufacturer.ManufacturerName,
                        MedcineAnnotation = item.MedcineAnnotation
                    }).ToList();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Returns one medcine.
        /// </summary>
        public static MedcineModel GetOneMedcine(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                return uow.MedcinesRepository.GetAsQuery(med => med.MedcineId == id).
                    Select(item => new MedcineModel
                    {
                        MedcineId = item.MedcineId,
                        ActiveSubstanceName = item.ActiveSubstance.ActiveSubstanceName,
                        MedcineName = item.MedcineName,
                        MedcineFormName = item.MedcineForm.MedcineFormName,
                        ManufacturerName = item.Manufacturer.ManufacturerName,
                        MedcineAnnotation = item.MedcineAnnotation
                    }).FirstOrDefault();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Returns all users with roles.
        /// </summary>
        public static List<UserModel> GetAllUsers()
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                return uow.UsersRepository.GetAsQuery().
                    Select(item => new UserModel
                    {
                        Login = item.Login,
                        Role = item.Role.Name
                    }).ToList();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Returns one user with role.
        /// </summary>
        public static UserModel GetOneUser(int id)
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                return uow.UsersRepository.GetAsQuery(usr => usr.UserId == id).
                    Select(item => new UserModel
                    {
                        Login = item.Login,
                        Role = item.Role.Name
                    }).FirstOrDefault();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Returns a list of ordered medcines.
        /// </summary>
        public static IEnumerable<OrderViewModel.OrderViewModel> GetLastOrderedMedcines(int count)
        {
            if (count <= 0)
                return new List<OrderViewModel.OrderViewModel>();
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                return uow.OrdersRepository.GetAsQuery().
                    OrderByDescending(ord => ord.OrderId).
                    Select(ord => new OrderViewModel.OrderViewModel{ Medcine = new MedcineModel { MedcineId = ord.MedcineId, MedcineName = ord.Medcine.MedcineName}}).
                    Take(count).ToList();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        #region TypesConversion
        private static MedcineModel MedcineFromEntity(Medcine item)
        {
            return new MedcineModel
            {
                MedcineId = item.MedcineId,
                ActiveSubstanceName = item.ActiveSubstance.ActiveSubstanceName,
                MedcineName = item.MedcineName,
                MedcineFormName = item.MedcineForm.MedcineFormName,
                ManufacturerName = item.Manufacturer.ManufacturerName,
                MedcineAnnotation = item.MedcineAnnotation
            };
        }

        private static Order OrderToEntity(OrderViewModel.OrderViewModel model)
        {
            return new Order
            {
                MedcineId = model.Medcine.MedcineId,
                Address = model.Address,
                Comments = model.Comments,
                EMailAddress = model.EMailAddress,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber
            };
        }

        private static EditDetailsViewModel EditMedcineModelFromEntity(Medcine item)
        {
            return new EditDetailsViewModel
            {
                MedcineId = item.MedcineId,
                MedcineName = item.MedcineName,
                MedcineAnnotation = item.MedcineAnnotation,
                ActiveSubstanceId = item.ActiveSubstanceId,
                ManufacturerId = item.ManufacturerId,
                MedcineFormId = item.MedcineFormId,
                ImageId = item.MedcineImageId,
                CurrentImage = item.MedcineImage == null ? null : item.MedcineImage.Image
            };
        }

        private static Medcine EditMedcineModelToEntity(EditDetailsViewModel model)
        {
            return new Medcine
            {
                MedcineId = model.MedcineId,
                MedcineName = model.MedcineName,
                MedcineFormId = model.MedcineFormId,
                ActiveSubstanceId = model.ActiveSubstanceId,
                ManufacturerId = model.ManufacturerId,
                MedcineAnnotation = model.MedcineAnnotation,
                MedcineImageId = model.ImageId
            };
        }
        #endregion
    }
}
﻿using System;
using System.Web.Mvc;

namespace Task08Application.Models.EditUserViewModel
{
    /// <summary>
    /// Represents changable data for a user.
    /// </summary>
    public class EditUserDetailsViewModel
    {
        [NonSerialized]
        private SelectList _roles;

        /// <summary>
        /// Selected user.
        /// </summary>
        public int CurerntRole { get; set; }

        /// <summary>
        /// A list of users to select.
        /// </summary>
        public SelectList Roles
        {
            get { return _roles; }
            set { _roles = value; }
        }
    }
}
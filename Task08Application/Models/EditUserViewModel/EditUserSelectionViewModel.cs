﻿using System;
using System.Web.Mvc;

namespace Task08Application.Models.EditUserViewModel
{
    /// <summary>
    /// Represents data to select a user.
    /// </summary>
    public class EditUserSelectionViewModel
    {
        [NonSerialized]
        private SelectList _users;

        /// <summary>
        /// Selected user.
        /// </summary>
        public int SelectedUser { get; set; }

        /// <summary>
        /// A list of users to select.
        /// </summary>
        public SelectList Users
        {
            get { return _users; }
            set { _users = value; }
        }
    }
}
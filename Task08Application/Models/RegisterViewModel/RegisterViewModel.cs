﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Task08Application.Models.RegisterViewModel
{
    /// <summary>
    /// An instance to register a user.
    /// </summary>
    [Serializable]
    public class RegisterViewModel
    {
        /// <summary>
        /// A user login.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(150, ErrorMessage = "Максимальная допустимая длина значения для этого поля 150 символов.")]
        [XmlAttribute("Login")]
        public string Login { get; set; }
        
        /// <summary>
        /// A user password.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(150, ErrorMessage = "Максимальная допустимая длина значения для этого поля 150 символов.")]
        [XmlAttribute("Password")]
        public string Password { get; set; }

        /// <summary>
        /// A user password once again.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(150, ErrorMessage = "Максимальная допустимая длина значения для этого поля 150 символов.")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают.")]
        [XmlAttribute("ConfirmPassword")]
        public string ConfirmPassword { get; set; }
    }
}
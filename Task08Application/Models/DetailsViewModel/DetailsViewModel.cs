﻿using Task08Application.Models.IndexViewModel;

namespace Task08Application.Models.DetailsViewModel
{
    /// <summary>
    /// An instance to show medcine info on details view.
    /// </summary>
    public class DetailsViewModel
    {
        /// <summary>
        /// A medcine to show.
        /// </summary>
        public MedcineModel Medcine { get; set; }
    }
}
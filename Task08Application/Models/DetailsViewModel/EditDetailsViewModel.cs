﻿#region Copyright Notice
//-----------------------------------------------------------------------
// <copyright>
//   Copyright © 2016 MTS Systems Corporation.  All rights reserved.
//   This software is furnished under a license and may be used and 
//   copied only in accordance with the terms of such license
//   and with inclusion of the above copyright notice.
//   <author>
//     MTS Systems Corporation, SergeyOsipchyk, 2/24/2016 6:10:16 PM
//   </author>
// </copyright>
//-----------------------------------------------------------------------
#endregion

using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using Task08Application.Validation;

namespace Task08Application.Models.DetailsViewModel
{
    /// <summary>
    /// An instance to edit medcine info.
    /// </summary>
    [Serializable]
    public class EditDetailsViewModel
    {
        [NonSerialized]
        private HttpPostedFileBase _selectedFile;
        [NonSerialized]
        private SelectList _substances;
        [NonSerialized]
        private SelectList _manufacturers;
        [NonSerialized]
        private SelectList _medcineForms;

        /// <summary>
        /// Id of medcine.
        /// </summary>
        [XmlAttribute("MedcineId")]
        public int MedcineId { get; set; }

        /// <summary>
        /// Name of medcine.
        /// </summary>
        [Required(ErrorMessage = "Это обязатльное поле")]
        [MaxLength(150, ErrorMessage = "Максимальная допустимая длина значения для этого поля 150 символов.")]
        [XmlAttribute("MedcineName")]
        public string MedcineName { get; set; }

        /// <summary>
        /// Annotation of medcine.
        /// </summary>
        [MaxLength(4000, ErrorMessage = "Максимальная допустимая длина значения для этого поля 4000 символов.")]
        [XmlAttribute("MedcineAnnotation")]
        public string MedcineAnnotation { get; set; }

        /// <summary>
        /// Id of medcine form.
        /// </summary>
        [XmlAttribute("MedcineFormId")]
        public int MedcineFormId { get; set; }

        /// <summary>
        /// Id of substance.
        /// </summary>
        [XmlAttribute("ActiveSubstanceId")]
        public int ActiveSubstanceId { get; set; }

        /// <summary>
        /// Id of manufacturer.
        /// </summary>
        [XmlAttribute("ManufacturerId")]
        public int ManufacturerId { get; set; }

        /// <summary>
        /// Id of medcine image.
        /// </summary>
        [XmlAttribute("ImageId")]
        public int? ImageId { get; set; }

        /// <summary>
        /// Current image to show.
        /// </summary>
        [XmlAttribute("CurrentImage")]
        public byte[] CurrentImage { get; set; }

        /// <summary>
        /// Selected file.
        /// </summary>
        [ValidateImage(CanBeEmpty = true, ErrorMessage = "Допустимы только файлы типа JPEG или PNG объёмом не более 2 мегабайта.")]
        public HttpPostedFileBase SelectedFile
        {
            get { return _selectedFile;  }
            set { _selectedFile = value; }
        }

        /// <summary>
        /// A list of substances to select.
        /// </summary>
        public SelectList Substances
        {
            get { return _substances; }
            set { _substances = value; }
        }

        /// <summary>
        /// A list of manufacturers to  select.
        /// </summary>
        public SelectList Manufacturers
        {
            get { return _manufacturers; }
            set { _manufacturers = value; }
        }

        /// <summary>
        /// A list of medcine forms to  select.
        /// </summary>
        public SelectList MedcineForms
        {
            get { return _medcineForms; }
            set { _medcineForms = value; }
        }
    }
}
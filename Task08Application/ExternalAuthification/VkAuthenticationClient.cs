﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using DotNetOpenAuth.AspNet;
using Newtonsoft.Json;

namespace Task08Application.ExternalAuthification
{
    /// <summary>
    /// Allows to useauthorization from vk.com.
    /// </summary>
    public class VkAuthenticationClient : IAuthenticationClient
    {
        private readonly string _appId;
        private readonly string _appSecret;
        private const string BaseUrl = "https://oauth.vk.com/authorize?client_id=";
        private const string GraphApiToken = "https://oauth.vk.com/access_token?client_id=";
        private const string GraphApiMe = "https://api.vk.com/method/";

        /// <summary>
        /// Default contructor.
        /// </summary>
        public VkAuthenticationClient(string appId, string appSecret)
        {
            _appId = appId;
            _appSecret = appSecret;
        }

        /// <summary>
        /// Name of provider.
        /// </summary>
        public string ProviderName { get { return "vkClient"; } }

        /// <summary>
        /// Requests authification from the provider.
        /// </summary>
        public void RequestAuthentication(HttpContextBase context, Uri returnUrl)
        {
            var url = string.Format("{0}{1}&redirect_uri={2}&response_type=code",
                BaseUrl, _appId, HttpUtility.UrlEncode(returnUrl.ToString()));
            context.Response.Redirect(url);
        }

        /// <summary>
        /// Requests authification info from the provider.
        /// </summary>
        public AuthenticationResult VerifyAuthentication(HttpContextBase context)
        {
            if (context == null)
                return new AuthenticationResult(false, ProviderName, null, null, null);
            var code = context.Request.QueryString["code"];
            var queryStr = context.Request.QueryString;
            var parameters = string.Join("&", 
                queryStr.AllKeys.Where(key => !"code".Equals(key, StringComparison.OrdinalIgnoreCase)).Select(key => string.Format("{0}={1}", key, queryStr[key])));
            var userData = GetUserData(code, string.Format("{0}?{1}", context.Request.UrlReferrer.GetLeftPart(UriPartial.Path), parameters));
            return userData == null ? 
                new AuthenticationResult(false, ProviderName, null, null, null) : 
                new AuthenticationResult(true, ProviderName, userData.Id.ToString(), userData.GetName(), null);
        }

        private VkUser GetUserData(string accessCode, string redirectUri)
        {
            //Get token info to retrieve user data.
            var token = GetHtml(
                string.Format(
                    "{0}{1}&redirect_uri={2}&client_secret={3}&code={4}",
                    GraphApiToken, _appId, HttpUtility.UrlEncode(redirectUri), _appSecret, accessCode));
            if (string.IsNullOrEmpty(token))
                return null;
            var accessTokenParam = JsonConvert.DeserializeObject<Dictionary<string, string>>(token);

            //Get users data using token info.
            var data = GetHtml(
                string.Format(
                    "{0}getProfiles?uid={1}&access_token={2}&fields=nickname", 
                    GraphApiMe, accessTokenParam["user_id"], accessTokenParam["access_token"]));
            
            //Get the first user from users data.
            var vkr = JsonConvert.DeserializeObject<VkResponce>(data);
            return vkr.GetFirstUser();
        }

        private static string GetHtml(string url)
        {
            var connectionString = url;
            var myRequest = (HttpWebRequest)WebRequest.Create(connectionString);
            myRequest.Credentials = CredentialCache.DefaultCredentials;
            using (var webResponse = myRequest.GetResponse())
            using (var respStream = webResponse.GetResponseStream())
                return respStream == null ? null : new StreamReader(respStream).ReadToEnd();
        }

        #region Internal Classes
        /// <summary>
        /// Desirialized data for responce.
        /// </summary>
        [JsonObject]
        internal class VkResponce
        {
            /// <summary>
            /// Default constructor.
            /// </summary>
            public VkResponce()
            {
                VkUserArray = new VkUser[1];
            }

            /// <summary>
            /// Retrieved array of users.
            /// </summary>
            [JsonProperty("response")]
            public VkUser[] VkUserArray { get; set; }

            /// <summary>
            /// Returns the first user in array or null if array is empty.
            /// </summary>
            public VkUser GetFirstUser()
            {
                return VkUserArray == null || VkUserArray.Length == 0 ? null : VkUserArray[0];
            }
        }


        /// <summary>
        /// Responce with user data from vk.com.
        /// </summary>
        [JsonObject]
        internal class VkUser
        {
            /// <summary>
            /// User's id.
            /// </summary>
            [JsonProperty("uid")]
            public int Id { get; set; }

            /// <summary>
            /// User's first name.
            /// </summary>
            [JsonProperty("first_name")]
            public string FirstName { get; set; }

            /// <summary>
            /// User's last name.
            /// </summary>
            [JsonProperty("last_name")]
            public string LastName { get; set; }

            /// <summary>
            /// User's nickname.
            /// </summary>
            [JsonProperty("nickname")]
            public string Nickname { get; set; }

            /// <summary>
            /// Returns user's name.
            /// </summary>
            public string GetName()
            {
                return string.IsNullOrEmpty(Nickname) ? string.Format("{0} {1}", FirstName, LastName) : Nickname;
            }
        }
        #endregion
    }
}
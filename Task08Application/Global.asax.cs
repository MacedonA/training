﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Microsoft.Web.WebPages.OAuth;
using Task08Application.Controllers;
using Task08Application.ExternalAuthification;
using Task08Application.Models;

namespace Task08Application
{
    /// <summary>
    /// An initialization class for application.
    /// </summary>
    public class MvcApplication : HttpApplication
    {
        /// <summary>
        /// Register routes.
        /// </summary>
        /// <param name="routes"></param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "ShowAll",
                "{controller}/{action}",
                new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
                "ShowDetails",
                "{controller}/{action}/{id:int}",
                new { controller = "Home", action = "Details", id = 0 }
            );
        }

        /// <summary>
        /// Do some initialization.
        /// </summary>
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RegisterRoutes(RouteTable.Routes);

            DataReader.CreateCachingSqlDependecies();
            
            OAuthWebSecurity.RegisterClient(
                   new VkAuthenticationClient("5417192", "L7ZrcwlqNrKxs73vgZpP"),
                   "vkClient",
                   null);
        }

        /// <summary>
        /// Show an error page for exception and log exception info.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContext ctx = HttpContext.Current;
            Exception ex = ctx.Server.GetLastError();
            ctx.Response.Clear();
            RequestContext rc = null;
            var mvcHandler = ctx.CurrentHandler as MvcHandler;
            if (mvcHandler != null)
                rc = mvcHandler.RequestContext;
            IController controller = new HomeController();
            var context = rc == null ? null : new ControllerContext(rc, (ControllerBase)controller);
            var viewResult = new ViewResult
            {
                ViewName = "Error", //We can use different views for different error codes but at the moment it does not matter.
                ViewData =
                {
                    Model =
                        new HandleErrorInfo(
                            ex, 
                            context == null ? "Home" : context.RouteData.GetRequiredString("controller"),
                            context == null ? "Index" : context.RouteData.GetRequiredString("action"))
                }
            };
            if (context != null)
                viewResult.ExecuteResult(context);
            ctx.Server.ClearError();
        }

        /// <summary>
        /// Set role according to data info.
        /// </summary>
        protected void Application_OnAuthenticateRequest(object src, EventArgs e)
        {
            if (HttpContext.Current.User == null)
                return;
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return;
            if (!(HttpContext.Current.User.Identity is FormsIdentity))
                return;
            var id = (FormsIdentity)HttpContext.Current.User.Identity;
            var ticket = id.Ticket;
            var userData = ticket.UserData;
            // Roles is a helper class which places the roles of the
            // currently logged on user into a string array
            // accessable via the value property.
            HttpContext.Current.User = new GenericPrincipal(id, new[] {userData});
        }
    }
}

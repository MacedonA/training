﻿using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace Task08Application.Validation
{
    /// <summary>
    /// Validates image to load.
    /// </summary>
    public class ValidateImageAttribute : RequiredAttribute
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public ValidateImageAttribute()
        {
            MaxSizeMb = 2;
        }

        /// <summary>
        /// Indicates is it required to add an error if file is empty.
        /// </summary>
        public bool CanBeEmpty { get; set; }

        /// <summary>
        /// Indicates a maximum file size.
        /// </summary>
        public int MaxSizeMb { get; set; }

        /// <summary>
        /// Validates image to load (it should be png or jpg image and size should not exceed <see cref="MaxSizeMb"/>).
        /// </summary>
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file == null)
                return CanBeEmpty;

            if (file.ContentLength > MaxSizeMb * 1024 * 1024)
                return false;

            try
            {
                using (var img = Image.FromStream(file.InputStream))
                {
                    file.InputStream.Seek(0, SeekOrigin.Begin);
                    return img.RawFormat.Equals(ImageFormat.Png) || img.RawFormat.Equals(ImageFormat.Jpeg);
                }
            }
            catch { }
            return false;
        }
    }
}
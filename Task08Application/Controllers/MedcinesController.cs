﻿using System.Collections.Generic;
using System.Web.Http;
using Task08Application.Models;
using Task08Application.Models.IndexViewModel;

namespace Task08Application.Controllers
{
    /// <summary>
    /// Provides some data as API.
    /// </summary>
    [AllowAnonymous]
    public class MedcinesController : ApiController
    {
        /// <summary>
        /// Returns info about all medcines.
        /// </summary>
        public IEnumerable<MedcineModel> GetAllMedcines()
        {
            return DataReader.GetAllMedcines();
        }

        /// <summary>
        /// Returns info about a medcine.
        /// </summary>
        public MedcineModel GetMedcine(int id)
        {
            return DataReader.GetOneMedcine(id);
        }
    }
}

﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using Microsoft.Web.WebPages.OAuth;
using Task08Application.ActionFilters;
using Task08Application.Models;
using Task08Application.Models.DetailsViewModel;
using Task08Application.Models.EditUserViewModel;
using Task08Application.Models.IndexViewModel;
using Task08Application.Models.LoginViewModel;
using Task08Application.Models.OrderViewModel;
using Task08Application.Models.RegisterViewModel;

namespace Task08Application.Controllers
{
    /// <summary>
    /// A default controller.
    /// </summary>
    [GZipCompress]
    public class HomeController : Controller
    {
        /// <summary>
        /// Opens a list of medcines (no filters, first page).
        /// </summary>
        [Route("Home/Index")]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View("Index", DataReader.GetIndexData(null));
        }

        /// <summary>
        /// Loads a list of medcines.
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult MedcineData(IndexViewModel model)
        {
            System.Threading.Thread.Sleep(1500);  //It is an emulation of long term action.
            return PartialView("MedcineData", DataReader.GetIndexData(model));
        }

        /// <summary>
        /// Opens details of medcine if possible.
        /// Show an error otherwise.
        /// </summary>
        [Route("Home/Details?{int?:id}")]
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return View("Error");
            var data = DataReader.GetDetailsData(id.Value);
            return data == null ? 
                View("Error") : View("Details", data);
        }

        /// <summary>
        /// Opens order form for medcine if possible.
        /// Show an error otherwise.
        /// </summary>
        [Route("Home/Order?{int?:id}")]
        [Authorize(Roles = "admininstrator,user")]
        public ActionResult Order(int? id)
        {
            if (id == null)
                return View("Error");
            OrderViewModel data =TryGetDataFromSessionState<OrderViewModel>("OrderViewModel");
            if (data != null)
            {
                TryValidateModel(data);
            }
            else
            {
                data = new OrderViewModel();
                var ck = HttpContext == null ? null : HttpContext.Request.Cookies["order"];
                if (ck != null)
                    data = CookieSerializer.Deserialize<OrderViewModel>(ck.Value) ?? new OrderViewModel();
                data.Medcine = DataReader.GetOrderMedcine(id.Value);
            }
            return data.Medcine == null ?
                View("Error") : View("Order", data);
        }

        /// <summary>
        /// Validates an order. Saves it into database if valid and opens medcine's list.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = "admininstrator,user")]
        public ActionResult Order(OrderViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var result = RedirectToAction("Order", new { id = model.Medcine.MedcineId });
                SetDataToSessionState("OrderViewModel", DataReader.GetOrderData(model));
                return result;
            }
            try
            {
                if (HttpContext != null)
                {
                    var toStore = CookieSerializer.Serialize(model);
                    if (toStore != null)
                        HttpContext.Response.Cookies.Add(new HttpCookie("order", toStore));
                }
                DataReader.SaveOrder(model);
            }
            catch (Exception)
            {
                return View("Error");
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Opens edit form for medcine if possible.
        /// Show an error otherwise.
        /// </summary>
        [Route("Home/EditMedcine?{int?:id}")]
        [Authorize(Roles = "admininstrator")]
        public ActionResult EditMedcine(int? id)
        {
            if (id == null)
                return View("Error");

            EditDetailsViewModel data = TryGetDataFromSessionState<EditDetailsViewModel>("EditDetailsViewModel");
            if (data != null)
            {
                DataReader.FillLists(data);
                DataReader.FillImage(data);
                TryValidateModel(data);
            }
            else
            {
                data = DataReader.GetEditDetailsData(id.Value);
            }
            return data == null ? View("Error") : View("EditMedcine", data);
        }

        /// <summary>
        /// Validates a medcine. Saves it into database if valid and opens medcine's list.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = "admininstrator")]
        public ActionResult EditMedcine(EditDetailsViewModel model)
        {
            DataReader.ValidateEditSelectedIds(model).ForEach(errorKey =>
                ModelState.AddModelError(errorKey, "Такого идентификатора не существет."));
            if (!ModelState.IsValid)
            {
                var result = RedirectToAction("EditMedcine", new {id=model.MedcineId});
                SetDataToSessionState("EditDetailsViewModel", model);
                return result;
            }
            try
            {
                DataReader.SaveMedcine(model);
            }
            catch (Exception)
            {
                return View("Error");
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Opens an edit form to add a medcine.
        /// </summary>
        [Route("Home/AddMedcine")]
        [Authorize(Roles = "admininstrator")]
        public ActionResult AddMedcine()
        {
            var data = new EditDetailsViewModel();
            DataReader.FillLists(data);
            return View("EditMedcine", data);
        }

        /// <summary>
        /// Validates a medcine. Inserts it into database if valid and opens medcine's list.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = "admininstrator")]
        public ActionResult AddMedcine(EditDetailsViewModel model)
        {
            DataReader.ValidateEditSelectedIds(model).ForEach(errorKey =>
                ModelState.AddModelError(errorKey, "Такого идентификатора не существет."));
            if (!ModelState.IsValid)
            {
                DataReader.FillLists(model);
                var result = RedirectToAction("AddMedcine");
                TempData["EditDetailsViewModel"] = model;
                return result;
            }
            try
            {
                DataReader.SaveMedcine(model);
            }
            catch (Exception)
            {
                return View("Error");
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Tries to delete medcine and opens medcine's list.
        /// Show an error otherwise.
        /// </summary>
        [Route("Home/DeleteMedcine?{int?:id}")]
        [Authorize(Roles = "admininstrator")]
        public ActionResult DeleteMedcine(int? id)
        {
            if (id == null)
                return View("Error");
            try
            {
                DataReader.DeleteMedcine(id.Value);
            }
            catch (Exception)
            {
                return View("Error");
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Emulates a login action.
        /// </summary>
        [Route("Home/Login")]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index");
            var externalAuth = OAuthWebSecurity.VerifyAuthentication();
            if (externalAuth.IsSuccessful)
            {
                CreateAuthCookie(externalAuth.UserName, "user");
                return RedirectToAction("Index");
            }
            LoginViewModel model;
            if (TempData.ContainsKey("LoginInfo"))
            {
                model = TempData["LoginInfo"] as LoginViewModel;
                if (model != null)
                    TryValidateModel(model);
            }
            else
            {
                model = new LoginViewModel {RedirectUrl = returnUrl};
            }
            return View("Login", model);
        }

        /// <summary>
        /// Emulates a login action.
        /// </summary>
        [Route("Home/Login")]
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var role = DataReader.ValidateUserAndGetRole(model.Login, model.Password);
                if (!string.IsNullOrEmpty(role))
                {
                    CreateAuthCookie(model.Login, role);
                    if (model.RedirectUrl != null)
                        return Redirect(model.RedirectUrl);
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("Password", "Введённые данные некорректны.");
            }
            TempData["LoginInfo"] = model;
            return View("Login", model);
        }

        /// <summary>
        /// Emulates a logout action.
        /// </summary>
        [Route("Home/Login")]
        [AllowAnonymous]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            TempData.Remove("LoginInfo");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Generates an exception.
        /// </summary>
        [Route("Home/DoError")]
        [AllowAnonymous]
        public ActionResult DoError()
        {
            throw new NullReferenceException("Test Exception", new ArgumentException("Test Inner Exception"));
        }
        
        /// <summary>
        /// Shows a medcine image. Should be cached.
        /// </summary>
        [Route("Home/ShowMedcineImage")]
        [AllowAnonymous]
        [HttpGet]
        [OutputCache(SqlDependency = "MedcineData:MedcineImages", Duration = 10, VaryByParam = "id", Location = OutputCacheLocation.Any)]
        public ActionResult ShowMedcineImage(int id)
        {
            ViewBag.MedcineImage = DataReader.GetMedcineImage(id);
            return PartialView("MedcineImageView");
        }

        /// <summary>
        /// Applies a register action.
        /// </summary>
        [Route("Home/Register")]
        [AllowAnonymous]
        public ActionResult Register()
        {
            RegisterViewModel model;
            if (TempData.ContainsKey("RegisterInfo"))
            {
                model = TempData["RegisterInfo"] as RegisterViewModel;
                if (model != null)
                    TryValidateModel(model);
            }
            else
            {
                model = new RegisterViewModel();
            }
            return View("Register", model);
        }

        /// <summary>
        /// Applies a register action.
        /// </summary>
        [Route("Home/Register")]
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var role = DataReader.TryCreateNewUser(model.Login, model.Password);
                if (!string.IsNullOrEmpty(role))
                {
                    CreateAuthCookie(model.Login, role);
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("Login", "Введённые данные некорректны. Вероятно такой пользователь уже существует.");
            }
            TempData["RegisterInfo"] = model;
            return View("Register", model);
        }

        /// <summary>
        /// Opens edit user form.
        /// </summary>
        [Route("Home/EditUser")]
        [Authorize(Roles = "admininstrator")]
        public ActionResult EditUser()
        {
            return View("EditUserSelection", DataReader.GetUserViewModel(0));
        }

        /// <summary>
        /// Loads additional details to edit user form.
        /// </summary>
        [Route("Home/EditUserDetails")]
        [Authorize(Roles = "admininstrator")]
        [HttpPost]
        public ActionResult EditUserDetails(EditUserSelectionViewModel model)
        {
            return PartialView("EditUserDetails", DataReader.GetUserDetalisViewModel(model.SelectedUser));
        }

        /// <summary>
        /// Tries to save user details, reloads data.
        /// </summary>
        [Route("Home/SaveUserDetails")]
        [Authorize(Roles = "admininstrator")]
        [HttpPost]
        public ActionResult SaveUserDetails(EditUserSelectionViewModel model, EditUserDetailsViewModel detailsModel)
        {
            var lgn = DataReader.GetUserLogin(model.SelectedUser);
            if (lgn == "admin")
                ModelState.AddModelError("SelectedUser", "Вы не можете изменить этого пользователя");
            else if (HttpContext != null && HttpContext.User.Identity.Name == lgn)
                ModelState.AddModelError("SelectedUser", "Вы не можете изменить самого себя");
            if (ModelState.IsValid)
                DataReader.SaveUser(model.SelectedUser, detailsModel.CurerntRole);
            return PartialView("EditUserSelection", DataReader.GetUserViewModel(model.SelectedUser));
        }

        /// <summary>
        /// Performs authorisation using vk provider.
        /// </summary>
        [HttpGet]
        public ActionResult VkReg()
        {
            SetDataToSessionState("redirectAfterLogin", HttpContext.Request.UrlReferrer);
            OAuthWebSecurity.RequestAuthentication("vkClient", "~/Home/Login");
            return null;
        }

        private T TryGetDataFromSessionState<T>(string key) where T : class
        {
            if (HttpContext == null || HttpContext.Session == null)
                return null;
            var result = HttpContext.Session[key] as T;
            HttpContext.Session[key] = null;
            return result;
        }

        private void SetDataToSessionState<T>(string key, T data) where T : class
        {
            if (HttpContext == null || HttpContext.Session == null)
                return;
            HttpContext.Session[key] = data;
        }

        private void CreateAuthCookie(string login, string role)
        {
            var authTicket = new FormsAuthenticationTicket(1, login, DateTime.Now, DateTime.Now.AddMinutes(20), true, role);
            var encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            if (HttpContext != null)
                HttpContext.Response.Cookies.Add(authCookie);
        }
    }
}
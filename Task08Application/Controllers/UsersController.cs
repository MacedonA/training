﻿using System.Collections.Generic;
using System.Web.Http;
using Task08Application.Models;

namespace Task08Application.Controllers
{
    /// <summary>
    /// Provides some data as API.
    /// </summary>
    [AllowAnonymous]
    public class UsersController : ApiController
    {
        /// <summary>
        /// Returns info about all users.
        /// </summary>
        [HttpGet]
        public IEnumerable<UserModel> AllUsers()
        {
            return DataReader.GetAllUsers();
        }

        /// <summary>
        /// Returns info about an user.
        /// </summary>
        [HttpGet]
        public UserModel OneUser(int id)
        {
            return DataReader.GetOneUser(id);
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        [HttpPost]
        public bool RegisterUser([FromBody]UserModel user)
        {
            return DataReader.TryCreateNewUser(user.Login, user.Password) != null;
        }
    }
}

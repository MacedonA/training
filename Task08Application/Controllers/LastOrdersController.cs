﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Http;
using Task08Application.Models;

namespace Task08Application.Controllers
{
    /// <summary>
    /// Returns list of the latest orders.
    /// </summary>
    public class LastOrdersController : ApiController
    {
        /// <summary>
        /// Gets the list of the latest orders.
        /// </summary>
        public IEnumerable<Tuple<int, string>> GetLastOrderedMedcines(int id = 5)
        {
            Thread.Sleep(1000);
            return DataReader.GetLastOrderedMedcines(id).Select(ord => new Tuple<int, string>(ord.Medcine.MedcineId, ord.Medcine.MedcineName));
        }
    }
}

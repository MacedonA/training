﻿using System.IO.Compression;
using System.Web.Mvc;

namespace Task08Application.ActionFilters
{
    /// <summary>
    /// An action filter for gzip compression.
    /// </summary>
    public class GZipCompressAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Include gzip compression if it is possible.
        /// A sample is taken from http://metanit.com/sharp/mvc5/8.6.php.
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            var acceptEncoding = request.Headers["Accept-Encoding"];
            if (string.IsNullOrEmpty(acceptEncoding))
                return;
            acceptEncoding = acceptEncoding.ToUpperInvariant();
            var response = filterContext.HttpContext.Response;
            if (!acceptEncoding.Contains("GZIP")) 
                return;
            response.AppendHeader("Content-encoding", "gzip");
            response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
        }
    }
}
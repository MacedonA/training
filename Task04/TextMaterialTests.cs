﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task01.Classes;
using Task01.Interfaces;
using Task04.Helpers;

namespace Task04
{
    /// <summary>
    /// Tests <see cref="TextMaterial"/> class.
    /// </summary>
    [TestClass]
    public class TextMaterialTests
    {
        /// <summary>
        /// Tests <see cref="TextMaterial"/> constructor with wrong data.
        /// </summary>
        [TestMethod]
        public void Create_WrongData_Exception()
        {
            var isExpected = HelperMethods.ThrowsExceptionOfType(() => new TextMaterial("Desc", null), typeof(ArgumentNullException));
            Assert.IsTrue(isExpected, "'new TextMaterial(\"Desc\", null)' doesn't throw an exception of ArgumentNullException type");

            isExpected = HelperMethods.ThrowsExceptionOfType(() => new TextMaterial("Desc", new string('c', 200000)), typeof(ArgumentException));
            Assert.IsTrue(isExpected, "'new TextMaterial(\"Desc\", new string('c', 200000))' doesn't throw an exception of ArgumentException type");
        }

        /// <summary>
        /// Tests <see cref="TextMaterial"/> constructor with correct data.
        /// </summary>
        [TestMethod]
        public void Create_CorrectData_Stored()
        {
            const string text = "This is text.";
            var entry = new TextMaterial("Desc", text);
            Assert.AreEqual(text, entry.Text);
        }

        /// <summary>
        /// Tests <see cref="IEducationalMaterial.CreateCopy"/> implementation.
        /// </summary>
        [TestMethod]
        public void CreateCopy_CopiesExceptOfID()
        {
            const string description = "This is description.";
            const string text = "This is text.";
            var entry = new TextMaterial(description, text);
            var copy = entry.CreateCopy() as TextMaterial;
            Assert.IsNotNull(copy);
            Assert.AreEqual(entry.Description, copy.Description);
            Assert.AreEqual(entry.Text, copy.Text);
            Assert.AreNotEqual(entry.ID, copy.ID);
        }
    }
}

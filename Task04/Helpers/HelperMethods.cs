﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task04.Helpers
{
    /// <summary>
    /// Stores helper methods.
    /// </summary>
    public static class HelperMethods
    {
        /// <summary>
        /// Makes sure that action throws exception of defined type.
        /// </summary>
        public static bool ThrowsExceptionOfType(Action toDo, Type expected)
        {
            try
            {
                toDo();
            }
            catch (Exception e)
            {
                if (e.GetType() == expected)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a random version.
        /// </summary>
        public static byte[] CreateVersion()
        {
            var version = new byte[8];
            new Random().NextBytes(version);
            return version;
        }

        /// <summary>
        /// Compare versions.
        /// </summary>
        public static void CompareVersions(byte[] expected, byte[] current)
        {
            Assert.AreEqual(expected.Length, current.Length);
            for (int i = 0; i < expected.Length; i++)
                Assert.AreEqual(expected[i], current[i], string.Format("Iteration {0}.", i));
        }
    }
}
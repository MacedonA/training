﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task01.Classes;
using Task01.Extensions;

namespace Task04
{
    /// <summary>
    /// Tests <see cref="EntryBaseExtension"/> class.
    /// </summary>
    [TestClass]
    public class EntryBaseExtensionTests
    {
        /// <summary>
        /// Tests <see cref="EntryBaseExtension.CreateID"/> method.
        /// </summary>
        [TestMethod]
        public void CreateID_ChangesID()
        {
            var entry = new EntryBase("test");
            var oldId = entry.ID;
            entry.CreateID();
            Assert.AreNotEqual(oldId, entry.ID);
        }
    }
}
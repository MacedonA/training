﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task01.Classes;
using Task01.Enums;
using Task01.Interfaces;
using Task04.Helpers;

namespace Task04
{
    /// <summary>
    /// Tests <see cref="Lesson"/> class.
    /// </summary>
    [TestClass]
    public class LessonTests
    {
        /// <summary>
        /// Tests <see cref="Lesson"/> constructor with wrong data.
        /// </summary>
        [TestMethod]
        public void Create_WrongData_Exception()
        {
            var isExpected = HelperMethods.ThrowsExceptionOfType(() => new Lesson("Desc", new byte[8], null), typeof(ArgumentNullException));
            Assert.IsTrue(isExpected, "'new Lesson(\"Desc\", new byte[8], null)' doesn't throw an exception of ArgumentNullException type");
        }

        /// <summary>
        /// Tests <see cref="Lesson"/> constructor with correct data.
        /// </summary>
        [TestMethod]
        public void Create_CorrectData_Stored()
        {
            var materials = new IEducationalMaterial[]
            {
                new ReferenceMaterial("desc1", ReferenceTypes.Global, new Uri("http://test123.com")),
                new VideoMaterial("desc2", HelperMethods.CreateVersion(), VideoFormats.Mkv, new Uri("http://test234.com"), new Uri("http://test345.com")),
                new TextMaterial("desc3", "text")
            };
            var entry = new Lesson("Desc", new byte[8], materials);
            Assert.AreEqual(materials.Length, entry.Materials.Length);
            for (int i = 0; i < materials.Length; i++)
                Assert.AreEqual(materials[i], entry.Materials[i], string.Format("Iteration {0}.", i));
        }

        /// <summary>
        /// Tests <see cref="Lesson.KindOfLesson"/> property.
        /// </summary>
        [TestMethod]
        public void KindOfLesson_ReturnedValue()
        {
            var materials = new IEducationalMaterial[]
            {
                new ReferenceMaterial("desc1", ReferenceTypes.Global, new Uri("http://test123.com")),
                new VideoMaterial("desc2", HelperMethods.CreateVersion(), VideoFormats.Mkv, new Uri("http://test234.com"), new Uri("http://test345.com")),
                new TextMaterial("desc3", "text")
            };
            var entry = new Lesson("Desc", new byte[8], materials);
            Assert.AreEqual(KindsOfLesson.VideoLesson, entry.KindOfLesson());

            materials = new IEducationalMaterial[]
            {
                new ReferenceMaterial("desc1", ReferenceTypes.Global, new Uri("http://test123.com")),
                new TextMaterial("desc3", "text"),
                new ReferenceMaterial("desc4", ReferenceTypes.Local, new Uri("http://test456.com")),
            };
            entry = new Lesson("Desc", new byte[8], materials);
            Assert.AreEqual(KindsOfLesson.TextLesson, entry.KindOfLesson());
        }

        /// <summary>
        /// Tests <see cref="ICloneable.Clone"/> implementation.
        /// </summary>
        [TestMethod]
        public void Clone_MaterialCopies()
        {
            const string description = "This is description.";
            var version = HelperMethods.CreateVersion();
            var materials = new IEducationalMaterial[]
            {
                new ReferenceMaterial("desc1", ReferenceTypes.Global, new Uri("http://test123.com")),
                new VideoMaterial("desc2", HelperMethods.CreateVersion(), VideoFormats.Mkv, new Uri("http://test234.com"), new Uri("http://test345.com")),
                new TextMaterial("desc3", "text")
            };
            var entry = new Lesson(description, version, materials);
            var clone = entry.Clone() as Lesson;
            Assert.IsNotNull(clone);
            Assert.AreEqual(clone.Description, entry.Description);
            HelperMethods.CompareVersions(clone.Version, entry.Version);
            Assert.AreEqual(clone.Materials.Length, entry.Materials.Length);
            for (int i = 0; i < clone.Materials.Length; i++)
                Assert.AreNotSame(clone.Materials[i], entry.Materials[i], string.Format("Iteration {0}.", i));
            Assert.AreNotEqual(clone.ID, entry.ID);
        }
    }
}

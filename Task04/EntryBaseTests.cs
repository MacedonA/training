﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task01.Classes;
using Task04.Helpers;

namespace Task04
{
    /// <summary>
    /// Tests <see cref="EntryBase"/> class.
    /// </summary>
    [TestClass]
    public class EntryBaseTests
    {
        /// <summary>
        /// Tests <see cref="EntryBase"/> constructor with wrong data.
        /// </summary>
        [TestMethod]
        public void Create_WrongData_Exception()
        {
            var isExpected = HelperMethods.ThrowsExceptionOfType(() => new EntryBase(null), typeof(ArgumentNullException));
            Assert.IsTrue(isExpected, "'new EntryBase(null)' doesn't throw an exception of ArgumentNullException type");

            isExpected = HelperMethods.ThrowsExceptionOfType(() => new EntryBase(new string('c', 200000)), typeof(ArgumentException));
            Assert.IsTrue(isExpected, "'new EntryBase(new string('c', 200000))' doesn't throw an exception of ArgumentException type");
        }

        /// <summary>
        /// Tests <see cref="EntryBase"/> constructor with correct data.
        /// </summary>
        [TestMethod]
        public void Create_CorrectData_Stored()
        {
            const string description = "This is description.";
            var entry = new EntryBase(description);
            Assert.AreEqual(description, entry.Description);
        }

        /// <summary>
        /// Tests <see cref="EntryBase.ToString"/> override.
        /// </summary>
        [TestMethod]
        public void ToString_DescriptionShown()
        {
            const string description = "This is description 2.";
            var entry = new EntryBase(description);
            Assert.AreEqual(description, entry.ToString());
        }

        /// <summary>
        /// Tests <see cref="EntryBase.Equals(object)"/> override.
        /// </summary>
        [TestMethod]
        public void Equality()
        {
            const string description = "This is description 3.";
            var entry1 = new EntryBase(description);
            var entry2 = new EntryBase(description);
            Assert.AreNotEqual(entry1, entry2);
            Assert.AreEqual(entry1, entry1);
        }
    }
}

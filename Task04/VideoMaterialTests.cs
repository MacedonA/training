﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task01.Classes;
using Task01.Enums;
using Task01.Interfaces;
using Task04.Helpers;

namespace Task04
{
    /// <summary>
    /// Tests <see cref="VideoMaterial"/> class.
    /// </summary>
    [TestClass]
    public class VideoMaterialTests
    {
        /// <summary>
        /// Tests <see cref="VideoMaterial"/> constructor with wrong data.
        /// </summary>
        [TestMethod]
        public void Create_WrongData_Exception()
        {
            var isExpected = HelperMethods.ThrowsExceptionOfType(() => new TextMaterial("Desc", null), typeof(ArgumentNullException));
            Assert.IsTrue(isExpected, "'new TextMaterial(\"Desc\", null)' doesn't throw an exception of ArgumentNullException type");

            isExpected = HelperMethods.ThrowsExceptionOfType(() => new TextMaterial("Desc", new string('c', 200000)), typeof(ArgumentException));
            Assert.IsTrue(isExpected, "'new TextMaterial(\"Desc\", new string('c', 200000))' doesn't throw an exception of ArgumentException type");
        }

        /// <summary>
        /// Tests <see cref="VideoMaterial"/> constructor with correct data.
        /// </summary>
        [TestMethod]
        public void Create_CorrectData_Stored()
        {
            const VideoFormats format = VideoFormats.Mp4;
            var video = new Uri("http://test123.com");
            var miniature = new Uri("http://test234.com");
            var entry = new VideoMaterial("Desc", new byte[8], format, video, miniature);
            Assert.AreEqual(format, entry.VideoFormat);
            Assert.AreEqual(video, entry.VideoReference);
            Assert.AreEqual(miniature, entry.MiniatureReference);
        }

        /// <summary>
        /// Tests <see cref="IEducationalMaterial.CreateCopy"/> implementation.
        /// </summary>
        [TestMethod]
        public void CreateCopy_CopiesExceptOfID()
        {
            const string description = "This is description.";
            var version = HelperMethods.CreateVersion();
            const VideoFormats format = VideoFormats.Mp4;
            var video = new Uri("http://test345.com");
            var miniature = new Uri("http://test456.com");
            var entry = new VideoMaterial(description, version, format, video, miniature);
            var copy = entry.CreateCopy() as VideoMaterial;
            Assert.IsNotNull(copy);
            Assert.AreEqual(entry.Description, copy.Description);
            Assert.AreEqual(entry.Version.Length, copy.Version.Length);
            HelperMethods.CompareVersions(version, entry.Version);
            Assert.AreEqual(entry.VideoFormat, copy.VideoFormat);
            Assert.AreEqual(entry.VideoReference, copy.VideoReference);
            Assert.AreEqual(entry.MiniatureReference, copy.MiniatureReference);
            Assert.AreNotEqual(entry.ID, copy.ID);
        }
    }
}

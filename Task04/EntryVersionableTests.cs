﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task01.Classes;
using Task04.Helpers;

namespace Task04
{
    /// <summary>
    /// Tests <see cref="EntryVersionable"/> class.
    /// </summary>
    [TestClass]
    public class EntryVersionableTests
    {
        /// <summary>
        /// Tests <see cref="EntryVersionable"/> constructor with wrong data.
        /// </summary>
        [TestMethod]
        public void Create_WrongData_Exception()
        {
            var isExpected = HelperMethods.ThrowsExceptionOfType(() => new EntryVersionable("Desc", null), typeof(ArgumentNullException));
            Assert.IsTrue(isExpected, "'new EntryVersionable(\"Desc\", null)' doesn't throw an exception of ArgumentNullException type");

            isExpected = HelperMethods.ThrowsExceptionOfType(() => new EntryVersionable("Desc", new byte[9]), typeof(ArgumentException));
            Assert.IsTrue(isExpected, "'new EntryVersionable(\"Desc\", new byte[9])' doesn't throw an exception of ArgumentException type");
        }

        /// <summary>
        /// Tests <see cref="EntryVersionable"/> constructor with correct data.
        /// </summary>
        [TestMethod]
        public void Create_CorrectData_Stored()
        {
            var version = HelperMethods.CreateVersion();
            var entry = new EntryVersionable("Desc", version);
            HelperMethods.CompareVersions(version, entry.Version);
        }
    }
}

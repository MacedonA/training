﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task01.Classes;
using Task01.Enums;
using Task01.Interfaces;
using Task04.Helpers;

namespace Task04
{
    /// <summary>
    /// Tests <see cref="ReferenceMaterial"/> class.
    /// </summary>
    [TestClass]
    public class ReferenceMaterialTests
    {
        /// <summary>
        /// Tests <see cref="ReferenceMaterial"/> constructor with wrong data.
        /// </summary>
        [TestMethod]
        public void Create_WrongData_Exception()
        {
            var isExpected = HelperMethods.ThrowsExceptionOfType(() => new ReferenceMaterial("Desc", ReferenceTypes.Global, null), typeof(ArgumentNullException));
            Assert.IsTrue(isExpected, "'new ReferenceMaterial(\"Desc\", ReferenceTypes.Global, null)' doesn't throw an exception of ArgumentNullException type");
        }

        /// <summary>
        /// Tests <see cref="ReferenceMaterial"/> constructor with correct data.
        /// </summary>
        [TestMethod]
        public void Create_CorrectData_Stored()
        {
            ReferenceTypes type = ReferenceTypes.Local;
            var uri = new Uri("http://test.com");
            var entry = new ReferenceMaterial("Desc", type, uri);
            Assert.AreEqual(type, entry.ReferenceType);
            Assert.AreEqual(uri, entry.Reference);
        }

        /// <summary>
        /// Tests <see cref="IEducationalMaterial.CreateCopy"/> implementation.
        /// </summary>
        [TestMethod]
        public void CreateCopy_CopiesExceptOfID()
        {
            const string description = "This is description.";
            const ReferenceTypes type = ReferenceTypes.Global;
            var uri = new Uri("http://test123.com");
            var entry = new ReferenceMaterial(description, type, uri);
            var copy = entry.CreateCopy() as ReferenceMaterial;
            Assert.IsNotNull(copy);
            Assert.AreEqual(entry.Description, copy.Description);
            Assert.AreEqual(entry.Reference, copy.Reference);
            Assert.AreEqual(entry.ReferenceType, copy.ReferenceType);
            Assert.AreNotEqual(entry.ID, copy.ID);
        }
    }
}

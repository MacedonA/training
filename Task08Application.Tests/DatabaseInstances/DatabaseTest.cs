﻿using System.Data.Entity;
using System.Linq;
using NUnit.Framework;
using Task08Application.Controllers;
using Task08DataAccessLayer.BaseTypes;
using Task08DataAccessLayer.Entities;
using Task08DataAccessLayer.Model;
using Assert = NUnit.Framework.Assert;

namespace Task08Application.Tests.DatabaseInstances
{
    /// <summary>
    /// Test <see cref="HomeController"/>.
    /// </summary>
    [TestFixture]
    public class DatabaseTest
    {
        /// <summary>
        /// Drop test database and recreate it for the test.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            using (var context = new MedcineDbContext())
            {
                if (context.Database.Exists())
                    Database.Delete(context.Database.Connection.ConnectionString);
            }
        }

        /// <summary>
        /// Tests <see cref="IRepository{T}.GetById"/> method.
        /// </summary>
        [Test]
        public void GetByIdTest()
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var item = uow.MedcinesRepository.GetById(2);
                Assert.IsNotNull(item);
                Assert.AreEqual("Парацетомол", item.ActiveSubstance.ActiveSubstanceName);
                Assert.AreEqual("РосФарм", item.Manufacturer.ManufacturerName);
                Assert.AreEqual("Полезно", item.MedcineAnnotation);
                Assert.AreEqual("Жидкость наружного применения", item.MedcineForm.MedcineFormName);
                Assert.IsNotNull(item.MedcineImage);
                Assert.AreEqual(2, item.MedcineId);
                Assert.AreEqual("Что-то полезное", item.MedcineName);
                item = uow.MedcinesRepository.GetById(4);
                Assert.IsNotNull(item);
                Assert.AreEqual("Озверин", item.ActiveSubstance.ActiveSubstanceName);
                Assert.AreEqual("БелФарм", item.Manufacturer.ManufacturerName);
                Assert.IsNull(item.MedcineAnnotation);
                Assert.AreEqual("Сироп", item.MedcineForm.MedcineFormName);
                Assert.IsNull(item.MedcineImage);
                Assert.AreEqual(4, item.MedcineId);
                Assert.AreEqual("Оттакмвон", item.MedcineName);
                item = uow.MedcinesRepository.GetById(-1);
                Assert.IsNull(item);
                item = uow.MedcinesRepository.GetById(11);
                Assert.IsNull(item);
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Tests <see cref="IRepository{T}.GetByQuery"/> method.
        /// </summary>
        [Test]
        public void GetByQueryTest()
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var itemsInit = uow.ActiveSubstancesRepository.GetByQuery(
                    substance => substance.ActiveSubstanceId % 2 == 1 || substance.ActiveSubstanceId >= 3,
                    substances => substances.OrderByDescending(sub => sub.ActiveSubstanceName));
                Assert.IsFalse(itemsInit is IQueryable);
                var items = itemsInit.ToList();
                Assert.AreEqual(4, items.Count);
                Assert.AreEqual(3, items[0].ActiveSubstanceId);
                Assert.AreEqual("Парацетомол", items[0].ActiveSubstanceName);
                Assert.AreEqual(5, items[1].ActiveSubstanceId);
                Assert.AreEqual("Озверин", items[1].ActiveSubstanceName);
                Assert.AreEqual(4, items[2].ActiveSubstanceId);
                Assert.AreEqual("Нитроглицирин", items[2].ActiveSubstanceName);
                Assert.AreEqual(1, items[3].ActiveSubstanceId);
                Assert.AreEqual("Активированный уголь", items[3].ActiveSubstanceName);
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Tests <see cref="IRepository{T}.GetAsQuery"/> method.
        /// </summary>
        [Test]
        public void GetAsQueryTest()
        {
            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var itemsInit = uow.MedcineFormsRepository.GetAsQuery(
                    form => form.MedcineFormId % 2 == 1 || form.MedcineFormId >= 3,
                    forms => forms.OrderByDescending(frm => frm.MedcineFormName));
                Assert.IsTrue(itemsInit is IQueryable);
                var items = itemsInit.ToList();
                Assert.AreEqual(3, items.Count);
                Assert.AreEqual(1, items[0].MedcineFormId);
                Assert.AreEqual("Таблетки", items[0].MedcineFormName);
                Assert.AreEqual(3, items[1].MedcineFormId);
                Assert.AreEqual("Сироп", items[1].MedcineFormName);
                Assert.AreEqual(4, items[2].MedcineFormId);
                Assert.AreEqual("Мазь", items[2].MedcineFormName);
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Tests <see cref="IRepositoryExt{T}.Insert"/> and <see cref="IRepositoryExt{T}.Delete"/> methods.
        /// </summary>
        [Test]
        public void InsertDeleteTest()
        {
            int oldCount;
            var newOrder = new Order
            {
                MedcineId = 9,
                Address = "Here is address",
                Comments = "To be or not to be.",
                EMailAddress = "asdf@asdf.com",
                FirstName = "Somebody",
                LastName = "Nobody",
                PhoneNumber = "+1123234214345"
            };

            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var repository = uow.OrdersRepository;
                oldCount = repository.GetAsQuery(item => true).Count();
                repository.Insert(newOrder);
                uow.Commit();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }

            uow = facade.GetUnitOfWork();
            try
            {
                var repository = uow.OrdersRepository;
                Assert.AreEqual(oldCount + 1, repository.GetAsQuery(item => true).Count());
                var inserted = repository.GetById(newOrder.OrderId);
                Assert.IsNotNull(inserted);
                Assert.AreEqual(newOrder.MedcineId, inserted.MedcineId);
                Assert.AreEqual(newOrder.Address, inserted.Address);
                Assert.AreEqual(newOrder.Comments, inserted.Comments);
                Assert.AreEqual(newOrder.EMailAddress, inserted.EMailAddress);
                Assert.AreEqual(newOrder.FirstName, inserted.FirstName);
                Assert.AreEqual(newOrder.LastName, inserted.LastName);
                Assert.AreEqual(newOrder.PhoneNumber, inserted.PhoneNumber);
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }


            int id;
            uow = facade.GetUnitOfWork();
            try
            {
                var repository = uow.OrdersRepository;
                var inserted = repository.GetById(newOrder.OrderId);
                id = inserted.OrderId;
                repository.Delete(inserted);
                uow.Commit();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }

            uow = facade.GetUnitOfWork();
            try
            {
                var repository = uow.OrdersRepository;
                Assert.AreEqual(oldCount, repository.GetAsQuery(item => true).Count());
                Assert.IsNull(repository.GetById(id));
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }

        /// <summary>
        /// Tests <see cref="IRepositoryExt{T}.Update"/> method.
        /// </summary>
        [Test]
        public void UpdateTest()
        {
            int oldCount;
            var updateData = new Medcine
            {
                MedcineId = 3,
                MedcineName = "New name",
                MedcineAnnotation = "New annotation",
                ActiveSubstanceId = 3,
                ManufacturerId = 2,
                MedcineFormId = 4,
                MedcineImageId = 1
            };

            var facade = new Facade();
            var uow = facade.GetUnitOfWork();
            try
            {
                var repository = uow.MedcinesRepository;
                oldCount = repository.GetAsQuery(item => true).Count();
                repository.Update(updateData);
                uow.Commit();
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }

            uow = facade.GetUnitOfWork();
            try
            {
                var repository = uow.MedcinesRepository;
                Assert.AreEqual(oldCount, repository.GetAsQuery(item => true).Count());
                var updated = repository.GetById(updateData.MedcineId);
                Assert.IsNotNull(updated);
                Assert.AreEqual(updateData.MedcineName, updated.MedcineName);
                Assert.AreEqual(updateData.MedcineAnnotation, updated.MedcineAnnotation);
                Assert.AreEqual(updateData.ActiveSubstanceId, updated.ActiveSubstanceId);
                Assert.AreEqual(updateData.ManufacturerId, updated.ManufacturerId);
                Assert.AreEqual(updateData.MedcineFormId, updated.MedcineFormId);
                Assert.AreEqual(updateData.MedcineImageId, updated.MedcineImageId);
            }
            finally
            {
                facade.ReturnUnitOfWork();
            }
        }
    }
}

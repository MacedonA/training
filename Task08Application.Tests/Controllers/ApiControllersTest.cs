﻿using System.Data.Entity;
using System.Linq;
using NUnit.Framework;
using Task08Application.Controllers;
using Task08Application.Models;
using Task08Application.Models.IndexViewModel;
using Task08DataAccessLayer.Model;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace Task08Application.Tests.Controllers
{
    /// <summary>
    /// Test <see cref="HomeController"/>.
    /// </summary>
    [TestFixture]
    public class ApiControllersTest
    {
        private readonly System.Collections.Generic.List<MedcineModel> _medcines = new System.Collections.Generic.List<MedcineModel>
        {
            new MedcineModel { MedcineId = 1, MedcineName = "Лекарство №1", ActiveSubstanceName = "Активированный уголь", ManufacturerName = "БелФарм", MedcineAnnotation = null, MedcineFormName = "Мазь" },
            new MedcineModel { MedcineId = 2, MedcineName = "Что-то полезное", ActiveSubstanceName = "Парацетомол", ManufacturerName = "РосФарм", MedcineAnnotation = "Полезно", MedcineFormName = "Жидкость наружного применения" },
            new MedcineModel { MedcineId = 3, MedcineName = "Таблеточки", ActiveSubstanceName = "Аспирин", ManufacturerName = "Bauer", MedcineAnnotation = null, MedcineFormName = "Таблетки" },
            new MedcineModel { MedcineId = 4, MedcineName = "Оттакмвон", ActiveSubstanceName = "Озверин", ManufacturerName = "БелФарм", MedcineAnnotation = null, MedcineFormName = "Сироп" },
            new MedcineModel { MedcineId = 5, MedcineName = "Недопересыпазм", ActiveSubstanceName = "Нитроглицирин", ManufacturerName = "МонголФарм", MedcineAnnotation = "Очень полезно", MedcineFormName = "Сироп" },
            new MedcineModel { MedcineId = 6, MedcineName = "Антананариву", ActiveSubstanceName = "Парацетомол", ManufacturerName = "МонголФарм", MedcineAnnotation = null, MedcineFormName = "Таблетки" },
            new MedcineModel { MedcineId = 7, MedcineName = "Лекарство №3.14", ActiveSubstanceName = "Активированный уголь", ManufacturerName = "РосФарм", MedcineAnnotation = "Сверх полезно", MedcineFormName = "Таблетки" },
            new MedcineModel { MedcineId = 8, MedcineName = "Для палаты №6", ActiveSubstanceName = "Активированный уголь", ManufacturerName = "Bauer", MedcineAnnotation = null, MedcineFormName = "Мазь" },
            new MedcineModel { MedcineId = 9, MedcineName = "Сложно что-то придумать", ActiveSubstanceName = "Аспирин", ManufacturerName = "Bauer", MedcineAnnotation = "Бесполезно", MedcineFormName = "Мазь" },
            new MedcineModel { MedcineId = 10, MedcineName = "Лекарство №10.5", ActiveSubstanceName = "Нитроглицирин", ManufacturerName = "БелФарм", MedcineAnnotation = "Бесполезно и немного вредит", MedcineFormName = "Таблетки" }
        };

        private readonly System.Collections.Generic.List<UserModel> _users = new System.Collections.Generic.List<UserModel>
        {
            new UserModel { Login = "admin", Role = "Администратор", Password = null}
        };

        /// <summary>
        /// Drop test database and recreate it for the test.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            using (var context = new MedcineDbContext())
            {
                if (context.Database.Exists())
                    Database.Delete(context.Database.Connection.ConnectionString);
            }
        }

        /// <summary>
        /// Test <see cref="MedcinesController.GetAllMedcines()"/> action.
        /// </summary>
        [Test]
        public void GetAllMedcines()
        {
            var controller = new MedcinesController();
            //Check all medcines data.
            var result = controller.GetAllMedcines().ToList();
            Assert.AreEqual(_medcines.Count, result.Count);
            for (var i = 0; i < _medcines.Count; i++)
            {
                Assert.AreEqual(_medcines[i].MedcineId, result[i].MedcineId, "Iteration " + (i + 1));
                Assert.AreEqual(_medcines[i].MedcineName, result[i].MedcineName, "Iteration " + (i + 1));
                Assert.AreEqual(_medcines[i].ActiveSubstanceName, result[i].ActiveSubstanceName, "Iteration " + (i + 1));
                Assert.AreEqual(_medcines[i].ManufacturerName, result[i].ManufacturerName, "Iteration " + (i + 1));
                Assert.AreEqual(_medcines[i].MedcineAnnotation, result[i].MedcineAnnotation, "Iteration " + (i + 1));
                Assert.AreEqual(_medcines[i].MedcineFormName, result[i].MedcineFormName, "Iteration " + (i + 1));
            }
        }

        /// <summary>
        /// Test <see cref="MedcinesController.GetMedcine(int)"/> action with invalid data.
        /// </summary>
        [Test]
        public void GetMedcine_Invalid()
        {
            var controller = new MedcinesController();
            //Result is null.
            var result = controller.GetMedcine(-1);
            Assert.IsNull(result);
        }

        /// <summary>
        /// Test <see cref="MedcinesController.GetMedcine(int)"/> action with valid data.
        /// </summary>
        [Test]
        public void GetMedcine_Valid()
        {
            var controller = new MedcinesController();
            //Result is not null. Check all medcines data.
            var result = controller.GetMedcine(3);
            Assert.IsNotNull(result);
            var expected = _medcines.FirstOrDefault(med => med.MedcineId == 3);
            Assert.AreEqual(expected.MedcineId, result.MedcineId);
            Assert.AreEqual(expected.MedcineName, result.MedcineName);
            Assert.AreEqual(expected.ActiveSubstanceName, result.ActiveSubstanceName);
            Assert.AreEqual(expected.ManufacturerName, result.ManufacturerName);
            Assert.AreEqual(expected.MedcineAnnotation, result.MedcineAnnotation);
            Assert.AreEqual(expected.MedcineFormName, result.MedcineFormName);
        }

        /// <summary>
        /// Test <see cref="UsersController.AllUsers()"/> action.
        /// </summary>
        [Test]
        public void AllUsers()
        {
            var controller = new UsersController();
            //Check all users data.
            var result = controller.AllUsers().ToList();
            Assert.AreEqual(_users.Count, result.Count);
            for (var i = 0; i < _users.Count; i++)
            {
                Assert.AreEqual(_users[i].Login, result[i].Login, "Iteration " + (i + 1));
                Assert.AreEqual(_users[i].Role, result[i].Role, "Iteration " + (i + 1));
                Assert.AreEqual(_users[i].Password, result[i].Password, "Iteration " + (i + 1));
            }
        }

        /// <summary>
        /// Test <see cref="UsersController.OneUser(int)"/> action with invalid data.
        /// </summary>
        [Test]
        public void OneUser_Invalid()
        {
            var controller = new UsersController();
            //Result is null.
            var result = controller.OneUser(-1);
            Assert.IsNull(result);
        }

        /// <summary>
        /// Test <see cref="UsersController.OneUser(int)"/> action with valid data.
        /// </summary>
        [Test]
        public void GetUser_Valid()
        {
            var controller = new UsersController();
            //Result is not null. Check all user data.
            var result = controller.OneUser(1);
            Assert.IsNotNull(result);
            var expected = _users.FirstOrDefault(usr => usr.Login == "admin");
            Assert.AreEqual(expected.Login, result.Login);
            Assert.AreEqual(expected.Role, result.Role);
            Assert.AreEqual(expected.Password, result.Password);
        }

        /// <summary>
        /// Test <see cref="UsersController.RegisterUser(UserModel)"/> action with valid data.
        /// </summary>
        [Test]
        public void RegisterUser()
        {
            var newUser = new UserModel { Login = "new_test_user", Password = "123321", Role = "sdfasfdsfasfads" };
            var expected = new UserModel { Login = "new_test_user", Password = null, Role = "Пользователь" };
            var controller = new UsersController();
            //Result is true. Check added data. Password is not returned.
            var result = controller.RegisterUser(newUser);
            Assert.IsTrue(result);
            var data = controller.AllUsers().Where(usr => usr.Login == newUser.Login).ToList();
            Assert.AreEqual(1, data.Count);
            Assert.AreEqual(expected.Login, data[0].Login);
            Assert.AreEqual(expected.Role, data[0].Role);
            Assert.AreEqual(expected.Password, data[0].Password);
            //Result is false. User with the same login was not added. Check added data. Password is not returned.
            result = controller.RegisterUser(newUser);
            Assert.IsFalse(result);
            data = controller.AllUsers().Where(usr => usr.Login == newUser.Login).ToList();
            Assert.AreEqual(1, data.Count);
        }
    }
}

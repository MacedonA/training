﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using NUnit.Framework;
using Task08Application.Controllers;
using Task08Application.Models;
using Task08Application.Models.DetailsViewModel;
using Task08Application.Models.EditUserViewModel;
using Task08Application.Models.IndexViewModel;
using Task08Application.Models.OrderViewModel;
using Task08Application.Models.RegisterViewModel;
using Task08DataAccessLayer.Entities;
using Task08DataAccessLayer.Model;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace Task08Application.Tests.Controllers
{
    /// <summary>
    /// Test <see cref="HomeController"/>.
    /// </summary>
    [TestFixture]
    public class HomeControllerTest
    {
        /// <summary>
        /// Drop test database and recreate it for the test.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            using (var context = new MedcineDbContext())
            {
                if (context.Database.Exists())
                    Database.Delete(context.Database.Connection.ConnectionString);
            }
        }

        /// <summary>
        /// Test <see cref="HomeController.Index()"/> action.
        /// </summary>
        [Test]
        public void Index()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ViewName);
            //2. Has model.
            var model = result.Model as IndexViewModel;
            Assert.IsNotNull(model);
            //3. Has correct rows per page.
            Assert.AreEqual(2, model.PageSize);
            Assert.AreEqual(2, model.MedcineModels.Count);
            //4. Has correct manufactures filter.
            Assert.AreEqual(0, model.SelectedManufacturerValue);
            Assert.AreEqual(5, model.Manufacturers.Count());
            var man = model.Manufacturers.First();
            Assert.AreEqual("0", man.Value);
            Assert.AreEqual("", man.Text);
            //5. Has correct substances filter.
            Assert.AreEqual(0, model.SelectedSubstanceValue);
            Assert.AreEqual(6, model.Substances.Count());
            var sub = model.Substances.Skip(2).First();
            Assert.AreEqual("2", sub.Value);
            Assert.AreEqual("Аспирин", sub.Text);
            //6. Has correct forms filter.
            Assert.AreEqual(0, model.SelectedMedcineFormValue);
            Assert.AreEqual(5, model.MedcineForms.Count());
            var form = model.MedcineForms.Skip(3).First();
            Assert.AreEqual("3", form.Value);
            Assert.AreEqual("Сироп", form.Text);
        }

        /// <summary>
        /// Test <see cref="HomeController.MedcineData(IndexViewModel)"/> action.
        /// </summary>
        [Test]
        public void MedcineDataPost()
        {
            var controller = new HomeController();
            var oldModel = new IndexViewModel
            {
                PageNumber = 2,
                SelectedSubstanceValue = 1
            };
            //1. Has result with correct view.
            var result = controller.MedcineData(oldModel);
            Assert.IsNotNull(result);
            Assert.AreEqual("MedcineData", result.ViewName);
            //2. Has model with some values from old model.
            var model = result.Model as IndexViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(oldModel.PageNumber, model.PageNumber);
            Assert.AreEqual(oldModel.SelectedSubstanceValue, model.SelectedSubstanceValue);
            //3. Shows correct data.
            Assert.AreEqual(1, model.MedcineModels.Count);
            var med = model.MedcineModels.First();
            Assert.AreEqual("Лекарство №3.14", med.MedcineName);
            Assert.AreEqual("Сверх полезно", med.MedcineAnnotation);
            Assert.AreEqual("Активированный уголь", med.ActiveSubstanceName);
            Assert.AreEqual("Таблетки", med.MedcineFormName);
            Assert.AreEqual("РосФарм", med.ManufacturerName);
        }

        /// <summary>
        /// Test <see cref="HomeController.MedcineData(IndexViewModel)"/> action to show correct pages.
        /// </summary>
        [Test]
        public void MedcineDataPages()
        {
            var controller = new HomeController();
            var model = (controller.Index() as ViewResult).Model as IndexViewModel;
            //1. Shows correct pages initial.
            var pages = model.GetPages();
            Assert.AreEqual(3, pages.Length);
            Assert.AreEqual(1, pages[0]);
            Assert.AreEqual(2, pages[1]);
            Assert.AreEqual(5, pages[2]);
            //2. Shows correct pages after paging.
            model.PageNumber = 2;
            var result = controller.MedcineData(model);
            model = result.Model as IndexViewModel;
            pages = model.GetPages();
            Assert.AreEqual(4, pages.Length);
            Assert.AreEqual(1, pages[0]);
            Assert.AreEqual(2, pages[1]);
            Assert.AreEqual(3, pages[2]);
            Assert.AreEqual(5, pages[3]);
            //3. Shows correct pages after paging.
            model.PageNumber = 4;
            result = controller.MedcineData(model);
            model = result.Model as IndexViewModel;
            pages = model.GetPages();
            Assert.AreEqual(4, pages.Length);
            Assert.AreEqual(1, pages[0]);
            Assert.AreEqual(3, pages[1]);
            Assert.AreEqual(4, pages[2]);
            Assert.AreEqual(5, pages[3]);
            //4. Shows correct pages after filtering.
            model.SelectedManufacturerValue = 1;
            result = controller.MedcineData(model);
            model = result.Model as IndexViewModel;
            pages = model.GetPages();
            Assert.AreEqual(2, pages.Length);
            Assert.AreEqual(1, pages[0]);
            Assert.AreEqual(2, pages[1]);
        }

        /// <summary>
        /// Test <see cref="HomeController.Details"/> action with invalid data.
        /// </summary>
        [Test]
        public void DetailsInvalid()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.Details(-1) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Error", result.ViewName);
            //2. Has not model.
            Assert.IsNull(result.Model);
            //3. Has result with correct view.
            result = controller.Details(null) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Error", result.ViewName);
            //4. Has not model.
            Assert.IsNull(result.Model);
        }

        /// <summary>
        /// Test <see cref="HomeController.Details"/> action with valid data.
        /// </summary>
        [Test]
        public void DetailsValid()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.Details(3) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Details", result.ViewName);
            //2. Has model.
            var model = result.Model as DetailsViewModel;
            Assert.IsNotNull(model);
            //3. Validate data.
            Assert.IsNotNull(model.Medcine);
            Assert.AreEqual("Таблеточки", model.Medcine.MedcineName);
            Assert.AreEqual("Аспирин", model.Medcine.ActiveSubstanceName);
            Assert.AreEqual("Таблетки", model.Medcine.MedcineFormName);
            Assert.AreEqual("Bauer", model.Medcine.ManufacturerName);
            Assert.IsNull(model.Medcine.MedcineAnnotation);
        }

        /// <summary>
        /// Test <see cref="HomeController.Order(System.Nullable{int})"/> action with invalid data.
        /// </summary>
        [Test]
        public void OrderInvalid()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.Order(-1) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Error", result.ViewName);
            //2. Has not model.
            Assert.IsNull(result.Model);
            //3. Has result with correct view.
            result = controller.Order(null as int?) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Error", result.ViewName);
            //4. Has not model.
            Assert.IsNull(result.Model);
        }

        /// <summary>
        /// Test <see cref="HomeController.Order(System.Nullable{int})"/> action with valid data.
        /// </summary>
        [Test]
        public void OrderValid()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.Order(3) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Order", result.ViewName);
            //2. Has model.
            var model = result.Model as OrderViewModel;
            Assert.IsNotNull(model);
            //3. Validate data.
            Assert.IsNotNull(model.Medcine);
            Assert.AreEqual(3, model.Medcine.MedcineId);
        }

        /// <summary>
        /// Test <see cref="HomeController.Order(OrderViewModel)"/> action with invalid data.
        /// </summary>
        [Test]
        public void OrderPostInvalid()
        {
            var invalidData = new OrderViewModel
            {
                Medcine = new MedcineModel {MedcineId = 2},
                Address = "Valid Address",
                EMailAddress = "InvalidEmail",
                LastName = "LongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongName",
                FirstName = "Name",
                Comments = "Comments",
                PhoneNumber = "This is phone"
            };
            var controller = new HomeController();
            //1. Has result with correct view.
            controller.ModelState.AddModelError("this is test error", "test error text");
            var result = controller.Order(invalidData) as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.IsTrue(result.RouteValues.ContainsKey("id"));
            Assert.AreEqual(invalidData.Medcine.MedcineId, result.RouteValues["id"]);
            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.AreEqual("Order", result.RouteValues["action"]);
            /* Ignore these checks because data in session now.
            //2. Has model.
            Assert.IsTrue(controller.TempData.ContainsKey("OrderViewModel"));
            var model = controller.TempData["OrderViewModel"] as OrderViewModel;
            Assert.IsNotNull(model);
            //3. Validate data.
            Assert.IsNotNull(model.Medcine);
            Assert.AreEqual(invalidData.Medcine.MedcineId, model.Medcine.MedcineId);
            Assert.AreEqual(invalidData.Address, model.Address);
            Assert.AreEqual(invalidData.EMailAddress, model.EMailAddress);
            Assert.AreEqual(invalidData.LastName, model.LastName);
            Assert.AreEqual(invalidData.FirstName, model.FirstName);
            Assert.AreEqual(invalidData.Comments, model.Comments);
            Assert.AreEqual(invalidData.PhoneNumber, model.PhoneNumber);*/
        }

        /// <summary>
        /// Test <see cref="HomeController.Order(OrderViewModel)"/> action with valid data.
        /// </summary>
        [Test]
        public void OrderPostValid()
        {
            var validData = new OrderViewModel
            {
                Medcine = new MedcineModel { MedcineId = 1 },
                Address = "Valid Address",
                EMailAddress = "Invalid@Email.com",
                LastName = "LongName",
                FirstName = "Name",
                Comments = "Comments",
                PhoneNumber = "This is phone"
            };
            var controller = new HomeController();
            //1. Has redirect result with correct view.
            var resultRedirect = controller.Order(validData) as RedirectToRouteResult;
            Assert.IsNotNull(resultRedirect);
            //2. Correct redirect action.
            Assert.IsTrue(resultRedirect.RouteValues.ContainsKey("action"));
            Assert.AreEqual("Index", resultRedirect.RouteValues["action"]);
        }

        /// <summary>
        /// Test <see cref="HomeController.EditMedcine(System.Nullable{int})"/> action with invalid data.
        /// </summary>
        [Test]
        public void EditMedcineInvalid()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.EditMedcine(-1) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Error", result.ViewName);
            //2. Has not model.
            Assert.IsNull(result.Model);
            //3. Has result with correct view.
            result = controller.EditMedcine(null as int?) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Error", result.ViewName);
            //4. Has not model.
            Assert.IsNull(result.Model);
        }

        /// <summary>
        /// Test <see cref="HomeController.EditMedcine(System.Nullable{int})"/> action with valid data.
        /// </summary>
        [Test]
        public void EditMedcineValid()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.EditMedcine(3) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EditMedcine", result.ViewName);
            //2. Has model.
            var model = result.Model as EditDetailsViewModel;
            Assert.IsNotNull(model);
            //3. Validate data.
            Assert.AreEqual(3, model.MedcineId);
            Assert.AreEqual("Таблеточки", model.MedcineName);
            Assert.IsNull(model.MedcineAnnotation);
            Assert.AreEqual(1, model.MedcineFormId);
            Assert.AreEqual(4, model.ImageId);
            Assert.AreEqual(2, model.ActiveSubstanceId);
            Assert.AreEqual(1, model.ManufacturerId);
            Assert.AreEqual(4, model.Manufacturers.Count());
            Assert.AreEqual(4, model.MedcineForms.Count());
            Assert.AreEqual(5, model.Substances.Count());
        }

        /// <summary>
        /// Test <see cref="HomeController.EditMedcine(EditDetailsViewModel)"/> action with invalid data.
        /// </summary>
        [Test]
        public void EditMedcinePostInvalid()
        {
            var invalidData = new EditDetailsViewModel
            {
                MedcineId = 2,
                MedcineName = "LongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongName",
                MedcineFormId = 1,
                MedcineAnnotation = "LongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongName",
                ActiveSubstanceId = 3,
                ImageId = 5,
                ManufacturerId = 2
            };
            var controller = new HomeController();
            //1. Has result with correct view.
            controller.ModelState.AddModelError("this is test error", "test error text");
            var result = controller.EditMedcine(invalidData) as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.IsTrue(result.RouteValues.ContainsKey("id"));
            Assert.AreEqual(invalidData.MedcineId, result.RouteValues["id"]);
            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.AreEqual("EditMedcine", result.RouteValues["action"]);
            /* Ignore these checks because data in session now.
            //2. Has model.
            Assert.IsTrue(controller.TempData.ContainsKey("EditDetailsViewModel"));
            var model = controller.TempData["EditDetailsViewModel"] as EditDetailsViewModel;
            Assert.IsNotNull(model);
            //3. Validate data.
            Assert.AreEqual(invalidData.MedcineId, model.MedcineId);
            Assert.AreEqual(invalidData.MedcineName, model.MedcineName);
            Assert.AreEqual(invalidData.MedcineFormId, model.MedcineFormId);
            Assert.AreEqual(invalidData.MedcineAnnotation, model.MedcineAnnotation);
            Assert.AreEqual(invalidData.ActiveSubstanceId, model.ActiveSubstanceId);
            Assert.AreEqual(invalidData.ImageId, model.ImageId);
            Assert.AreEqual(invalidData.ManufacturerId, model.ManufacturerId);*/
        }

        /// <summary>
        /// Test <see cref="HomeController.EditMedcine(EditDetailsViewModel)"/> action with valid data.
        /// </summary>
        [Test]
        public void EditMedcinePostValid()
        {
            var validData = new EditDetailsViewModel
            {
                MedcineId = 2,
                MedcineName = "Just a Name",
                MedcineFormId = 1,
                MedcineAnnotation = "Just an Annotation",
                ActiveSubstanceId = 3,
                ImageId = 5,
                ManufacturerId = 1
            };
            var controller = new HomeController();
            //1. Has redirect result with correct view.
            var resultRedirect = controller.EditMedcine(validData) as RedirectToRouteResult;
            Assert.IsNotNull(resultRedirect);
            //2. Correct redirect action.
            Assert.IsTrue(resultRedirect.RouteValues.ContainsKey("action"));
            Assert.AreEqual("Index", resultRedirect.RouteValues["action"]);
        }

        /// <summary>
        /// Test <see cref="HomeController.AddMedcine()"/> action with valid data.
        /// </summary>
        [Test]
        public void AddMedcine()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.AddMedcine() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EditMedcine", result.ViewName);
            //2. Has model.
            var model = result.Model as EditDetailsViewModel;
            Assert.IsNotNull(model);
            //3. Validate data.
            Assert.AreEqual(0, model.MedcineId);
            Assert.IsNull(model.MedcineName);
            Assert.IsNull(model.MedcineAnnotation);
            Assert.AreEqual(0, model.MedcineFormId);
            Assert.IsNull(model.ImageId);
            Assert.AreEqual(0, model.ActiveSubstanceId);
            Assert.AreEqual(0, model.ManufacturerId);
            Assert.AreEqual(4, model.Manufacturers.Count());
            Assert.AreEqual(4, model.MedcineForms.Count());
            Assert.AreEqual(5, model.Substances.Count());
        }

        /// <summary>
        /// Test <see cref="HomeController.AddMedcine(EditDetailsViewModel)"/> action with invalid data.
        /// </summary>
        [Test]
        public void AddMedcinePostInvalid()
        {
            var invalidData = new EditDetailsViewModel
            {
                MedcineName = "LongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongName",
                MedcineFormId = 1,
                MedcineAnnotation = "LongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongName",
                ActiveSubstanceId = 3,
                ImageId = 5,
                ManufacturerId = 2
            };
            var controller = new HomeController();
            //1. Has result with correct view.
            controller.ModelState.AddModelError("this is test error", "test error text");
            var result = controller.AddMedcine(invalidData) as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.IsFalse(result.RouteValues.ContainsKey("id"));
            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.AreEqual("AddMedcine", result.RouteValues["action"]);
            //2. Has model.
            Assert.IsTrue(controller.TempData.ContainsKey("EditDetailsViewModel"));
            var model = controller.TempData["EditDetailsViewModel"] as EditDetailsViewModel;
            Assert.IsNotNull(model);
            //3. Validate data.
            Assert.AreEqual(invalidData.MedcineId, model.MedcineId);
            Assert.AreEqual(invalidData.MedcineName, model.MedcineName);
            Assert.AreEqual(invalidData.MedcineFormId, model.MedcineFormId);
            Assert.AreEqual(invalidData.MedcineAnnotation, model.MedcineAnnotation);
            Assert.AreEqual(invalidData.ActiveSubstanceId, model.ActiveSubstanceId);
            Assert.AreEqual(invalidData.ImageId, model.ImageId);
            Assert.AreEqual(invalidData.ManufacturerId, model.ManufacturerId);
        }

        /// <summary>
        /// Test <see cref="HomeController.DeleteMedcine(System.Nullable{int})"/> action with invalid data.
        /// </summary>
        [Test]
        public void DeleteMedcineInvalid()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.DeleteMedcine(-1) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Error", result.ViewName);
            //2. Has not model.
            Assert.IsNull(result.Model);
            //3. Has result with correct view.
            result = controller.DeleteMedcine(null as int?) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Error", result.ViewName);
            //4. Has not model.
            Assert.IsNull(result.Model);
        }

        /// <summary>
        /// Test <see cref="HomeController.AddMedcine(EditDetailsViewModel)"/> and <see cref="HomeController.DeleteMedcine(System.Nullable{int})"/> actions with valid data.
        /// </summary>
        [Test]
        public void AddMedcinePostDeleteMedcineValid()
        {
            var validData = new EditDetailsViewModel
            {
                MedcineName = "Just a Name",
                MedcineFormId = 1,
                MedcineAnnotation = "Just an Annotation",
                ActiveSubstanceId = 3,
                ImageId = 3,
                ManufacturerId = 2
            };
            var controller = new HomeController();
            //1. Has redirect result with correct view.
            var resultRedirect = controller.AddMedcine(validData) as RedirectToRouteResult;
            Assert.IsNotNull(resultRedirect);
            //2. Correct redirect action.
            Assert.IsTrue(resultRedirect.RouteValues.ContainsKey("action"));
            Assert.AreEqual("Index", resultRedirect.RouteValues["action"]);
            //3. Has result with correct view.
            int id;
            using (var context = new MedcineDbContext())
            {
                var dbSet = context.Set<Medcine>();
                id = dbSet.FirstOrDefault(x => x.MedcineName == validData.MedcineName).MedcineId;
            }
            resultRedirect = controller.DeleteMedcine(id) as RedirectToRouteResult;
            Assert.IsNotNull(resultRedirect);
            //4. Correct redirect action.
            Assert.IsTrue(resultRedirect.RouteValues.ContainsKey("action"));
            Assert.AreEqual("Index", resultRedirect.RouteValues["action"]);
        }

        /// <summary>
        /// Test <see cref="HomeController.DoError"/> action.
        /// </summary>
        [Test]
        public void DoError()
        {
            var controller = new HomeController();
            Exception generatedException = null;
            //1. An error was generated.
            try { controller.DoError(); }
            catch (Exception exc) { generatedException = exc; }
            Assert.IsNotNull(generatedException, "Action does not produced an error.");
            //2. Error has inner exception.
            Assert.IsNotNull(generatedException.InnerException, "An inner exception does not exist.");
            Assert.IsNull(generatedException.InnerException.InnerException, "There are to many inner exceptions.");
            //3. Error has an expected type exception.
            Assert.IsTrue(generatedException is NullReferenceException, "An exception has wrong type.");
            Assert.IsTrue(generatedException.InnerException is ArgumentException, "An inner exception has wrong type.");
        }

        /// <summary>
        /// Test <see cref="HomeController.ShowMedcineImage"/> action with valid data.
        /// </summary>
        [Test]
        public void ShowMedcineImageValid()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.ShowMedcineImage(3) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("MedcineImageView", result.ViewName);
            //2. Has image.
            var image = result.ViewBag.MedcineImage as byte[];
            Assert.IsNotNull(image);
            //4. Image can be null.
            result = controller.ShowMedcineImage(4) as PartialViewResult;
            Assert.IsNotNull(result);
            image = result.ViewBag.MedcineImage as byte[];
            Assert.IsNull(image);
        }

        /// <summary>
        /// Test <see cref="HomeController.Register()"/> action with valid data.
        /// </summary>
        [Test]
        public void RegisterValid()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.Register() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Register", result.ViewName);
            //2. Has model.
            var model = result.Model as RegisterViewModel;
            Assert.IsNotNull(model);
        }

        /// <summary>
        /// Test <see cref="HomeController.Register(RegisterViewModel)"/> action with invalid data.
        /// </summary>
        [Test]
        public void RegisterPostInvalid()
        {
            var invalidData = new RegisterViewModel
            {
                Login = "LongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongLongName",
                ConfirmPassword = "asd",
                Password = "asdf"
            };
            var controller = new HomeController();
            //1. Has result with correct view.
            controller.ModelState.AddModelError("this is test error", "test error text");
            var result = controller.Register(invalidData) as ViewResult;
            Assert.IsNotNull(result);
            //2. Has model.
            Assert.IsTrue(controller.TempData.ContainsKey("RegisterInfo"));
            var model = controller.TempData["RegisterInfo"] as RegisterViewModel;
            Assert.IsNotNull(model);
            //3. Validate data.
            Assert.AreEqual(invalidData.Login, model.Login);
            Assert.AreEqual(invalidData.Password, model.Password);
            Assert.AreEqual(invalidData.ConfirmPassword, model.ConfirmPassword);
        }

        /// <summary>
        /// Test <see cref="HomeController.Register(RegisterViewModel)"/> action with valid data.
        /// </summary>
        [Test]
        public void RegisterPostValid()
        {
            var validData = new RegisterViewModel
            {
                Login = "UsusalName",
                ConfirmPassword = "asdf",
                Password = "asdf"
            };
            var controller = new HomeController();
            //1. Has redirect result with correct view.
            var resultRedirect = controller.Register(validData) as RedirectToRouteResult;
            Assert.IsNotNull(resultRedirect);
            //2. Correct redirect action.
            Assert.IsTrue(resultRedirect.RouteValues.ContainsKey("action"));
            Assert.AreEqual("Index", resultRedirect.RouteValues["action"]);
        }

        /// <summary>
        /// Test <see cref="HomeController.EditUser"/> action with valid data.
        /// </summary>
        [Test]
        public void EditUser()
        {
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.EditUser() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EditUserSelection", result.ViewName);
            //2. Has model.
            var model = result.Model as EditUserSelectionViewModel;
            Assert.IsNotNull(model);
        }

        /// <summary>
        /// Test <see cref="HomeController.EditUserDetails"/> action with invalid data.
        /// </summary>
        [Test]
        public void EditUserDetailsInvalid()
        {
            var invalidData = new EditUserSelectionViewModel()
            {
                SelectedUser = 0
            };
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.EditUserDetails(invalidData) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EditUserDetails", result.ViewName);
            //2. Has no model.
            Assert.IsNull(result.Model);
        }

        /// <summary>
        /// Test <see cref="HomeController.EditUserDetails"/> action with valid data.
        /// </summary>
        [Test]
        public void EditUserDetailsValid()
        {
            var validData = new EditUserSelectionViewModel()
            {
                SelectedUser = 1
            };
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.EditUserDetails(validData) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EditUserDetails", result.ViewName);
            var model = result.Model as EditUserDetailsViewModel;
            //2. Has model image of correct type with correct data.
            Assert.IsNotNull(model);
            Assert.AreEqual(1, model.CurerntRole);
        }

        /// <summary>
        /// Test <see cref="HomeController.SaveUserDetails"/> action with invalid data.
        /// </summary>
        [Test]
        public void SaveUserDetailsInvalid()
        {
            var invalidSelectionData = new EditUserSelectionViewModel
            {
                SelectedUser = 1
            };
            var validDetailsData = new EditUserDetailsViewModel
            {
                CurerntRole = 2
            };
            var controller = new HomeController();
            //1. Has result with correct view.
            var result = controller.SaveUserDetails(invalidSelectionData, validDetailsData) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EditUserSelection", result.ViewName);
            //2. Has correct model and data was not changed.
            var model = result.Model as EditUserSelectionViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(invalidSelectionData.SelectedUser, model.SelectedUser);
            Assert.AreNotEqual(validDetailsData.CurerntRole, DataReader.GetUserDetalisViewModel(invalidSelectionData.SelectedUser).CurerntRole);
        }

        /// <summary>
        /// Test <see cref="HomeController.SaveUserDetails"/> action with valid data.
        /// </summary>
        [Test]
        public void SaveUserDetailsValid()
        {
            var toAdd = new RegisterViewModel
            {
                Login = "ToTestChange",
                ConfirmPassword = "123",
                Password = "123"
            };
            var controller = new HomeController();
            controller.Register(toAdd);
            int id;
            using (var context = new MedcineDbContext())
            {
                var dbSet = context.Set<User>();
                id = dbSet.FirstOrDefault(x => x.Login == toAdd.Login).UserId;
            }
            var validSelectionData = new EditUserSelectionViewModel
            {
                SelectedUser = id
            };
            var validDetailsData = new EditUserDetailsViewModel
            {
                CurerntRole = 1
            };
            //1. Has result with correct view.
            var result = controller.SaveUserDetails(validSelectionData, validDetailsData) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EditUserSelection", result.ViewName);
            //2. Has correct model and data was not changed.
            var model = result.Model as EditUserSelectionViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(validSelectionData.SelectedUser, model.SelectedUser);
            Assert.AreEqual(validDetailsData.CurerntRole, DataReader.GetUserDetalisViewModel(validSelectionData.SelectedUser).CurerntRole);
        }
    }
}

function checkRequiredField(fieldName) {
	var result = true;
	var field = document.getElementById(fieldName);
	var container = document.getElementById(fieldName+'Container');
	resetError(container);
	if (field != null) {
		if (!field.value || field.value.trim() == '') {
			showError(container, 'Это обязательное поле');
			result = false;
		}
	}
	return result;
}

function checkEmailField(fieldName) {
	var result = true;
	var field = document.getElementById(fieldName);
	var container = document.getElementById(fieldName+'Container');
	resetError(container);
	if (field != null && field.value) {
		var r = /^\w+@\w+\.\w{2,4}$/i;
		if (!r.test(field.value)) {
			showError(container, 'Это значение не соответвтвует формату адреса электронной почты.');
			result = false;
		}
	}
	return result;
}

function showError(container, errorMessage) {
	if (container==null)
		return;
	container.className = 'has-error';
	var msgElem = document.createElement('div');
	msgElem.className = "alert alert-danger";
	msgElem.innerHTML = errorMessage;
	container.appendChild(msgElem);
	return;
}

function resetError(container) {
	if (container==null)
		return;
	container.className = '';
	if (container.lastChild.className == "alert alert-danger") {
		container.removeChild(container.lastChild);
	}
}
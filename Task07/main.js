function selectTypeChange() {
	var selectType = document.getElementById('selectType');
	var dishesLink = selectType.options[selectType.selectedIndex].getAttribute("reference");
	var dishImage = document.getElementById('dishImage');
	document.getElementById('getChoiceContainer').style.display = 'none';
	if (dishImage != null) { dishImage.style.display = 'none'; }
	document.getElementById('dishesContainer').style.display = 'block';
	document.getElementById('additionalParametersContainer').style.display = 'none';
	setParametersState(false, null);
	var rqo = prepareXMLHttpRequest(dishesLink);
	rqo.onload = function() { setDishesState(false, rqo.responseText); }
	rqo.onerror = function() { setDishesState(false, 'Блюда не найдены. Попробуйте позже.'); }
	rqo.ontimeout = function() { setDishesState(false, 'Слишком долго. Попробуйте позже.'); }
	setDishesState(true, 'Загрузка...');
	rqo.send(null);
	return false;
}

function setDishesState(disableSelect, containerContent) {
	document.getElementById('selectType').disabled = disableSelect;
	document.getElementById('dishes').innerHTML = containerContent;
	return false;
}

function selectedDishChange() {
	var selectedDish = document.getElementById('selectDish');
	var selectedDishItem = selectedDish.options[selectedDish.selectedIndex];
	document.getElementById('getChoiceContainer').style.display = 'block';
	
	if (selectedDishItem.hasAttribute("imageLoc")) {
		var dishImage = document.getElementById('dishImage');
		dishImage.src = selectedDishItem.getAttribute("imageLoc");
		dishImage.style.display = 'block';
	}

	if (selectedDishItem.hasAttribute("addParams")) {	
		var additionalParameters = document.getElementById('additionalParameters');
		var params = selectedDishItem.getAttribute("addParams");
		var rqo = prepareXMLHttpRequest(params);
		rqo.onload = function() { setParametersState(false, rqo.responseText); }
		rqo.onerror = function() { setParametersState(false, 'Параметры не найдены. Попробуйте позже.'); }
		rqo.ontimeout = function() { setParametersState(false, 'Слишком долго. Попробуйте позже.'); }
		setParametersState(true, 'Загрузка...');
		rqo.send(null);	
		document.getElementById('additionalParametersContainer1').style.display = 'block';
	}
	else {
		document.getElementById('additionalParametersContainer1').style.display = 'none';
	}
	
	document.getElementById('additionalParametersContainer').style.display = 'block';

	return false;
}

function prepareXMLHttpRequest(params) {
	var rqo = new XMLHttpRequest();
	rqo.open('GET', params + ((/\?/).test(params) ? "&" : "?") + (new Date()).getTime(), true);
	rqo.timeout = 3000;
	return rqo;
}

function setParametersState(disableSelect, containerContent) {
	document.getElementById('selectType').disabled = disableSelect;
	var selectDish = document.getElementById('selectDish');
	if (selectDish != null) { document.getElementById('selectDish').disabled = disableSelect; }
	document.getElementById('additionalParameters').innerHTML = containerContent;
	return false;
}
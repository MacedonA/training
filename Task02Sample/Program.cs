﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Task02.Data;
using Task02.Statistics;

namespace Task02Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            var statistics = new Statistics();

            #region Loads Sample Data. It Expects Correct Data In Correct Format
            var questions = new List<Question>();
            var doc = new XmlDocument();
            doc.Load(@"sample.xml");
            foreach (var questionNode in doc.SelectNodes(@"//root/question").OfType<XmlElement>())
            {
                if (!questionNode.HasAttribute("id") || !questionNode.HasAttribute("text") || !questionNode.HasAttribute("complexity"))
                    throw new ArgumentException("Incorrect data");
                uint complexity;
                if (!uint.TryParse(questionNode.Attributes["complexity"].InnerText, out complexity))
                    throw new ArgumentException("Incorrect data");
                Guid id;
                if (!Guid.TryParse(questionNode.Attributes["id"].InnerText, out id))
                    throw new ArgumentException("Incorrect data");
                string text = questionNode.Attributes["text"].InnerText;
                questions.Add(new Question(complexity, text, id));
            }

            foreach (var testNode in doc.SelectNodes(@"//root/test").OfType<XmlElement>())
            {
                if (!testNode.HasAttribute("name") || !testNode.HasAttribute("passDate") || !testNode.HasAttribute("user"))
                    throw new ArgumentException("Incorrect data");
                DateTime passDate;
                if (!DateTime.TryParse(testNode.Attributes["passDate"].InnerText, out passDate))
                    throw new ArgumentException("Incorrect data");
                string name = testNode.Attributes["name"].InnerText;
                string user = testNode.Attributes["user"].InnerText;

                var answers = new List<Answer>();
                foreach (var answerNode in testNode.SelectNodes(@"./answer").OfType<XmlElement>())
                {
                    if (!answerNode.HasAttribute("questionId") || !answerNode.HasAttribute("isCorrect"))
                        throw new ArgumentException("Incorrect data");
                    bool isCorrect;
                    if (!bool.TryParse(answerNode.Attributes["isCorrect"].InnerText, out isCorrect))
                        throw new ArgumentException("Incorrect data");
                    Guid questionId;
                    if (!Guid.TryParse(answerNode.Attributes["questionId"].InnerText, out questionId))
                        throw new ArgumentException("Incorrect data");
                    var question = questions.FirstOrDefault(quest => quest.Id == questionId);
                    if (question == null)
                        throw new ArgumentException("Incorrect data");
                    answers.Add(new Answer(isCorrect, question));
                }
                statistics.AddTest(user, new Test(passDate, answers, name));
            }
            #endregion

            Console.WriteLine("Part 1:");
            foreach (var item in statistics.GetTestsCountByName())
                Console.WriteLine("{0}: {1}", item.Item1, item.Item2);
            Console.WriteLine();

            var mostPopular = statistics.GetMostPopularTest();
            Console.WriteLine("Part 2: {0}", mostPopular == null ? string.Empty : mostPopular.Name);
            Console.WriteLine();

            Console.WriteLine("Part 3:");
            foreach (var item in statistics.GetResultsByTestName())
            {
                Console.WriteLine(item.Key);
                foreach (var subItem in item.Value)
                    Console.WriteLine("  {0}: {1}", subItem.Item1, subItem.Item2);
            }
            Console.WriteLine();

            var hardest = statistics.GetHardestQuestion();
            Console.WriteLine("Part 4: {0}", hardest == null ? string.Empty : hardest.Text);
            Console.WriteLine();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
